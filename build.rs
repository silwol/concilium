// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use glib_build_tools::compile_resources;

fn main() {
    compile_resources(
        &["resources"],
        "resources/app.gresource.xml",
        "concilium.gresource",
    );
}
