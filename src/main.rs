// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use app::App;
use clap::Parser;
use cli::Cli;
use gtk4::{gdk::Display, gio::resources_register_include, IconTheme};
use log::LevelFilter;
use relm4::RelmApp;
use simple_logger::SimpleLogger;

mod app;
mod authentication;
mod cli;
mod connect_form;
mod error_message;
mod join_form;
mod meeting;
mod participant_ext;
mod progress_indicator;
mod theme;
mod timer_ext;
mod utils;
mod waiting_room;

fn main() {
    resources_register_include!("concilium.gresource").expect("Failed to register resources.");

    let cli = Cli::parse();

    SimpleLogger::new()
        .env()
        .with_level(LevelFilter::Debug)
        .init()
        .unwrap();

    let app = RelmApp::new("net.silwol.Concilium");

    let icon_theme = IconTheme::for_display(&Display::default().expect("No display found"));
    icon_theme.add_resource_path("/icons");

    relm4::set_global_css(include_str!("style.css"));

    app.run_async::<App>(cli);
}
