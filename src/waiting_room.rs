// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt, ButtonExt, OrientableExt, WidgetExt};
use log::{info, warn};
use opentalk_client::{SignalingEvent, SignalingSink, SignalingStream, SignalingStreamEvent};
use opentalk_types::signaling::moderation::event::ModerationEvent;
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentParts},
    gtk, tokio, AsyncComponentSender,
};
use tokio::sync::mpsc::{Receiver, Sender};

use crate::theme::Theme;

pub struct WaitingRoom {
    theme: Theme,
    stop_tx: Sender<()>,
    stop_rx: Option<Receiver<()>>,
}

#[derive(Debug)]
pub struct WaitingRoomInit {
    pub theme: Theme,
}

#[derive(Debug)]
pub enum WaitingRoomInput {
    Enter {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
    },
    Return,
    SwitchTheme(Theme),
}

#[derive(Debug)]
pub enum WaitingRoomOutput {
    Accepted {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
    },
    Disconnected,
    Return,
}

#[derive(Debug)]
pub enum WaitingRoomCommandOutput {
    Listen {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
        stop_rx: Receiver<()>,
    },
    PingReceived {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
        stop_rx: Receiver<()>,
        data: Vec<u8>,
    },
    EventReceived {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
        stop_rx: Receiver<()>,
        event: SignalingEvent,
    },
    Closed {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
        stop_rx: Receiver<()>,
    },
    StoppedListening {
        stop_rx: Receiver<()>,
    },
}

#[relm4::component(async, pub)]
impl AsyncComponent for WaitingRoom {
    type Init = WaitingRoomInit;
    type Input = WaitingRoomInput;
    type Output = WaitingRoomOutput;
    type CommandOutput = WaitingRoomCommandOutput;

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Vertical,

            set_margin_start: 5,
            set_margin_end: 5,
            set_margin_top: 5,
            set_margin_bottom: 5,

            set_start_widget: Some(&start),
            set_center_widget: Some(&center),
        },

        #[name = "start"]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,

            set_spacing: 10,

            gtk::Button {
                set_icon_name: "go-previous-symbolic",
                connect_clicked => WaitingRoomInput::Return,
            },
        },

        #[name = "center"]
        adw::StatusPage {
            #[watch]
            set_icon_name: Some(model.theme.icon_name("waiting-room", "waiting-room-dark")),

            set_title: "Waiting room",
            set_description: Some("You're in the waiting room now, waiting for a moderator to grant admission"),
        }
    }

    async fn init(
        WaitingRoomInit { theme }: WaitingRoomInit,
        _root: Self::Root,
        _sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let (stop_tx, stop_rx) = tokio::sync::mpsc::channel(1);
        let model = Self {
            theme,
            stop_tx,
            stop_rx: Some(stop_rx),
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: WaitingRoomInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            WaitingRoomInput::Enter {
                signaling_sink,
                signaling_stream,
            } => {
                let stop_rx = self.stop_rx.take().unwrap();
                sender.oneshot_command(async move {
                    handle_next_event(signaling_sink, signaling_stream, stop_rx).await
                });
            }
            WaitingRoomInput::Return => {
                self.stop_tx.send(()).await.unwrap();
            }
            WaitingRoomInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
        }
    }

    async fn update_cmd(
        &mut self,
        message: WaitingRoomCommandOutput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match message {
            WaitingRoomCommandOutput::Listen {
                signaling_sink,
                signaling_stream,
                stop_rx,
            } => {
                sender.oneshot_command(async move {
                    handle_next_event(signaling_sink, signaling_stream, stop_rx).await
                });
            }
            WaitingRoomCommandOutput::PingReceived {
                mut signaling_sink,
                signaling_stream,
                stop_rx,
                data,
            } => {
                sender.oneshot_command(async move {
                    signaling_sink.send_pong(data).await.unwrap();
                    handle_next_event(signaling_sink, signaling_stream, stop_rx).await
                });
            }
            WaitingRoomCommandOutput::EventReceived {
                signaling_sink,
                signaling_stream,
                stop_rx,
                event,
            } => {
                if let SignalingEvent::Moderation(ModerationEvent::Accepted) = event {
                    info!("Accepted into room");
                    self.stop_rx = Some(stop_rx);
                    sender
                        .output(WaitingRoomOutput::Accepted {
                            signaling_sink,
                            signaling_stream,
                        })
                        .unwrap();
                } else {
                    warn!("Received unexpected event {event:?} while in waiting room, not doing anything");
                    sender.oneshot_command(async move {
                        handle_next_event(signaling_sink, signaling_stream, stop_rx).await
                    });
                }
            }
            WaitingRoomCommandOutput::Closed {
                signaling_sink,
                signaling_stream,
                stop_rx,
            } => {
                self.stop_rx = Some(stop_rx);
                drop(signaling_stream);
                drop(signaling_sink);
                sender.output(WaitingRoomOutput::Disconnected).unwrap();
            }
            WaitingRoomCommandOutput::StoppedListening { stop_rx } => {
                self.stop_rx = Some(stop_rx);
                sender.output(WaitingRoomOutput::Return).unwrap();
            }
        }
    }
}

async fn handle_next_event(
    signaling_sink: SignalingSink,
    mut signaling_stream: SignalingStream,
    mut stop_rx: Receiver<()>,
) -> WaitingRoomCommandOutput {
    tokio::select! {
        _ = stop_rx.recv() => WaitingRoomCommandOutput::StoppedListening{stop_rx},
        ev = signaling_stream.get_next_event() => {
            let ev = ev.unwrap();
            match ev {
                None => WaitingRoomCommandOutput::Listen {
                    signaling_sink,
                    signaling_stream,
                    stop_rx,
                },
                Some(SignalingStreamEvent::Event(event)) => WaitingRoomCommandOutput::EventReceived {
                    signaling_sink,
                    signaling_stream,
                    stop_rx,
                    event,
                },
                Some(SignalingStreamEvent::Ping(data)) => WaitingRoomCommandOutput::PingReceived {
                    signaling_sink,
                    signaling_stream,
                    stop_rx,
                    data,
                },
                Some(SignalingStreamEvent::Close) => WaitingRoomCommandOutput::Closed {
                    signaling_sink,
                    signaling_stream,
                    stop_rx,
                },
            }
        }
    }
}
