// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::collections::HashSet;

use gtk::{
    glib,
    prelude::Cast,
    traits::{BoxExt as _, ButtonExt, EditableExt, EntryExt, OrientableExt as _, WidgetExt as _},
};
use log::warn;
use opentalk_types::{
    core::ParticipantId,
    signaling::{
        chat::{event::MessageSent, state::GroupHistory, Scope},
        moderation::KickScope,
        timer::{command as timer_command, TimerId},
    },
};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender,
};

use crate::{
    meeting::{
        chat::ChatOutput,
        controls::{ControlsInit, ControlsOutput},
        group_chat_list::{GroupChatListInit, GroupChatListOutput},
        participant_list::{
            ParticipantExtraWidget, ParticipantList, ParticipantListInit, ParticipantListOutput,
        },
        user_view::{UserViewInit, UserViewOutput},
        TIMER_STATE,
    },
    theme::Theme,
    timer_ext::{NormalTimerInfo, ThemedBreakInfo, TimerStartedExt},
};

use super::{
    chat::{Chat, ChatAction, ChatInit, ChatInput},
    controls::{Controls, ControlsInput},
    group_chat_list::{GroupChatList, GroupChatListInput},
    participant_list::ParticipantListInput,
    participants::Participants,
    user_view::{UserView, UserViewInput},
};

const ROOM_CHAT_LABEL: &'static str = "Chat";
const GROUP_CHAT_LABEL: &'static str = "Group chat";

pub struct Sidebar {
    participants: Participants,
    self_view: AsyncController<UserView>,
    controls: AsyncController<Controls>,
    chat: AsyncController<Chat>,
    group_chats: AsyncController<GroupChatList>,
    participants_widget: AsyncController<ParticipantList>,
    theme: Theme,
    break_duration_selection_model: gtk::StringList,
    themed_break_duration_selection: Option<u64>,
    themed_break_duration_custom_value: u64,
    timer_duration_selection_model: gtk::StringList,
    normal_timer_duration_selection: Option<u64>,
    normal_timer_duration_custom_value: u64,
    normal_timer_title: String,
    normal_timer_ready_check_enabled: bool,
    normal_timer_participant_list: AsyncController<ParticipantList>,
    timer_active: bool,
    normal_timer_info: Option<NormalTimerInfo>,
    themed_break_info: Option<ThemedBreakInfo>,
    protocol_participant_list: AsyncController<ParticipantList>,
    current_homeview_page: HomeviewPage,
    room_chat_unread_count: usize,
    room_chat_label: String,
    group_chat_label: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum HomeviewPage {
    RoomChat,
    Participants,
    GroupChat,
}

#[derive(Debug)]
pub enum SidebarInput {
    ParticipantJoined(ParticipantId),
    ParticipantLeft(ParticipantId),
    ParticipantUpdated(ParticipantId),
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessage {
        message: MessageSent,
    },
    RaiseHandsEnableChanged {
        enabled: bool,
    },
    RaisedHandsReset,
    ResetRaisedHands,
    Shutdown(KickScope),
    SwitchTheme(Theme),
    ThemedBreakDurationSelected(Option<u64>),
    ThemedBreakDurationCustomValueChanged(u64),
    StartThemedBreak,
    NormalTimerDurationSelected(Option<u64>),
    NormalTimerDurationCustomValueChanged(u64),
    SetNormalTimerTitle(String),
    SetNormalTimerReadyCheckEnabled(bool),
    StartNormalTimer,
    StopTimer,
    TimerChanged,
    UnreadCountWasReset {
        target: ParticipantId,
    },
    SendChatMessage {
        content: String,
        scope: Scope,
    },
    GroupUnreadCountChanged {
        count: usize,
    },
    HomeviewPageSwitched(HomeviewPage),
    TimerReadyStatusUpdated {
        timer_id: TimerId,
        participant_id: ParticipantId,
        ready: bool,
    },
}

#[derive(Debug)]
pub enum SidebarOutput {
    LeaveRoom,
    RaiseHand,
    LowerHand,
    ResetRaisedHands,
    Shutdown(KickScope),
    SendChatMessage {
        content: String,
        scope: Scope,
    },
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    StartTimer {
        kind: timer_command::Kind,
        title: Option<String>,
        style: Option<String>,
        enable_ready_check: bool,
    },
    StopTimer,
    ResetUnreadCount {
        target: ParticipantId,
    },
    ChangeProtocolWritePermissions {
        target: ParticipantId,
        enabled: bool,
    },
}

pub struct SidebarInit {
    pub participants: Participants,
    pub room_chat: ChatInit,
    pub group_chat: Vec<GroupHistory>,
    pub raise_hands_enabled: bool,
    pub theme: Theme,
}

const THEMED_BREAK_DURATION_LIST: [Option<u64>; 5] = [Some(5), Some(10), Some(15), Some(30), None];
const NORMAL_TIMER_DURATION_LIST: [Option<u64>; 6] =
    [Some(u64::MAX), Some(5), Some(10), Some(15), Some(30), None];

#[relm4::component(async, pub)]
impl AsyncComponent for Sidebar {
    type Init = SidebarInit;
    type Input = SidebarInput;
    type Output = SidebarOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,
            set_vexpand: true,
            set_hexpand: false,
            set_halign: gtk::Align::Start,

            gtk::Revealer {
                #[watch]
                set_reveal_child: model.participants.current_participant_is_moderator(),
                set_transition_type: gtk::RevealerTransitionType::SlideRight,

                gtk::Box {
                    set_orientation: gtk::Orientation::Vertical,

                    gtk::StackSwitcher {
                        set_orientation: gtk::Orientation::Vertical,
                        add_css_class: "toolbar",
                        add_css_class: "flat",
                        set_stack: Some(&tools),
                    },
                },
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                add_css_class: "sidebar",

                model.self_view.widget(),
                model.controls.widget(),
                #[name="tools"]
                gtk::Stack {
                    add_named: (&homeview, Some("homeview")),
                    add_named: (&raisedhands, Some("raisedhands")),
                    add_named: (&shutdown_meeting, Some("shutdown_meeting")),
                    add_named: (&timer, Some("timer")),
                    add_named: (&themedbreak, Some("themedbreak")),
                    add_named: (&protocol, Some("protocol")),

                    #[watch]
                    update_child_icon: ("homeview", model.theme.icon_name("home", "home-dark")),
                    #[watch]
                    update_child_icon: ("raisedhands", model.theme.icon_name("hand-disabled", "hand-disabled-dark")),
                    #[watch]
                    update_child_icon: ("shutdown_meeting", model.theme.icon_name("exit", "exit-dark")),
                    #[watch]
                    update_child_icon: ("timer", model.theme.icon_name("countdown", "countdown-dark")),
                    #[watch]
                    update_child_icon: ("themedbreak", model.theme.icon_name("beverage", "beverage-dark")),
                    #[watch]
                    update_child_icon: ("protocol", model.theme.icon_name("protocol", "protocol-dark")),
                    #[watch]
                    set_visible_child_if: (!model.participants.current_participant_is_moderator(), "homeview"),
                },
            }
        },

        #[name = "homeview"]
        gtk::Notebook {
            set_vexpand: true,
            append_page: (&chat_box.clone(), Some(&chat_tab_label)),
            append_page: (participant_box, Some(&people_tab_label)),
            append_page_conditionally: (has_groups, group_chats_box, Some(&group_chats_tab_label)),
            connect_switch_page[sender] => move |_notebook, widget, _index| {
                let sender = sender.input_sender();
                if widget == &chat_widget {
                    sender.send(SidebarInput::HomeviewPageSwitched(HomeviewPage::RoomChat)).ok();
                } else if widget == &participant_widget {
                    sender.send(SidebarInput::HomeviewPageSwitched(HomeviewPage::Participants)).ok();
                } else if widget == &group_chats_widget {
                    sender.send(SidebarInput::HomeviewPageSwitched(HomeviewPage::GroupChat)).ok();
                }
            },
        },

        #[name = "chat_tab_label"]
        adw::ButtonContent {
            #[watch]
            set_icon_name: model.theme.icon_name("chat", "chat-dark"),
            #[watch]
            set_label: &model.room_chat_label,
        },

        #[name = "people_tab_label"]
        adw::ButtonContent {
            #[watch]
            set_icon_name: model.theme.icon_name("person", "person-dark"),
            set_label: "People",
        },

        #[name = "group_chats_tab_label"]
        adw::ButtonContent {
            #[watch]
            set_icon_name: model.theme.icon_name("chat", "chat-dark"),
            #[watch]
            set_label: &model.group_chat_label,
        },

        #[name = "raisedhands"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_vexpand: true,
            set_spacing: 10,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 10,
            set_margin_end: 10,

            gtk::Label {
                set_label: "Reset raised hands",
                add_css_class: "title-2",
            },

            gtk::Button {
                set_label: "All",
                add_css_class: "pill",
                add_css_class: "suggested-action",
                connect_clicked => SidebarInput::ResetRaisedHands,
            },
        },

        #[name = "shutdown_meeting"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_vexpand: true,
            set_spacing: 10,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 10,
            set_margin_end: 10,

            gtk::Label {
                set_label: "Shutdown meeting",
                add_css_class: "title-2",
            },

            gtk::Button {
                set_label: "Shutdown for everybody",
                add_css_class: "pill",
                add_css_class: "suggested-action",
                connect_clicked => SidebarInput::Shutdown(KickScope::All),
            },

            gtk::Button {
                set_label: "Shutdown (moderators stay)",
                add_css_class: "pill",
                add_css_class: "suggested-action",
                connect_clicked => SidebarInput::Shutdown(KickScope::UsersAndGuests),
            },

            gtk::Button {
                set_label: "Shutdown (registered users stay)",
                add_css_class: "pill",
                add_css_class: "suggested-action",
                connect_clicked => SidebarInput::Shutdown(KickScope::Guests),
            },
        },

        #[name = "timer"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            set_margin_start: 10,
            set_margin_end: 10,
            set_margin_bottom: 10,

            gtk::Separator,

            gtk::Label {
                set_label: "Timer",
                add_css_class: "title-2",
            },

            #[name = "start_timer"]
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 10,

                #[watch]
                set_visible: !model.timer_active,

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_spacing: 10,

                    gtk::Label {
                        set_label: "Duration: ",
                        set_hexpand: false,
                    },
                    gtk::DropDown {
                        set_hexpand: true,
                        set_model: Some(&model.timer_duration_selection_model),
                        set_selected: 1,
                        connect_selected_notify[sender] => move |v| {
                            let selected_index = v.selected();
                            let value = NORMAL_TIMER_DURATION_LIST.iter().cloned().nth(selected_index as usize).unwrap_or_default();
                            sender.input(SidebarInput::NormalTimerDurationSelected(value));
                        }
                    },
                    gtk::SpinButton {
                        #[watch]
                        set_visible: model.normal_timer_duration_selection.is_none(),
                        set_numeric: true,
                        set_range: (1.0, 1_000_000_000_000.0),
                        set_value: model.normal_timer_duration_custom_value as f64,
                        set_increments: (1.0, 10.0),
                        connect_value_changed[sender] => move |v| {
                          sender.input(SidebarInput::NormalTimerDurationCustomValueChanged(v.value_as_int() as u64));
                        },
                    },
                },

                gtk::Entry {
                    set_placeholder_text: Some("Title"),
                    set_text: model.normal_timer_title.as_str(),
                    connect_changed[sender] => move |entry| {
                        sender.input(SidebarInput::SetNormalTimerTitle(entry.text().to_string()));
                    }
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_spacing: 10,

                    gtk::Label {
                        set_text: "Ask participants if they are ready",
                    },

                    gtk::Switch {
                        set_active: model.normal_timer_ready_check_enabled,

                        connect_state_set[sender] => move |_switch, state| {
                            sender.input(SidebarInput::SetNormalTimerReadyCheckEnabled(state));
                            gtk::Inhibit(false)
                        }
                    }
                },

                gtk::Box {
                    set_vexpand: true,
                },

                gtk::Button {
                    set_label: "Start timer",
                    add_css_class: "pill",
                    add_css_class: "suggested-action",
                    connect_clicked => SidebarInput::StartNormalTimer,
                },
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,

                #[watch]
                set_visible: model.timer_active,

                gtk::Label {
                    set_hexpand: true,
                    set_label: "A timer is currently active",
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Vertical,

                    set_hexpand: true,
                    #[watch]
                    set_visible: model.normal_timer_info.is_some(),

                    gtk::Box {
                        set_vexpand: true,

                        gtk::Box {
                            #[watch]
                            set_visible: if let Some(t) = &model.normal_timer_info {
                                t.started.ready_check_enabled
                            } else {
                                false
                            },
                            model.normal_timer_participant_list.widget(),
                        },
                    },

                    gtk::Button {
                        set_hexpand: true,
                        set_label: "Stop timer",
                        add_css_class: "pill",
                        add_css_class: "destructive-action",
                        connect_clicked => SidebarInput::StopTimer,
                    },
                },
            },
        },

        #[name = "themedbreak"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            set_margin_start: 10,
            set_margin_end: 10,

            gtk::Separator,

            gtk::Label {
                set_label: "Tea and coffee break",
                add_css_class: "title-2",
            },

            #[name = "start_themed_break"]
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 10,

                #[watch]
                set_visible: !model.timer_active,

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_spacing: 10,

                    gtk::Label {
                        set_label: "Duration: ",
                        set_hexpand: false,
                    },
                    gtk::DropDown {
                        set_hexpand: true,
                        set_model: Some(&model.break_duration_selection_model),
                        connect_selected_notify[sender] => move |v| {
                            let selected_index = v.selected();
                            let value = THEMED_BREAK_DURATION_LIST.iter().cloned().nth(selected_index as usize).unwrap_or_default();
                            sender.input(SidebarInput::ThemedBreakDurationSelected(value));
                        }
                    },
                    gtk::SpinButton {
                        #[watch]
                        set_visible: model.themed_break_duration_selection.is_none(),
                        set_numeric: true,
                        set_range: (1.0, 1_000_000_000_000.0),
                        set_value: model.themed_break_duration_custom_value as f64,
                        set_increments: (1.0, 10.0),
                        connect_value_changed[sender] => move |v| {
                          sender.input(SidebarInput::ThemedBreakDurationCustomValueChanged(v.value_as_int() as u64));
                        },
                    },
                },

                gtk::Button {
                    set_label: "Start break",
                    add_css_class: "pill",
                    add_css_class: "suggested-action",
                    connect_clicked => SidebarInput::StartThemedBreak,
                },
            },

            #[name = "timer_active"]
            gtk::Box {
                set_hexpand: true,
                #[watch]
                set_visible: model.timer_active,
                gtk::Label {
                    set_hexpand: true,
                    set_label: "A timer is currently active",
                },
            },

            #[name = "themed_break_active"]
            gtk::Box {
                set_hexpand: true,
                #[watch]
                set_visible: model.themed_break_info.is_some(),

                gtk::Button {
                    set_hexpand: true,
                    set_label: "Stop break",
                    add_css_class: "pill",
                    add_css_class: "destructive-action",
                    connect_clicked => SidebarInput::StopTimer,
                },
            },
        },

        #[name = "protocol"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            set_margin_start: 10,
            set_margin_end: 10,

            gtk::Separator,

            gtk::Label {
                set_label: "Meeting protocol",
                add_css_class: "title-2",
            },

            model.protocol_participant_list.widget(),
        }
    }

    async fn init(
        SidebarInit {
            participants,
            room_chat,
            group_chat,
            raise_hands_enabled,
            theme,
        }: SidebarInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        TIMER_STATE.subscribe(sender.input_sender(), |_| SidebarInput::TimerChanged);
        let chat_enabled = room_chat.enabled;

        let self_view = UserView::builder()
            .launch(UserViewInit {
                participants: participants.clone(),
                participant_id: participants.current_participant_id(),
                theme,
                chat_enabled,
            })
            .forward(sender.output_sender(), |msg| match msg {
                UserViewOutput::GrantModeratorRights { target } => {
                    SidebarOutput::GrantModeratorRights { target }
                }
                UserViewOutput::RevokeModeratorRights { target } => {
                    SidebarOutput::RevokeModeratorRights { target }
                }
                UserViewOutput::SendMessage { scope, content } => {
                    SidebarOutput::SendChatMessage { scope, content }
                }
                UserViewOutput::ResetUnreadCount { target } => {
                    SidebarOutput::ResetUnreadCount { target }
                }
            });
        let controls = Controls::builder()
            .launch(ControlsInit {
                raise_hands_enabled,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ControlsOutput::LeaveRoom => SidebarOutput::LeaveRoom,
                ControlsOutput::RaiseHand => SidebarOutput::RaiseHand,
                ControlsOutput::LowerHand => SidebarOutput::LowerHand,
            });

        let chat =
            Chat::builder()
                .launch(room_chat)
                .forward(sender.output_sender(), |msg| match msg {
                    ChatOutput::SendMessage { scope, content } => {
                        SidebarOutput::SendChatMessage { scope, content }
                    }
                });
        let participants_widget = ParticipantList::builder()
            .launch(ParticipantListInit {
                extra_widgets: ParticipantExtraWidget::default_extra_widgets(),
                participants: participants.clone(),
                chat_enabled,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantListOutput::GrantModeratorRights { target } => {
                    SidebarOutput::GrantModeratorRights { target }
                }
                ParticipantListOutput::RevokeModeratorRights { target } => {
                    SidebarOutput::RevokeModeratorRights { target }
                }
                ParticipantListOutput::SendMessage { scope, content } => {
                    SidebarOutput::SendChatMessage { scope, content }
                }
                ParticipantListOutput::ResetUnreadCount { target } => {
                    SidebarOutput::ResetUnreadCount { target }
                }
                ParticipantListOutput::ChangeProtocolWritePermissions { target, enabled } => {
                    SidebarOutput::ChangeProtocolWritePermissions { target, enabled }
                }
            });
        let has_groups = !group_chat.is_empty();
        let group_chats = GroupChatList::builder()
            .launch(GroupChatListInit {
                participants: participants.clone(),
                theme,
                groups: group_chat,
                enabled: chat_enabled,
            })
            .forward(sender.input_sender(), |msg| match msg {
                GroupChatListOutput::SendMessage { scope, content } => {
                    SidebarInput::SendChatMessage { content, scope }
                }
                GroupChatListOutput::UnreadCountChanged { count } => {
                    SidebarInput::GroupUnreadCountChanged { count }
                }
            });

        let iter = THEMED_BREAK_DURATION_LIST.iter().map(|d| {
            if let Some(minutes) = d {
                format!("{minutes} minutes")
            } else {
                "custom minutes".to_string()
            }
        });
        let break_duration_selection_model = gtk::StringList::from_iter(iter);

        let iter = NORMAL_TIMER_DURATION_LIST.iter().map(|d| {
            if let Some(minutes) = d {
                if *minutes == u64::MAX {
                    "unlimited".to_string()
                } else {
                    format!("{minutes} minutes")
                }
            } else {
                "custom minutes".to_string()
            }
        });
        let timer_duration_selection_model = gtk::StringList::from_iter(iter);

        let (timer_active, normal_timer_info, themed_break_info) =
            if let Some(timer) = &*TIMER_STATE.read() {
                (true, timer.normal_timer_info(), timer.themed_break_info())
            } else {
                (false, None, None)
            };

        let normal_timer_participant_list = ParticipantList::builder()
            .launch(ParticipantListInit {
                extra_widgets: HashSet::from([ParticipantExtraWidget::TimerReadyState]),
                participants: participants.clone(),
                chat_enabled,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantListOutput::GrantModeratorRights { target } => {
                    SidebarOutput::GrantModeratorRights { target }
                }
                ParticipantListOutput::RevokeModeratorRights { target } => {
                    SidebarOutput::RevokeModeratorRights { target }
                }
                ParticipantListOutput::SendMessage { scope, content } => {
                    SidebarOutput::SendChatMessage { scope, content }
                }
                ParticipantListOutput::ResetUnreadCount { target } => {
                    SidebarOutput::ResetUnreadCount { target }
                }
                ParticipantListOutput::ChangeProtocolWritePermissions { target, enabled } => {
                    SidebarOutput::ChangeProtocolWritePermissions { target, enabled }
                }
            });

        let protocol_participant_list = ParticipantList::builder()
            .launch(ParticipantListInit {
                extra_widgets: [ParticipantExtraWidget::ProtocolPermissions].into(),
                participants: participants.clone(),
                chat_enabled,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantListOutput::GrantModeratorRights { target } => {
                    SidebarOutput::GrantModeratorRights { target }
                }
                ParticipantListOutput::RevokeModeratorRights { target } => {
                    SidebarOutput::RevokeModeratorRights { target }
                }
                ParticipantListOutput::SendMessage { scope, content } => {
                    SidebarOutput::SendChatMessage { scope, content }
                }
                ParticipantListOutput::ResetUnreadCount { target } => {
                    SidebarOutput::ResetUnreadCount { target }
                }
                ParticipantListOutput::ChangeProtocolWritePermissions { target, enabled } => {
                    SidebarOutput::ChangeProtocolWritePermissions { target, enabled }
                }
            });

        let model = Sidebar {
            participants,
            self_view,
            controls,
            participants_widget,
            chat,
            group_chats,
            theme,
            break_duration_selection_model,
            timer_duration_selection_model,
            themed_break_duration_selection: Some(5),
            themed_break_duration_custom_value: 5,
            normal_timer_duration_selection: Some(5),
            normal_timer_duration_custom_value: 5,
            normal_timer_title: "".to_string(),
            normal_timer_ready_check_enabled: true,
            normal_timer_participant_list,
            timer_active,
            normal_timer_info,
            themed_break_info,
            protocol_participant_list,
            current_homeview_page: HomeviewPage::RoomChat,
            room_chat_unread_count: 0,
            room_chat_label: ROOM_CHAT_LABEL.to_string(),
            group_chat_label: GROUP_CHAT_LABEL.to_string(),
        };

        let chat_box = model.chat.widget().clone();

        let participant_box = model.participants_widget.widget();
        participant_box.set_orientation(gtk::Orientation::Vertical);
        participant_box.set_spacing(5);

        let group_chats_box = model.group_chats.widget();

        let chat_widget = chat_box.clone().upcast::<gtk::Widget>();
        let participant_widget = participant_box.clone().upcast::<gtk::Widget>();
        let group_chats_widget = group_chats_box.clone().upcast::<gtk::Widget>();
        let widgets = view_output!();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: SidebarInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            SidebarInput::ParticipantJoined(participant_id) => {
                self.chat.emit(ChatInput::Action(ChatAction::Join {
                    participant_id,
                    display_name: self.participants.display_name_or_default(participant_id),
                }));
                self.participants_widget
                    .emit(ParticipantListInput::ParticipantJoined(participant_id));
                self.normal_timer_participant_list
                    .emit(ParticipantListInput::ParticipantJoined(participant_id));
                self.protocol_participant_list
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
            }
            SidebarInput::ParticipantLeft(participant_id) => {
                self.chat.emit(ChatInput::Action(ChatAction::Leave {
                    participant_id,
                    display_name: self.participants.display_name_or_default(participant_id),
                }));
                self.participants_widget
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
                self.normal_timer_participant_list
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
                self.protocol_participant_list
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
            }
            SidebarInput::ParticipantUpdated(participant_id) => {
                self.participants_widget
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
                self.normal_timer_participant_list
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
                self.protocol_participant_list
                    .emit(ParticipantListInput::ParticipantUpdated(participant_id));
            }
            SidebarInput::ChatEnableChanged { enabled, issued_by } => {
                let action = ChatAction::ChatEnableChanged {
                    issued_by,
                    display_name: issued_by
                        .as_ref()
                        .map(|p| self.participants.display_name_or_default(*p))
                        .unwrap_or("unknown".to_string()),
                    enabled,
                };
                self.chat.emit(ChatInput::Action(action.clone()));
                self.group_chats.emit(GroupChatListInput::Action {
                    group: None,
                    action,
                });
                self.participants_widget
                    .emit(ParticipantListInput::ChatEnableChanged { issued_by, enabled });
            }
            SidebarInput::ChatMessage { message } => match message.scope {
                Scope::Global => {
                    self.chat.emit(ChatInput::Action(ChatAction::Message {
                        participant_id: message.source,
                        display_name: self.participants.display_name_or_default(message.source),
                        content: message.content,
                        is_moderator: self.participants.is_moderator(message.source),
                    }));
                    if self.current_homeview_page != HomeviewPage::RoomChat {
                        self.room_chat_unread_count = self.room_chat_unread_count.saturating_add(1);
                        self.room_chat_label =
                            format!("{ROOM_CHAT_LABEL} ({})", self.room_chat_unread_count);
                    }
                }
                Scope::Group(group) => {
                    self.group_chats.emit(GroupChatListInput::Action {
                        group: Some(group),
                        action: ChatAction::Message {
                            participant_id: message.source,
                            display_name: self.participants.display_name_or_default(message.source),
                            content: message.content,
                            is_moderator: self.participants.is_moderator(message.source),
                        },
                    });
                }
                Scope::Private(_target) => {
                    self.participants_widget
                        .emit(ParticipantListInput::ChatMessageSent { message });
                }
            },
            SidebarInput::RaiseHandsEnableChanged { enabled } => {
                self.controls
                    .emit(ControlsInput::RaiseHandsEnableChanged { enabled });
            }
            SidebarInput::RaisedHandsReset => {
                self.controls.emit(ControlsInput::RaisedHandsReset);
            }
            SidebarInput::ResetRaisedHands => {
                sender.output(SidebarOutput::ResetRaisedHands).unwrap();
            }
            SidebarInput::Shutdown(kick_scope) => {
                sender.output(SidebarOutput::Shutdown(kick_scope)).unwrap();
            }
            SidebarInput::SwitchTheme(theme) => {
                self.theme = theme;
                self.controls.emit(ControlsInput::SwitchTheme(theme));
                self.self_view.emit(UserViewInput::SwitchTheme(theme));
                self.chat.emit(ChatInput::SwitchTheme(theme));
                self.participants_widget
                    .emit(ParticipantListInput::SwitchTheme(theme));
                self.group_chats
                    .emit(GroupChatListInput::SwitchTheme(theme));
            }
            SidebarInput::NormalTimerDurationSelected(optional_minutes) => {
                self.normal_timer_duration_selection = optional_minutes;
            }

            SidebarInput::NormalTimerDurationCustomValueChanged(minutes) => {
                self.normal_timer_duration_custom_value = minutes;
            }
            SidebarInput::SetNormalTimerTitle(title) => {
                self.normal_timer_title = title;
            }
            SidebarInput::SetNormalTimerReadyCheckEnabled(enabled) => {
                self.normal_timer_ready_check_enabled = enabled;
            }
            SidebarInput::StartNormalTimer => {
                let minutes = self
                    .normal_timer_duration_selection
                    .unwrap_or(self.normal_timer_duration_custom_value);
                let kind = if minutes == u64::MAX {
                    timer_command::Kind::Stopwatch
                } else {
                    timer_command::Kind::Countdown {
                        duration: minutes * 60,
                    }
                };
                sender
                    .output(SidebarOutput::StartTimer {
                        kind,
                        title: if self.normal_timer_title.is_empty() {
                            None
                        } else {
                            Some(self.normal_timer_title.to_string())
                        },
                        style: Some("normal".to_string()),
                        enable_ready_check: self.normal_timer_ready_check_enabled,
                    })
                    .unwrap();
            }
            SidebarInput::ThemedBreakDurationSelected(optional_minutes) => {
                self.themed_break_duration_selection = optional_minutes;
            }

            SidebarInput::ThemedBreakDurationCustomValueChanged(minutes) => {
                self.themed_break_duration_custom_value = minutes;
            }
            SidebarInput::StartThemedBreak => {
                let minutes = self
                    .themed_break_duration_selection
                    .unwrap_or(self.themed_break_duration_custom_value);
                sender
                    .output(SidebarOutput::StartTimer {
                        kind: timer_command::Kind::Countdown {
                            duration: minutes * 60,
                        },
                        title: None,
                        style: Some("coffee-break".to_string()),
                        enable_ready_check: false,
                    })
                    .unwrap();
            }
            SidebarInput::StopTimer => {
                sender.output(SidebarOutput::StopTimer).unwrap();
            }
            SidebarInput::TimerChanged => {
                let (timer_active, normal_timer_info, themed_break_info) =
                    if let Some(timer) = &*TIMER_STATE.read() {
                        (true, timer.normal_timer_info(), timer.themed_break_info())
                    } else {
                        (false, None, None)
                    };
                self.timer_active = timer_active;
                self.normal_timer_info = normal_timer_info;
                self.themed_break_info = themed_break_info;
            }
            SidebarInput::UnreadCountWasReset { target } => {
                self.participants_widget
                    .emit(ParticipantListInput::UnreadCountWasReset { target });
            }
            SidebarInput::SendChatMessage { content, scope } => {
                sender
                    .output(SidebarOutput::SendChatMessage { content, scope })
                    .unwrap();
            }
            SidebarInput::GroupUnreadCountChanged { count } => {
                self.group_chat_label = if count == 0 {
                    GROUP_CHAT_LABEL.to_string()
                } else {
                    format!("{GROUP_CHAT_LABEL} ({count})")
                }
            }
            SidebarInput::HomeviewPageSwitched(page) => {
                self.current_homeview_page = page;

                if page == HomeviewPage::RoomChat {
                    self.room_chat_unread_count = 0;
                    self.room_chat_label = ROOM_CHAT_LABEL.to_string();
                }

                if page == HomeviewPage::GroupChat {
                    self.group_chats.emit(GroupChatListInput::IsShown);
                } else {
                    self.group_chats.emit(GroupChatListInput::IsHidden);
                }
            }
            SidebarInput::TimerReadyStatusUpdated {
                timer_id,
                participant_id,
                ready,
            } => {
                if let Some(ref t) = self.normal_timer_info {
                    if t.started.timer_id == timer_id {
                        self.normal_timer_participant_list.emit(
                            ParticipantListInput::TimerReadyStatusUpdated {
                                participant_id,
                                ready,
                            },
                        );
                    } else {
                        warn!("Received timer status update while no timer is running");
                    }
                }
            }
        }
    }
}

trait StackExt {
    fn update_child_icon(&self, child_name: &str, icon_name: &str);
    fn set_visible_child_if(&self, condition: bool, child_name: &str);
}

impl StackExt for gtk::Stack {
    fn update_child_icon(&self, child_name: &str, icon_name: &str) {
        let child = self.child_by_name(child_name).unwrap();
        self.page(&child).set_icon_name(icon_name);
    }

    fn set_visible_child_if(&self, condition: bool, child_name: &str) {
        if condition {
            self.set_visible_child_name(child_name);
        }
    }
}

trait NotebookExt {
    fn append_page_conditionally(
        &self,
        condition: bool,
        child: &impl glib::IsA<gtk::Widget>,
        tab_label: Option<&impl glib::IsA<gtk::Widget>>,
    ) -> Option<u32>;
}

impl NotebookExt for gtk::Notebook {
    fn append_page_conditionally(
        &self,
        condition: bool,
        child: &impl glib::IsA<gtk::Widget>,
        tab_label: Option<&impl glib::IsA<gtk::Widget>>,
    ) -> Option<u32> {
        if condition {
            Some(self.append_page(child, tab_label))
        } else {
            None
        }
    }
}
