// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use opentalk_types::{
    core::ParticipantId,
    signaling::control::{AssociatedParticipant, Participant},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    factory::AsyncFactoryVecDeque,
    gtk, AsyncComponentSender,
};

use crate::meeting::waiting_participants_list::entry::WaitingParticipantEntryInit;

use self::entry::WaitingParticipantEntry;

mod entry;

#[derive(Debug)]
pub struct WaitingParticipantsListInit {
    pub participants: Vec<Participant>,
}

#[derive(Debug)]
pub enum WaitingParticipantsListInput {
    AddParticipant(Participant),
    RemoveParticipant(AssociatedParticipant),
    Accept { target: ParticipantId },
}

#[derive(Debug)]
pub enum WaitingParticipantsListOutput {
    Accept { target: ParticipantId },
}

pub struct WaitingParticipantsList {
    participants: AsyncFactoryVecDeque<WaitingParticipantEntry>,
}

#[relm4::component(async, pub)]
impl AsyncComponent for WaitingParticipantsList {
    type Init = WaitingParticipantsListInit;
    type Input = WaitingParticipantsListInput;
    type Output = WaitingParticipantsListOutput;
    type CommandOutput = ();

    view! {
        #[root]
        #[name = "scroll"]
        gtk::ScrolledWindow {
            set_propagate_natural_width: true,
            set_propagate_natural_height: true,

            model.participants.widget(),
        }
    }

    async fn init(
        init: WaitingParticipantsListInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let mut participants = AsyncFactoryVecDeque::new(
            gtk::Box::builder()
                .orientation(gtk::Orientation::Vertical)
                .spacing(10)
                .margin_start(5)
                .margin_end(5)
                .margin_top(5)
                .margin_bottom(5)
                .build(),
            sender.input_sender(),
        );

        {
            let mut guard = participants.guard();
            for participant in init.participants.iter().cloned() {
                guard.push_back(WaitingParticipantEntryInit { participant });
            }
        }

        let model = Self { participants };

        let widgets = view_output!();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            WaitingParticipantsListInput::AddParticipant(participant) => {
                let entry = WaitingParticipantEntryInit { participant };
                let mut guard = self.participants.guard();
                guard.push_back(entry);
            }
            WaitingParticipantsListInput::RemoveParticipant(associated_participant) => {
                while let Some(index) = self.get_participant_list_index(associated_participant.id) {
                    let mut guard = self.participants.guard();
                    guard.remove(index);
                }
            }
            WaitingParticipantsListInput::Accept { target } => {
                sender
                    .output(WaitingParticipantsListOutput::Accept { target })
                    .unwrap();
            }
        }
    }
}

impl WaitingParticipantsList {
    fn get_participant_list_index(&self, id: ParticipantId) -> Option<usize> {
        self.participants.iter().position(|p| {
            matches!(
                p,
                Some(p) if p.is(id)
            )
        })
    }
}
