// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::{
    glib,
    traits::{BoxExt, ButtonExt, EditableExt, PopoverExt, WidgetExt as _},
};
use opentalk_types::{
    common::shared_folder::{SharedFolder, SharedFolderAccess},
    core::{ParticipantId, Timestamp},
    signaling::{
        control::{AssociatedParticipant, Participant},
        timer::{event as timer_event, TimerId},
    },
};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk::{self, traits::OrientableExt as _},
    AsyncComponentSender, RelmWidgetExt,
};

use crate::{
    meeting::{
        waiting_participants_list::{
            WaitingParticipantsList, WaitingParticipantsListInit, WaitingParticipantsListOutput,
        },
        TIMER_STATE,
    },
    theme::Theme,
    timer_ext::{NormalTimerInfo, ThemedBreakInfo, TimerStartedExt as _},
    utils::{build_duration_label, TimeResolution},
};

use self::view_switcher::ViewSwitcher;

use super::waiting_participants_list::WaitingParticipantsListInput;

mod view_switcher;

#[derive(Debug)]
pub enum Protocol {
    Read(String),
    Write(String),
}

pub struct MeetingHeader {
    my_id: ParticipantId,
    view_switcher: AsyncController<ViewSwitcher>,
    waiting_participants: AsyncController<WaitingParticipantsList>,
    meeting_start_time: Timestamp,
    duration_label: String,
    theme: Theme,
    normal_timer: Option<NormalTimerInfo>,
    themed_break: Option<ThemedBreakInfo>,
    waiting_participants_count: usize,
    shared_folder: Option<SharedFolder>,
    protocol: Option<Protocol>,
}

#[derive(Debug)]
pub struct ThemedBreakInit {
    pub timer_id: TimerId,
    pub ends_at: Timestamp,
}

#[derive(Debug)]
pub struct MeetingHeaderInit {
    pub my_id: ParticipantId,
    pub meeting_start_time: Timestamp,
    pub theme: Theme,
    pub waiting_participants: Vec<Participant>,
    pub shared_folder: Option<SharedFolder>,
    pub protocol: Option<Protocol>,
}

#[derive(Debug)]
pub enum MeetingHeaderInput {
    Tick,
    SwitchTheme(Theme),
    ThemedBreakButtonActivated,
    TimerChanged,
    ParticipantJoinedWaitingRoom(Participant),
    ParticipantLeftWaitingRoom(AssociatedParticipant),
    AcceptParticipantFromWaitingRoom {
        target: ParticipantId,
    },
    CopySharedFolderReadPassword,
    CopySharedFolderWritePassword,
    UpdateTimerReadyStatus {
        ready: bool,
    },
    TimerReadyStatusUpdated {
        timer_id: TimerId,
        participant_id: ParticipantId,
        ready: bool,
    },
    UpdateProtocol(Protocol),
    OpenProtocolInBrowser,
    CopyProtocolUrl,
}

#[derive(Debug)]
pub enum MeetingHeaderOutput {
    ShowThemedBreak,
    AcceptParticipantFromWaitingRoom { target: ParticipantId },
    UpdateTimerReadyStatus { timer_id: TimerId, ready: bool },
}

#[relm4::component(async, pub)]
impl AsyncComponent for MeetingHeader {
    type Init = MeetingHeaderInit;
    type Input = MeetingHeaderInput;
    type Output = MeetingHeaderOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Horizontal,
            set_halign: gtk::Align::Fill,
            set_hexpand: true,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 10,
            set_margin_end: 10,

            set_start_widget: Some(&gtk::Label::builder().label("Concilium").css_classes(["title-1"]).build()),
            set_center_widget: Some(model.view_switcher.widget()),
            set_end_widget: Some(&infoarea),
        },

        #[name = "normal_timer_button_content"]
        adw::ButtonContent {
            #[watch]
            set_label: model.normal_timer.as_ref().map(|t| t.duration_label.as_str()).unwrap_or_default(),

            #[watch]
            set_icon_name: match model.normal_timer.as_ref().map(|t| t.started.kind) {
                Some(timer_event::Kind::Stopwatch) => {
                    model.theme.icon_name("stopwatch", "stopwatch-dark")
                }
                Some(timer_event::Kind::Countdown{..}) => {
                    model.theme.icon_name("countdown", "countdown-dark")
                }
                None => ""
            },
        },

        #[name = "normal_timer_popover"]
        gtk::Popover {
            set_child: Some(&normal_timer_contents)
        },

        #[name = "normal_timer_contents"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            gtk::Label {
                #[watch]
                set_label: model.normal_timer.as_ref().map(NormalTimerInfo::title).unwrap_or_default(),
                add_css_class: "title-2",
                set_xalign: 0f32,
            },

            gtk::Label {
                #[watch]
                set_label: model.normal_timer.as_ref().map(|t| t.duration_name).unwrap_or_default(),
                set_xalign: 0f32,
            },

            gtk::Label {
                #[watch]
                set_label: model.normal_timer.as_ref().map(|t| t.duration_label.as_str()).unwrap_or_default(),
                add_css_class: "heading",
                set_xalign: 0f32,
            },

            gtk::Button {
                #[watch]
                set_visible: model
                    .normal_timer
                    .as_ref()
                    .map(|t| t.started.ready_check_enabled && !t.participant_marked_as_done)
                    .unwrap_or_default(),

                add_css_class: "suggested-action",
                set_label: "Mark me as done",

                connect_clicked[sender] => move |_button| {
                    sender.input(MeetingHeaderInput::UpdateTimerReadyStatus{ready: true});
                }
            },

            gtk::Button {
                #[watch]
                set_visible: model
                    .normal_timer
                    .as_ref()
                    .map(|t| t.started.ready_check_enabled && t.participant_marked_as_done)
                    .unwrap_or_default(),

                set_label: "Unmark me as done",

                connect_clicked[sender] => move |_button| {
                    sender.input(MeetingHeaderInput::UpdateTimerReadyStatus{ready: false});
                }
            }
        },

        #[name = "waiting_room_button_content"]
        adw::ButtonContent {
            #[watch]
            set_icon_name: model.theme.icon_name("waiting-room", "waiting-room-dark"),

            #[watch]
            set_label: &format!("{}", model.waiting_participants_count),
        },

        #[name = "waiting_room_popover"]
        gtk::Popover {
            set_child: Some(model.waiting_participants.widget()),
        },

        #[name = "protocol_popover"]
        gtk::Popover {
            set_child: Some(&protocol_contents)
        },

        #[name = "protocol_contents"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            gtk::Label {
                #[watch]
                set_label: if matches!(model.protocol, Some(Protocol::Write{..})) { "Write access" } else { "Read access" },
                add_css_class: "heading",
                set_xalign: 0f32,
            },

            gtk::Separator,

            gtk::Label {
                set_label: "Open this URL in a browser to access the protocol",
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_spacing: 10,

                gtk::Entry {
                    #[watch]
                    set_text: model.protocol_url(),
                    set_editable: false,
                    set_hexpand: true,
                    set_xalign: 0f32,
                },

                gtk::Button {
                    #[watch]
                    set_icon_name: model.theme.icon_name("launch", "launch-dark"),
                    set_tooltip: "Open URL in browser",

                    connect_clicked => MeetingHeaderInput::OpenProtocolInBrowser,
                },

                gtk::Button {
                    set_icon_name: "edit-copy-symbolic",
                    set_tooltip: "Copy URL to clipboard",

                    connect_clicked => MeetingHeaderInput::CopyProtocolUrl,
                },
            },
        },

        #[name = "shared_folder_popover"]
        gtk::Popover {
            set_child: Some(&shared_folder_contents),
        },

        #[name = "shared_folder_contents"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            gtk::Label {
                set_label: "Read access",
                add_css_class: "heading",
                set_xalign: 0f32,
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_spacing: 10,
                gtk::Label {
                    set_label: "URL:"
                },
                gtk::Label {
                    #[watch]
                    set_markup: &format!("<a href=\"{0}\">{0}</a>", model.shared_folder_read().map(|f|f.url.as_str()).unwrap_or_default()),
                    set_hexpand: true,
                    set_xalign: 0f32,
                },
                gtk::Separator,
                gtk::Label {
                    set_label: "Password: "
                },
                gtk::PasswordEntry {
                    set_show_peek_icon: true,
                    set_editable: false,
                    #[watch]
                    set_text:  model.shared_folder_read().map(|f| f.password.as_str()).unwrap_or_default(),
                },
                gtk::Button {
                    set_icon_name: "edit-copy-symbolic",
                    set_tooltip: "Copy password to clipboard",

                    connect_clicked => MeetingHeaderInput::CopySharedFolderReadPassword,
                },
            },

            gtk::Separator {
                #[watch]
                set_visible: model.has_shared_folder_write(),
            },

            gtk::Label {
                set_label: "Write access (don't share with others)",
                add_css_class: "heading",
                set_xalign: 0f32,

                #[watch]
                set_visible: model.has_shared_folder_write(),
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_spacing: 10,

                #[watch]
                set_visible: model.has_shared_folder_write(),

                gtk::Label {
                    set_label: "URL:"
                },
                gtk::Label {
                    #[watch]
                    set_markup: &format!("<a href=\"{0}\">{0}</a>", model.shared_folder_write().map(|f|f.url.as_str()).unwrap_or_default()),
                    set_hexpand: true,
                    set_xalign: 0f32,
                },
                gtk::Separator,
                gtk::Label {
                    set_label: "Password: "
                },
                gtk::PasswordEntry {
                    set_show_peek_icon: true,
                    set_editable: false,
                    #[watch]
                    set_text:  model.shared_folder_write().map(|f| f.password.as_str()).unwrap_or_default(),
                },
                gtk::Button {
                    set_icon_name: "edit-copy-symbolic",
                    set_tooltip: "Copy password to clipboard",

                    connect_clicked => MeetingHeaderInput::CopySharedFolderWritePassword,
                },
            },
        },

        #[name = "infoarea"]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,
            set_spacing: 10,

            #[name = "normal_timer"]
            gtk::MenuButton {
                #[watch]
                set_visible: model.normal_timer.is_some(),

                set_always_show_arrow: true,
                set_popover: Some(&normal_timer_popover),

                add_css_class: "suggested-action",

                set_child: Some(&normal_timer_button_content),
            },

            #[name = "themed_break_timer"]
            gtk::Button {
                #[watch]
                set_visible: model.themed_break.is_some(),

                #[watch]
                add_css_class: if model.themed_break.as_ref().map(|b| b.is_alert).unwrap_or_default() {
                    "destructive-action"
                } else {
                    "suggested-action"
                },

                #[watch]
                remove_css_class: if model.themed_break.as_ref().map(|b| b.is_alert).unwrap_or_default() {
                    "suggested-action"
                } else {
                    "destructive-action"
                },


                adw::ButtonContent {
                    #[watch]
                    set_label: model.themed_break.as_ref().map(|b| b.duration_label.as_str()).unwrap_or(""),

                    #[watch]
                    set_icon_name: model.theme.icon_name("beverage", "beverage-dark"),
                },

                connect_clicked => MeetingHeaderInput::ThemedBreakButtonActivated,
            },

            #[name = "protocol_button"]
            gtk::MenuButton {
                #[watch]
                set_visible: model.has_protocol(),

                set_always_show_arrow: true,
                set_popover: Some(&protocol_popover),

                add_css_class: "suggested-action",

                #[watch]
                set_icon_name: model.theme.icon_name("protocol", "protocol-dark"),
            },

            #[name = "shared_folder_button"]
            gtk::MenuButton {
                #[watch]
                set_visible: model.has_shared_folder(),

                set_always_show_arrow: true,
                set_popover: Some(&shared_folder_popover),

                add_css_class: "suggested-action",

                #[watch]
                set_icon_name: model.theme.icon_name("shared-folder", "shared-folder-dark"),
            },

            #[name = "waiting_room_indicator"]
            gtk::MenuButton {
                set_popover: Some(&waiting_room_popover),
                set_tooltip: &format!("Participants in waiting room: {}", model.waiting_participants_count),
                set_always_show_arrow: true,

                #[watch]
                set_visible: model.waiting_participants_count > 0,

                add_css_class: "suggested-action",

                set_child: Some(&waiting_room_button_content),
            },

            #[name = "meeting_duration"]
            adw::ButtonContent {
                add_css_class: "timer",
                add_css_class: "card",

                #[watch]
                set_label: &model.duration_label,

                #[watch]
                set_icon_name: model.theme.icon_name("hourglass", "hourglass-dark"),
            },
        }
    }

    async fn init(
        MeetingHeaderInit {
            my_id,
            meeting_start_time,
            theme,
            waiting_participants,
            shared_folder,
            protocol,
        }: MeetingHeaderInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        TIMER_STATE.subscribe(sender.input_sender(), |_| {
            println!("TIMER_STATE SUBSCRIBED IN MEETING HEADER");
            MeetingHeaderInput::TimerChanged
        });
        let waiting_participants_count = waiting_participants.len();
        let normal_timer = if let Some(started) = &*TIMER_STATE.read() {
            started.normal_timer_info()
        } else {
            None
        };
        let themed_break = if let Some(started) = &*TIMER_STATE.read() {
            started.themed_break_info()
        } else {
            None
        };
        let view_switcher =
            ViewSwitcher::builder()
                .launch(())
                .forward(sender.input_sender(), |_|
                 // TODO: this needs to be replaced by the selection of the correct view
                 MeetingHeaderInput::Tick);
        let waiting_participants = WaitingParticipantsList::builder()
            .launch(WaitingParticipantsListInit {
                participants: waiting_participants.clone(),
            })
            .forward(sender.input_sender(), |msg| match msg {
                WaitingParticipantsListOutput::Accept { target } => {
                    MeetingHeaderInput::AcceptParticipantFromWaitingRoom { target }
                }
            });

        let model = Self {
            my_id,
            view_switcher,
            waiting_participants,
            meeting_start_time,
            duration_label: build_duration_label(
                meeting_start_time,
                Timestamp::now(),
                TimeResolution::Minutes,
            ),
            normal_timer,
            themed_break,
            theme,
            waiting_participants_count,
            shared_folder,
            protocol,
        };
        let widgets = view_output!();

        glib::timeout_add_seconds(1, move || {
            let success = sender.input_sender().send(MeetingHeaderInput::Tick).is_ok();
            glib::Continue(success)
        });

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: MeetingHeaderInput,
        sender: AsyncComponentSender<Self>,
        root: &Self::Root,
    ) {
        match msg {
            MeetingHeaderInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
            MeetingHeaderInput::ThemedBreakButtonActivated => {
                sender.output(MeetingHeaderOutput::ShowThemedBreak).unwrap();
            }
            MeetingHeaderInput::TimerChanged | MeetingHeaderInput::Tick => {
                let timer = TIMER_STATE.read();

                if let Some(timer) = &*timer {
                    self.update_normal_timer(timer.normal_timer_info());
                    self.update_themed_break(timer.themed_break_info());
                } else {
                    self.update_normal_timer(None);
                    self.update_themed_break(None);
                }

                self.duration_label = build_duration_label(
                    self.meeting_start_time,
                    Timestamp::now(),
                    TimeResolution::Minutes,
                )
            }
            MeetingHeaderInput::ParticipantJoinedWaitingRoom(participant) => {
                self.waiting_participants
                    .emit(WaitingParticipantsListInput::AddParticipant(participant));
                self.waiting_participants_count = self.waiting_participants_count.saturating_add(1);
            }
            MeetingHeaderInput::ParticipantLeftWaitingRoom(associated_participant) => {
                self.waiting_participants
                    .emit(WaitingParticipantsListInput::RemoveParticipant(
                        associated_participant,
                    ));
                self.waiting_participants_count = self.waiting_participants_count.saturating_sub(1);
            }
            MeetingHeaderInput::AcceptParticipantFromWaitingRoom { target } => {
                sender
                    .output(MeetingHeaderOutput::AcceptParticipantFromWaitingRoom { target })
                    .unwrap();
            }
            MeetingHeaderInput::CopySharedFolderReadPassword => {
                if let Some(shared_folder) = self.shared_folder_read() {
                    let clipboard = root.clipboard();
                    clipboard.set_text(&shared_folder.password);
                }
            }
            MeetingHeaderInput::CopySharedFolderWritePassword => {
                if let Some(shared_folder) = self.shared_folder_write() {
                    let clipboard = root.clipboard();
                    clipboard.set_text(&shared_folder.password);
                }
            }
            MeetingHeaderInput::UpdateTimerReadyStatus { ready } => {
                let timer = TIMER_STATE.read();
                if let Some(timer) = &*timer {
                    sender
                        .output(MeetingHeaderOutput::UpdateTimerReadyStatus {
                            timer_id: timer.timer_id,
                            ready,
                        })
                        .unwrap();
                }
            }
            MeetingHeaderInput::TimerReadyStatusUpdated {
                timer_id,
                participant_id,
                ready,
            } => {
                if participant_id == self.my_id {
                    match self.normal_timer.as_mut() {
                        Some(timer) if timer.started.timer_id == timer_id => {
                            timer.participant_marked_as_done = ready;
                        }
                        Some(_) | None => {}
                    }
                }
            }
            MeetingHeaderInput::UpdateProtocol(protocol) => self.protocol = Some(protocol),
            MeetingHeaderInput::OpenProtocolInBrowser => {
                webbrowser::open(self.protocol_url()).ok();
            }
            MeetingHeaderInput::CopyProtocolUrl => {
                let clipboard = root.clipboard();
                clipboard.set_text(self.protocol_url());
            }
        }
    }
}

impl MeetingHeader {
    fn has_shared_folder(&self) -> bool {
        self.shared_folder.is_some()
    }

    fn has_shared_folder_write(&self) -> bool {
        self.shared_folder
            .as_ref()
            .map(|f| f.read_write.is_some())
            .unwrap_or_default()
    }

    fn shared_folder_read(&self) -> Option<&SharedFolderAccess> {
        self.shared_folder.as_ref().map(|f| &f.read)
    }

    fn shared_folder_write(&self) -> Option<&SharedFolderAccess> {
        self.shared_folder
            .as_ref()
            .map(|f| f.read_write.as_ref())
            .flatten()
    }

    fn has_protocol(&self) -> bool {
        self.protocol.is_some()
    }

    fn protocol_url(&self) -> &str {
        match &self.protocol {
            Some(Protocol::Read(url)) => url,
            Some(Protocol::Write(url)) => url,
            None => "",
        }
    }

    fn update_themed_break(&mut self, themed_break_info: Option<ThemedBreakInfo>) {
        self.themed_break = themed_break_info;
    }

    fn update_normal_timer(&mut self, normal_timer_info: Option<NormalTimerInfo>) {
        match normal_timer_info {
            Some(info) => {
                if let Some(ref mut n) = self.normal_timer {
                    n.update(info);
                } else {
                    self.normal_timer = Some(info);
                }
            }
            None => self.normal_timer = None,
        }
    }
}
