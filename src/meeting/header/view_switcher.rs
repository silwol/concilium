// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use relm4::{
    actions::{RelmAction, RelmActionGroup},
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};
use strum::{AsRefStr, Display, EnumCount, EnumIter, EnumString, EnumVariantNames, IntoStaticStr};

pub struct ViewSwitcher;

fn build_entries() -> gtk::StringList {
    use strum::VariantNames;
    gtk::StringList::from_iter(
        ViewSwitcherOutput::VARIANTS
            .iter()
            .map(|s| String::from(*s)),
    )
}

#[derive(
    Debug, AsRefStr, Display, EnumCount, EnumIter, EnumString, EnumVariantNames, IntoStaticStr,
)]
pub enum ViewSwitcherOutput {
    Grid,
    Large,
    Fullscreen,
}

#[relm4::component(async, pub)]
impl AsyncComponent for ViewSwitcher {
    type Init = ();
    type Input = ();
    type Output = ViewSwitcherOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::DropDown {
            set_model: Some(&build_entries()),
            set_show_arrow: false,
            connect_selected_notify[sender] => move |v| {
                use strum::IntoEnumIterator as _;
                let item = Self::Output::iter().nth(v.selected().try_into().unwrap()).unwrap();
                println!("{:?}", item);
                sender.output(item).unwrap();
            },
        }
    }

    async fn init(
        _init: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self;
        let widgets = view_output!();

        let mut group = RelmActionGroup::<MeetingMenuActionGroup>::new();
        let grid_action: RelmAction<GridAction> = {
            RelmAction::new_stateless(move |_| {
                println!("Grid action selected");
            })
        };
        let large_action: RelmAction<GridAction> = {
            RelmAction::new_stateless(move |_| {
                println!("Large action selected");
            })
        };
        let fullscreen_action: RelmAction<GridAction> = {
            RelmAction::new_stateless(move |_| {
                println!("Fullscreen action selected");
            })
        };
        group.add_action(grid_action);
        group.add_action(large_action);
        group.add_action(fullscreen_action);

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        _msg: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
    }
}

relm4::new_action_group!(MeetingMenuActionGroup, "meeting_menu");
relm4::new_stateless_action!(GridAction, MeetingMenuActionGroup, "grid");
relm4::new_stateless_action!(LargeAction, MeetingMenuActionGroup, "large");
relm4::new_stateless_action!(FullscreenAction, MeetingMenuActionGroup, "fullscreen");
