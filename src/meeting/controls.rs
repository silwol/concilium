// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt as _, ButtonExt as _, ToggleButtonExt as _, WidgetExt as _};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk::{self, traits::OrientableExt as _},
    AsyncComponentSender,
};

use crate::theme::Theme;

pub struct Controls {
    lower_hand: bool,
    raise_hands_enabled: bool,
    theme: Theme,
}

#[derive(Debug)]
pub enum ControlsOutput {
    LeaveRoom,
    RaiseHand,
    LowerHand,
}

#[derive(Debug)]
pub enum ControlsInput {
    RaiseHandsEnableChanged { enabled: bool },
    RaisedHandsReset,
    SwitchTheme(Theme),
}

#[derive(Debug)]
pub struct ControlsInit {
    pub raise_hands_enabled: bool,
    pub theme: Theme,
}

#[relm4::component(async, pub)]
impl AsyncComponent for Controls {
    type Init = ControlsInit;
    type Input = ControlsInput;
    type Output = ControlsOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::CenterBox {
            set_center_widget: Some(&center_controls),
        },

        #[name = "center_controls"]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,
            set_margin_start: 5,
            set_margin_end: 5,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_spacing: 3,
            add_css_class: "toolbar",

            gtk::ToggleButton {
                set_label: "Hand",
                #[watch]
                set_icon_name: model.theme.icon_name("hand", "hand-dark"),
                add_css_class: "pill",
                add_css_class: "raised",
                #[track(model.lower_hand)]
                set_active: false,
                #[watch]
                set_sensitive: model.raise_hands_enabled,
                connect_toggled[sender] => move |button| {
                    if button.is_active() {
                        sender.output(Self::Output::RaiseHand).unwrap();
                    } else {
                        sender.output(Self::Output::LowerHand).unwrap();
                    }
                },
            },

            gtk::Button {
                set_label: "Leave room",
                set_icon_name: "call-stop-symbolic",
                add_css_class: "pill",
                add_css_class: "destructive-action",
                add_css_class: "raised",
                connect_clicked[sender] => move |_| {
                    sender.output(Self::Output::LeaveRoom).unwrap()
                },
            },
        }
    }

    async fn init(
        ControlsInit {
            raise_hands_enabled,
            theme,
        }: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self {
            lower_hand: true,
            raise_hands_enabled,
            theme,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        self.lower_hand = false;
        match msg {
            Self::Input::RaiseHandsEnableChanged { enabled } => {
                if !enabled {
                    self.lower_hand = true;
                }
                self.raise_hands_enabled = enabled;
            }
            Self::Input::RaisedHandsReset => {
                self.lower_hand = true;
            }
            Self::Input::SwitchTheme(theme) => {
                self.theme = theme;
            }
        }
    }
}
