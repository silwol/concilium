// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentController, AsyncController},
    factory::AsyncFactoryComponent,
    gtk,
    prelude::DynamicIndex,
    AsyncFactorySender,
};

use crate::{
    meeting::{
        participants::Participants,
        user_view::{UserView, UserViewInit, UserViewInput, UserViewOutput},
    },
    theme::Theme,
};

use super::VideoGridInput;

pub struct VideoGridEntry {
    participant_id: ParticipantId,
    user_view: AsyncController<UserView>,
}

#[derive(Debug)]
pub struct VideoGridEntryInit {
    pub participants: Participants,
    pub participant_id: ParticipantId,
    pub chat_enabled: bool,
    pub theme: Theme,
}

#[derive(Clone, Debug)]
pub enum VideoGridEntryInput {
    SwitchTheme(Theme),
    ParticipantUpdated(ParticipantId),
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessageSent {
        message: MessageSent,
    },
    UnreadCountWasReset,
}

#[derive(Clone, Debug)]
pub enum VideoGridEntryOutput {
    GrantModeratorRights { target: ParticipantId },
    RevokeModeratorRights { target: ParticipantId },
    SendMessage { scope: Scope, content: String },
    ResetUnreadCount { target: ParticipantId },
}

#[relm4::factory(async, pub)]
impl AsyncFactoryComponent for VideoGridEntry {
    type Init = VideoGridEntryInit;
    type Input = VideoGridEntryInput;
    type Output = VideoGridEntryOutput;
    type CommandOutput = ();
    type Widgets = VideoGridWidgets;
    type ParentInput = VideoGridInput;
    type ParentWidget = gtk::FlowBox;

    view! {
        root = gtk::Box {
            self.user_view.widget(),
        }
    }

    async fn init_model(
        VideoGridEntryInit {
            participants,
            participant_id,
            chat_enabled,
            theme,
        }: Self::Init,
        _index: &DynamicIndex,
        sender: AsyncFactorySender<Self>,
    ) -> Self {
        let user_view = UserView::builder()
            .launch(UserViewInit {
                participants,
                participant_id,
                theme,
                chat_enabled,
            })
            .forward(sender.output_sender(), |msg| match msg {
                UserViewOutput::GrantModeratorRights { target } => {
                    VideoGridEntryOutput::GrantModeratorRights { target }
                }
                UserViewOutput::RevokeModeratorRights { target } => {
                    VideoGridEntryOutput::RevokeModeratorRights { target }
                }
                UserViewOutput::SendMessage { scope, content } => {
                    VideoGridEntryOutput::SendMessage { scope, content }
                }
                UserViewOutput::ResetUnreadCount { target } => {
                    VideoGridEntryOutput::ResetUnreadCount { target }
                }
            });

        Self {
            participant_id,
            user_view,
        }
    }

    fn forward_to_parent(output: VideoGridEntryOutput) -> Option<VideoGridInput> {
        match output {
            VideoGridEntryOutput::GrantModeratorRights { target } => {
                Some(VideoGridInput::GrantModeratorRights { target })
            }
            VideoGridEntryOutput::RevokeModeratorRights { target } => {
                Some(VideoGridInput::RevokeModeratorRights { target })
            }
            VideoGridEntryOutput::SendMessage { scope, content } => {
                Some(VideoGridInput::SendMessage { scope, content })
            }
            VideoGridEntryOutput::ResetUnreadCount { target } => {
                Some(VideoGridInput::ResetUnreadCount { target })
            }
        }
    }

    async fn update(&mut self, msg: Self::Input, _sender: AsyncFactorySender<Self>) {
        match msg {
            VideoGridEntryInput::SwitchTheme(theme) => {
                self.user_view.emit(UserViewInput::SwitchTheme(theme));
            }
            VideoGridEntryInput::ParticipantUpdated(participant_id) => {
                self.user_view
                    .emit(UserViewInput::ParticipantUpdated(participant_id));
            }
            VideoGridEntryInput::ChatEnableChanged { issued_by, enabled } => self
                .user_view
                .emit(UserViewInput::ChatEnableChanged { issued_by, enabled }),
            VideoGridEntryInput::ChatMessageSent { message } => self
                .user_view
                .emit(UserViewInput::ChatMessageSent { message }),
            VideoGridEntryInput::UnreadCountWasReset => {
                self.user_view.emit(UserViewInput::UnreadCountWasReset);
            }
        }
    }
}

impl VideoGridEntry {
    pub fn is(&self, participant_id: ParticipantId) -> bool {
        self.participant_id == participant_id
    }
}
