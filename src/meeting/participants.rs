// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use anyhow::{ensure, Result};
use chrono::{DateTime, Local};

use std::{
    collections::{HashMap, HashSet},
    sync::{Arc, RwLock},
};

use opentalk_types::{
    core::{ParticipantId, Timestamp},
    signaling::{control::Participant, Role},
};

use crate::participant_ext::ParticipantExt;

#[derive(Debug, Clone)]
pub struct Participants {
    current_participant_id: ParticipantId,
    inner: Arc<RwLock<HashMap<ParticipantId, Participant>>>,
}

impl Participants {
    pub fn new<P>(current_participant_id: ParticipantId, participants: P) -> Result<Self>
    where
        P: IntoIterator<Item = Participant>,
    {
        let participants: HashMap<ParticipantId, Participant> =
            participants.into_iter().map(|p| (p.id, p)).collect();
        ensure!(
            participants.contains_key(&current_participant_id),
            "Participant's own id must be contained in the participants collection"
        );
        Ok(Self {
            current_participant_id,
            inner: Arc::new(RwLock::new(participants)),
        })
    }

    pub fn earliest_join(&self) -> Timestamp {
        let lock = self.inner.read().unwrap();
        lock.values()
            .filter_map(|p| p.participant_data.control.as_ref())
            .map(|c| c.joined_at)
            .min()
            .unwrap_or_else(|| Timestamp::now())
    }

    pub fn get(&self, participant_id: ParticipantId) -> Option<Participant> {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id).cloned()
    }

    /// Returns the previous value of the participant, if any was present
    pub fn set(&self, participant: Participant) -> Option<Participant> {
        let mut lock = self.inner.write().unwrap();
        lock.insert(participant.id, participant)
    }

    pub fn update_current_with<F: FnMut(&mut Participant)>(&self, f: F) -> Option<Participant> {
        self.update_with(self.current_participant_id, f)
    }

    pub fn update_with<F: FnMut(&mut Participant)>(
        &self,
        participant_id: ParticipantId,
        mut f: F,
    ) -> Option<Participant> {
        let mut lock = self.inner.write().unwrap();
        if let Some(p) = lock.get_mut(&participant_id) {
            f(p);
            Some(p.clone())
        } else {
            None
        }
    }

    pub fn display_name_or_default(&self, participant_id: ParticipantId) -> String {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.display_name_or_default())
            .unwrap_or("unknown".to_string())
    }

    pub fn is_moderator(&self, participant_id: ParticipantId) -> bool {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.is_moderator())
            .unwrap_or_default()
    }

    pub fn is_present(&self, participant_id: ParticipantId) -> bool {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.is_present())
            .unwrap_or_default()
    }

    pub fn is_room_owner(&self, participant_id: ParticipantId) -> bool {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.is_room_owner())
            .unwrap_or_default()
    }

    pub fn hand_is_up(&self, participant_id: ParticipantId) -> bool {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.hand_is_up())
            .unwrap_or_default()
    }

    pub fn role(&self, participant_id: ParticipantId) -> Role {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.role())
            .unwrap_or(Role::Guest)
    }

    pub fn joined_at_local(&self, participant_id: ParticipantId) -> Option<DateTime<Local>> {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.joined_at_local())
            .flatten()
    }

    pub fn protocol_write_access(&self, participant_id: ParticipantId) -> bool {
        let lock = self.inner.read().unwrap();
        lock.get(&participant_id)
            .map(|p| p.protocol_write_access())
            .unwrap_or_default()
    }

    pub fn get_all_ids(&self) -> HashSet<ParticipantId> {
        let lock = self.inner.read().unwrap();
        lock.keys().cloned().collect()
    }

    pub fn get_all_present(&self) -> HashMap<ParticipantId, Participant> {
        let lock = self.inner.read().unwrap();
        lock.iter()
            .filter_map(|(id, p)| p.is_present().then_some((*id, p.clone())))
            .collect()
    }

    pub fn current_participant_id(&self) -> ParticipantId {
        self.current_participant_id
    }

    pub fn current_participant_is_moderator(&self) -> bool {
        self.is_moderator(self.current_participant_id)
    }

    pub fn current_participant_is_room_owner(&self) -> bool {
        self.is_room_owner(self.current_participant_id)
    }
}
