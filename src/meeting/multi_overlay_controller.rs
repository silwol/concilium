// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::cell::RefCell;

use opentalk_types::signaling::timer::TimerId;
use relm4::{
    component::{AsyncComponentController, AsyncController},
    gtk,
};

use crate::theme::Theme;

use super::themed_break::{ThemedBreak, ThemedBreakInput};

enum MultiOverlayControllerState {
    Idle,
    StartTransfer,
    StageOneTransferToA(Option<Overlay>),
    StageOneTransferToB(Option<Overlay>),
    StageTwoTransferToA(Option<Overlay>),
    StageTwoTransferToB(Option<Overlay>),
}

impl Default for MultiOverlayControllerState {
    fn default() -> Self {
        Self::Idle
    }
}

#[derive(Default)]
pub(super) struct MultiOverlayController {
    a: OverlayController,
    b: OverlayController,
    state: MultiOverlayControllerState,
}

impl MultiOverlayController {
    pub(super) fn update_theme(&mut self, theme: Theme) {
        if let Some(ref mut current) = self.a.current {
            current.update_theme(theme);
        }
        if let Some(ref mut current) = self.b.current {
            current.update_theme(theme);
        }
        match self.state {
            MultiOverlayControllerState::StageOneTransferToA(Some(ref mut overlay))
            | MultiOverlayControllerState::StageOneTransferToB(Some(ref mut overlay))
            | MultiOverlayControllerState::StageTwoTransferToA(Some(ref mut overlay))
            | MultiOverlayControllerState::StageTwoTransferToB(Some(ref mut overlay)) => {
                overlay.update_theme(theme);
            }
            _ => {}
        }
    }

    pub(super) fn start_swap(&mut self) {
        // TODO: make a counter to perform more than one remaining transition
        self.state = MultiOverlayControllerState::StartTransfer;
    }

    pub(super) fn set_next_a(&mut self, next: Option<Overlay>) {
        self.a.set_next(next);
    }

    pub(super) fn set_next_b(&mut self, next: Option<Overlay>) {
        self.b.set_next(next);
    }
}

#[derive(Default)]
struct OverlayController {
    current: Option<Overlay>,
    next: Option<Option<Overlay>>,
}

impl OverlayController {
    fn set_next(&mut self, next: Option<Overlay>) {
        self.next = Some(next);
    }
}

pub(super) enum Overlay {
    ThemedBreak(TimerId, AsyncController<ThemedBreak>),
}

impl Overlay {
    fn remove_from_gtk_overlay(&self, gtk_overlay: &gtk::Overlay) {
        match self {
            Overlay::ThemedBreak(_, c) => {
                gtk_overlay.remove_overlay(c.widget());
            }
        }
    }

    fn add_to_gtk_overlay(&self, gtk_overlay: &gtk::Overlay) {
        match self {
            Overlay::ThemedBreak(_, c) => {
                gtk_overlay.add_overlay(c.widget());
            }
        }
    }

    fn update_theme(&mut self, theme: Theme) {
        match self {
            Overlay::ThemedBreak(_, c) => {
                c.emit(ThemedBreakInput::SwitchTheme(theme));
            }
        }
    }
}

trait OverlayPrivateExt {
    fn apply_from_controller(&self, controller: &mut OverlayController) -> Option<Overlay>;

    fn set_current_overlay(
        &self,
        current_overlay: &mut Option<Overlay>,
        next_overlay: &mut Option<Option<Overlay>>,
    ) -> Option<Overlay>;
}

impl OverlayPrivateExt for gtk::Overlay {
    fn apply_from_controller(&self, controller: &mut OverlayController) -> Option<Overlay> {
        self.set_current_overlay(&mut controller.current, &mut controller.next)
    }

    fn set_current_overlay(
        &self,
        current_overlay: &mut Option<Overlay>,
        next_overlay: &mut Option<Option<Overlay>>,
    ) -> Option<Overlay> {
        let mut removed_overlay = None;
        if let Some(outer_next_overlay) = next_overlay.take() {
            if let Some(current_overlay) = current_overlay.take() {
                current_overlay.remove_from_gtk_overlay(self);
                let _ = removed_overlay.insert(current_overlay);
            }
            if let Some(inner_next_overlay) = outer_next_overlay {
                inner_next_overlay.add_to_gtk_overlay(self);
                let _ = current_overlay.insert(inner_next_overlay);
            }
        }
        removed_overlay
    }
}

pub(super) trait OverlayExt {
    fn apply_from_multi_controller_a_refcell(&self, controller: &RefCell<MultiOverlayController>);
    fn apply_from_multi_controller_b_refcell(&self, controller: &RefCell<MultiOverlayController>);
    fn apply_from_multi_controller_a(&self, controller: &mut MultiOverlayController);
    fn apply_from_multi_controller_b(&self, controller: &mut MultiOverlayController);
}

impl<T: OverlayPrivateExt> OverlayExt for T {
    fn apply_from_multi_controller_a_refcell(&self, controller: &RefCell<MultiOverlayController>) {
        self.apply_from_multi_controller_a(&mut controller.borrow_mut())
    }

    fn apply_from_multi_controller_b_refcell(&self, controller: &RefCell<MultiOverlayController>) {
        self.apply_from_multi_controller_b(&mut controller.borrow_mut())
    }

    fn apply_from_multi_controller_a(&self, controller: &mut MultiOverlayController) {
        let mut temporary_state = MultiOverlayControllerState::Idle;
        std::mem::swap(&mut temporary_state, &mut controller.state);

        match temporary_state {
            MultiOverlayControllerState::Idle => {
                self.apply_from_controller(&mut controller.a);
                controller.state = MultiOverlayControllerState::Idle;
            }
            MultiOverlayControllerState::StartTransfer => {
                controller.a.set_next(None);
                let removed_overlay = self.apply_from_controller(&mut controller.a);
                controller.state =
                    MultiOverlayControllerState::StageOneTransferToB(removed_overlay);
            }
            MultiOverlayControllerState::StageOneTransferToA(overlay) => {
                controller.a.set_next(overlay);
                let removed_overlay = self.apply_from_controller(&mut controller.a);
                controller.state =
                    MultiOverlayControllerState::StageTwoTransferToB(removed_overlay);
            }
            MultiOverlayControllerState::StageOneTransferToB(overlay) => {
                self.apply_from_controller(&mut controller.a);
                controller.state = MultiOverlayControllerState::StageOneTransferToB(overlay);
            }
            MultiOverlayControllerState::StageTwoTransferToA(overlay) => {
                controller.a.set_next(overlay);
                self.apply_from_controller(&mut controller.a);
                controller.state = MultiOverlayControllerState::Idle;
            }
            MultiOverlayControllerState::StageTwoTransferToB(overlay) => {
                self.apply_from_controller(&mut controller.a);
                controller.state = MultiOverlayControllerState::StageTwoTransferToB(overlay);
            }
        };
    }
    fn apply_from_multi_controller_b(&self, controller: &mut MultiOverlayController) {
        let mut temporary_state = MultiOverlayControllerState::Idle;
        std::mem::swap(&mut temporary_state, &mut controller.state);

        match temporary_state {
            MultiOverlayControllerState::Idle => {
                self.apply_from_controller(&mut controller.b);
                controller.state = MultiOverlayControllerState::Idle;
            }
            MultiOverlayControllerState::StartTransfer => {
                controller.b.set_next(None);
                let removed_overlay = self.apply_from_controller(&mut controller.b);
                controller.state =
                    MultiOverlayControllerState::StageOneTransferToA(removed_overlay);
            }
            MultiOverlayControllerState::StageOneTransferToA(overlay) => {
                self.apply_from_controller(&mut controller.b);
                controller.state = MultiOverlayControllerState::StageOneTransferToA(overlay);
            }
            MultiOverlayControllerState::StageOneTransferToB(overlay) => {
                controller.b.set_next(overlay);
                let removed_overlay = self.apply_from_controller(&mut controller.b);
                controller.state =
                    MultiOverlayControllerState::StageTwoTransferToA(removed_overlay);
            }
            MultiOverlayControllerState::StageTwoTransferToA(overlay) => {
                self.apply_from_controller(&mut controller.b);
                controller.state = MultiOverlayControllerState::StageTwoTransferToA(overlay);
            }
            MultiOverlayControllerState::StageTwoTransferToB(overlay) => {
                controller.b.set_next(overlay);
                self.apply_from_controller(&mut controller.b);
                controller.state = MultiOverlayControllerState::Idle;
            }
        };
    }
}
