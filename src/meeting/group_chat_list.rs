// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::collections::HashMap;

use gtk::traits::{BoxExt, OrientableExt, WidgetExt};
use opentalk_types::{
    core::GroupName,
    signaling::chat::{
        state::{GroupHistory, StoredMessage},
        Scope,
    },
};
use relm4::{
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender,
};

use crate::{
    meeting::chat::{ChatAction, ChatInit, ChatOutput},
    participant_ext::ParticipantExt,
    theme::Theme,
};

use super::{
    chat::{Chat, ChatInput},
    participants::Participants,
};

pub struct GroupChatList {
    group_chats: HashMap<GroupName, AsyncController<Chat>>,
    groups_stack: gtk::Stack,
    unread_count: HashMap<GroupName, usize>,
    is_shown: bool,
}

#[derive(Debug)]
pub enum GroupChatListInput {
    SwitchTheme(Theme),
    SendMessage {
        scope: Scope,
        content: String,
    },
    Action {
        group: Option<GroupName>,
        action: ChatAction,
    },
    SwitchedToGroup(GroupName),
    IsShown,
    IsHidden,
}

#[derive(Debug)]
pub enum GroupChatListOutput {
    SendMessage { scope: Scope, content: String },
    UnreadCountChanged { count: usize },
}

#[derive(Debug)]
pub struct GroupChatListInit {
    pub participants: Participants,
    pub theme: Theme,
    pub groups: Vec<GroupHistory>,
    pub enabled: bool,
}

#[relm4::component(async, pub)]
impl AsyncComponent for GroupChatList {
    type Init = GroupChatListInit;
    type Input = GroupChatListInput;
    type Output = GroupChatListOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_vexpand: true,

            gtk::StackSwitcher {
                set_orientation: gtk::Orientation::Vertical,
                set_stack: Some(&groups_stack),
            },

            append: &groups_stack,
        }
    }

    async fn init(
        GroupChatListInit {
            participants,
            theme,
            groups,
            enabled,
        }: GroupChatListInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let groups_stack = gtk::Stack::new();

        let widgets = view_output!();

        let mut group_chats = HashMap::new();
        for group in groups {
            let actions = group
                .history
                .into_iter()
                .map(
                    |StoredMessage {
                         id: _,
                         source,
                         timestamp: _,
                         content,
                         scope: _,
                     }| {
                        let (display_name, is_moderator) = if let Some(p) = participants.get(source)
                        {
                            (p.display_name_or_default(), p.is_moderator())
                        } else {
                            ("unknown".to_string(), false)
                        };
                        ChatAction::Message {
                            display_name,
                            is_moderator,
                            participant_id: source,
                            content,
                        }
                    },
                )
                .collect();
            let name = group.name.to_string();

            let init = ChatInit {
                theme,
                enabled,
                actions,
                emoji_chooser_enabled: true,
                chat_name: "Group chat".to_string(),
                scope: Scope::Group(group.name.clone()),
            };

            let chat =
                Chat::builder()
                    .launch(init)
                    .forward(sender.input_sender(), |msg| match msg {
                        ChatOutput::SendMessage { scope, content } => {
                            GroupChatListInput::SendMessage { scope, content }
                        }
                    });
            groups_stack.add_titled(chat.widget(), Some(&name), &name);

            group_chats.insert(group.name, chat);
        }

        groups_stack.connect_visible_child_notify(move |stack| {
            let name = GroupName::from(stack.visible_child_name().unwrap().as_str().to_string());
            sender.input(GroupChatListInput::SwitchedToGroup(name));
        });

        let model = GroupChatList {
            group_chats,
            groups_stack,
            unread_count: Default::default(),
            is_shown: false,
        };

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: GroupChatListInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            GroupChatListInput::SwitchTheme(theme) => {
                for chat in self.group_chats.values() {
                    chat.emit(ChatInput::SwitchTheme(theme));
                }
            }
            GroupChatListInput::SendMessage { scope, content } => {
                sender
                    .output(GroupChatListOutput::SendMessage { scope, content })
                    .unwrap();
            }
            GroupChatListInput::Action { group, action } => {
                if let Some(group) = group {
                    if let Some(chat) = self.group_chats.get(&group) {
                        if matches!(action, ChatAction::Message { .. }) {
                            let visible_group = GroupName::from(
                                self.groups_stack.visible_child_name().unwrap().to_string(),
                            );

                            if !self.is_shown || visible_group != group {
                                let child = self
                                    .groups_stack
                                    .child_by_name(group.as_ref().as_str())
                                    .unwrap();
                                let page = self.groups_stack.page(&child);
                                let unread_count =
                                    self.unread_count.entry(group.clone()).or_insert(0);
                                *unread_count = unread_count.saturating_add(1);
                                page.set_title(&format!(
                                    "{} ({})",
                                    group.to_string(),
                                    *unread_count
                                ));
                            }
                        }
                        chat.emit(ChatInput::Action(action));
                    }
                } else {
                    for chat in self.group_chats.values() {
                        chat.emit(ChatInput::Action(action.clone()));
                    }
                }
                sender
                    .output(GroupChatListOutput::UnreadCountChanged {
                        count: self.unread_count.values().sum(),
                    })
                    .unwrap();
            }
            GroupChatListInput::SwitchedToGroup(group) => {
                let child = self
                    .groups_stack
                    .child_by_name(group.as_ref().as_str())
                    .unwrap();
                let page = self.groups_stack.page(&child);
                self.unread_count.remove(&group);
                page.set_title(&group.to_string());

                sender
                    .output(GroupChatListOutput::UnreadCountChanged {
                        count: self.unread_count.values().sum(),
                    })
                    .unwrap();
            }
            GroupChatListInput::IsShown => {
                self.is_shown = true;

                if let Some(group) = self.groups_stack.visible_child_name() {
                    let group = GroupName::from(group.to_string());
                    self.unread_count.remove(&group);

                    if let Some(child) = self.groups_stack.visible_child() {
                        let page = self.groups_stack.page(&child);
                        page.set_title(&group.to_string());
                    }
                }

                sender
                    .output(GroupChatListOutput::UnreadCountChanged {
                        count: self.unread_count.values().sum(),
                    })
                    .unwrap();
            }
            GroupChatListInput::IsHidden => {
                self.is_shown = false;
            }
        }
    }
}
