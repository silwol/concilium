// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt, OrientableExt, WidgetExt as _};
use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender, RelmWidgetExt,
};

use crate::{
    meeting::{
        chat::ChatInit,
        participant_chat_menu::{ParticipantChatMenuInit, ParticipantChatMenuOutput},
        participant_menu::{ParticipantMenuInit, ParticipantMenuOutput},
    },
    theme::Theme,
};

use super::{
    participant_chat_menu::{ParticipantChatMenu, ParticipantChatMenuInput},
    participant_menu::{ParticipantMenu, ParticipantMenuInput},
    participants::Participants,
};

pub struct UserView {
    participants: Participants,
    participant_id: ParticipantId,
    theme: Theme,
    menu: AsyncController<ParticipantMenu>,
    chat_menu: AsyncController<ParticipantChatMenu>,
}

#[derive(Debug)]
pub struct UserViewInit {
    pub participants: Participants,
    pub participant_id: ParticipantId,
    pub theme: Theme,
    pub chat_enabled: bool,
}

#[derive(Debug)]
pub enum UserViewInput {
    SwitchTheme(Theme),
    ParticipantUpdated(ParticipantId),
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessageSent {
        message: MessageSent,
    },
    UnreadCountWasReset,
}

#[derive(Debug)]
pub enum UserViewOutput {
    GrantModeratorRights { target: ParticipantId },
    RevokeModeratorRights { target: ParticipantId },
    SendMessage { scope: Scope, content: String },
    ResetUnreadCount { target: ParticipantId },
}

#[relm4::component(async, pub)]
impl AsyncComponent for UserView {
    type Init = UserViewInit;
    type Input = UserViewInput;
    type Output = UserViewOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::AspectFrame {
            set_ratio: 4.0/3.0,
            set_xalign: 0.5,
            set_yalign: 0.5,
            set_obey_child: false,
            set_vexpand: false,

            adw::Bin {
                add_css_class: "user_view",
                set_margin_bottom: 10,
                set_margin_top: 10,
                set_margin_start: 10,
                set_margin_end: 10,
                set_hexpand: true,
                set_vexpand: true,

                gtk::Overlay{
                    add_overlay: &participant_info,
                    add_overlay: &hand_indicator,
                    add_overlay: &participant_menu,

                    adw::Avatar {
                        set_margin_top: 60,
                        set_margin_bottom: 60,
                        set_margin_start: 60,
                        set_margin_end: 60,

                        set_halign: gtk::Align::Center,
                        set_size: 100,
                        set_show_initials: true,

                        #[watch]
                        set_text: Some(&model.participants.display_name_or_default(model.participant_id)),
                    },
                },
            },
        },

        #[name = "participant_menu"]
        gtk::Box {
            set_halign: gtk::Align::Start,
            set_valign: gtk::Align::End,
            set_visible: model.participants.current_participant_id() != model.participant_id,
            set_margin_start: 5,
            set_margin_bottom: 5,
            add_css_class: "linked",
            add_css_class: "toolbar",

            model.menu.widget(),
            model.chat_menu.widget(),
        },

        #[name = "participant_info"]
        gtk::Box {
            add_css_class: "user_view_participant_info",
            set_orientation: gtk::Orientation::Horizontal,
            set_halign: gtk::Align::Center,
            set_valign: gtk::Align::End,
            set_spacing: 5,

            gtk::Label {
                #[watch]
                set_label: &model.participants.display_name_or_default(model.participant_id),
            },

            gtk::Image {
                #[watch]
                set_icon_name: Some(model.theme.icon_name("moderator", "moderator-dark")),
                set_tooltip: "Participant is moderator",

                #[watch]
                set_visible: model.participants.is_moderator(model.participant_id),
            },

        },

        #[name = "hand_indicator"]
        gtk::Image {
            add_css_class: "user_view_hand_indicator",
            add_css_class: "accent",

            set_halign: gtk::Align::End,
            set_valign: gtk::Align::End,
            set_margin_bottom: 5,
            set_margin_end: 5,

            #[watch]
            set_icon_name: Some(model.theme.icon_name("hand", "hand-dark")),

           #[watch]
            set_visible: model.participants.hand_is_up(model.participant_id),
        }
    }

    async fn init(
        UserViewInit {
            participants,
            participant_id,
            theme,
            chat_enabled,
        }: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let menu = ParticipantMenu::builder()
            .launch(ParticipantMenuInit {
                participants: participants.clone(),
                participant_id,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantMenuOutput::GrantModeratorRights { target } => {
                    UserViewOutput::GrantModeratorRights { target }
                }
                ParticipantMenuOutput::RevokeModeratorRights { target } => {
                    UserViewOutput::RevokeModeratorRights { target }
                }
            });

        let chat_menu = ParticipantChatMenu::builder()
            .launch(ParticipantChatMenuInit {
                participants: participants.clone(),
                participant_id,
                theme,
                chat: ChatInit {
                    enabled: chat_enabled,
                    actions: Vec::new(),
                    theme,
                    emoji_chooser_enabled: false,
                    chat_name: "Private chat".to_string(),
                    scope: Scope::Private(participant_id),
                },
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantChatMenuOutput::SendMessage { scope, content } => {
                    UserViewOutput::SendMessage { scope, content }
                }
                ParticipantChatMenuOutput::ResetUnreadCount { target } => {
                    UserViewOutput::ResetUnreadCount { target }
                }
            });

        let model = Self {
            participants: participants.clone(),
            participant_id,
            theme,
            menu,
            chat_menu,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            UserViewInput::SwitchTheme(theme) => {
                self.theme = theme;
                self.menu.emit(ParticipantMenuInput::SwitchTheme(theme));
                self.chat_menu
                    .emit(ParticipantChatMenuInput::SwitchTheme(theme));
            }
            UserViewInput::ParticipantUpdated(participant_id) => {
                self.menu
                    .emit(ParticipantMenuInput::ParticipantUpdated(participant_id));
                self.chat_menu
                    .emit(ParticipantChatMenuInput::ParticipantUpdated(participant_id));
            }
            UserViewInput::ChatEnableChanged { issued_by, enabled } => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::ChatEnableChanged { issued_by, enabled });
            }
            UserViewInput::ChatMessageSent { message } => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::MessageSent { message });
            }
            UserViewInput::UnreadCountWasReset => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::UnreadCountWasReset);
            }
        }
    }
}
