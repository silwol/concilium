// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::collections::HashSet;

use gtk::traits::{BoxExt as _, CheckButtonExt as _, OrientableExt as _, WidgetExt as _};
use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentController, AsyncController},
    factory::AsyncFactoryComponent,
    gtk,
    prelude::DynamicIndex,
    AsyncFactorySender, RelmWidgetExt,
};

use crate::{
    meeting::{
        chat::ChatInit,
        participant_chat_menu::{
            ParticipantChatMenu, ParticipantChatMenuInit, ParticipantChatMenuInput,
            ParticipantChatMenuOutput,
        },
        participant_menu::{
            ParticipantMenu, ParticipantMenuInit, ParticipantMenuInput, ParticipantMenuOutput,
        },
        participants::Participants,
    },
    theme::Theme,
};

use super::{ParticipantExtraWidget, ParticipantListInput};

pub struct ParticipantEntry {
    extra_widgets: HashSet<ParticipantExtraWidget>,
    participants: Participants,
    participant_id: ParticipantId,
    menu: AsyncController<ParticipantMenu>,
    chat_menu: AsyncController<ParticipantChatMenu>,
    theme: Theme,
    timer_ready: bool,
}

#[derive(Debug)]
pub struct ParticipantEntryInit {
    pub extra_widgets: HashSet<ParticipantExtraWidget>,
    pub participants: Participants,
    pub participant_id: ParticipantId,
    pub chat_enabled: bool,
    pub theme: Theme,
}

#[derive(Clone, Debug)]
pub enum ParticipantEntryInput {
    SwitchTheme(Theme),
    ParticipantUpdated(ParticipantId),
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessageSent {
        message: MessageSent,
    },
    UnreadCountWasReset,
    TimerReadyStatusUpdated {
        ready: bool,
    },
    ChangeProtocolWritePermissions {
        enabled: bool,
    },
}

#[derive(Debug)]
pub enum ParticipantEntryOutput {
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    SendMessage {
        scope: Scope,
        content: String,
    },
    ResetUnreadCount {
        target: ParticipantId,
    },
    ChangeProtocolWritePermissions {
        target: ParticipantId,
        enabled: bool,
    },
}

#[relm4::factory(async, pub)]
impl AsyncFactoryComponent for ParticipantEntry {
    type Init = ParticipantEntryInit;
    type Input = ParticipantEntryInput;
    type Output = ParticipantEntryOutput;
    type CommandOutput = ();
    type Widgets = ParticipantWidgets;
    type ParentInput = ParticipantListInput;
    type ParentWidget = gtk::Box;

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Horizontal,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 5,
            set_margin_end: 5,

            #[watch]
            set_visible: self.participants.is_present(self.participant_id),

            set_start_widget: Some(&start_box),
            set_end_widget: Some(&end_box)
        },

        #[name = "start_box"]
        gtk::Box {
            set_spacing: 10,

            adw::Avatar {
                set_halign: gtk::Align::Start,
                set_size: 40,
                set_show_initials: true,

                #[watch]
                set_text: Some(&self.participants.display_name_or_default(self.participant_id)),
            },

            gtk::Box {
                set_halign: gtk::Align::Start,
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 3,

                gtk::Box {
                    set_spacing: 10,
                    set_orientation: gtk::Orientation::Horizontal,

                    gtk::Label {
                        set_halign: gtk::Align::Start,
                        add_css_class: "heading",

                        #[watch]
                        set_text: &self.participants.display_name_or_default(self.participant_id),
                    },

                    gtk::Image {
                        #[watch]
                        set_icon_name: Some(self.theme.icon_name("moderator", "moderator-dark")),
                        set_tooltip: "Participant is moderator",

                        #[watch]
                        set_visible: self.participants.is_moderator(self.participant_id),
                    },
                },

                gtk::Label {
                    set_halign: gtk::Align::Start,

                    #[watch]
                    set_label: &self.participants.joined_at_local(self.participant_id).map(
                        |j| j.format("Joined %H:%M").to_string()
                    ).unwrap_or_else(
                        || "Joined at unknown time".to_string()
                    ),
                },
            },
        },

        #[name = "end_box"]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,
            set_spacing: 10,
            set_margin_start: 10,
            set_margin_end: 10,
            add_css_class: "linked",

            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_visible: self.extra_widgets.contains(&ParticipantExtraWidget::ParticipantMenu),
                self.menu.widget(),
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_visible: self.extra_widgets.contains(&ParticipantExtraWidget::ChatMenu),
                self.chat_menu.widget(),
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_visible: self.extra_widgets.contains(&ParticipantExtraWidget::TimerReadyState),

                gtk::CheckButton{
                    set_sensitive:false,
                    #[watch]
                    set_active: self.timer_ready,
                },
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_visible: self.extra_widgets.contains(&ParticipantExtraWidget::ProtocolPermissions),

                gtk::Label {
                    set_text: "Can edit: "
                },

                gtk::CenterBox {
                    set_orientation: gtk::Orientation::Vertical,
                    set_center_widget: Some(&protocol_write_permission),
                },
            },
        },

        #[name = "protocol_write_permission"]
        gtk::Switch {
            #[watch]
            set_state: self.participants.protocol_write_access(self.participant_id),
            #[watch]
            set_active: self.participants.protocol_write_access(self.participant_id),

            connect_state_set[sender] => move |switch, enabled| {
                if switch.state() != enabled {
                    sender.input(ParticipantEntryInput::ChangeProtocolWritePermissions{enabled});
                }
                gtk::Inhibit(false)
            },
        }
    }

    async fn init_model(
        ParticipantEntryInit {
            extra_widgets,
            participants,
            participant_id,
            chat_enabled,
            theme,
        }: Self::Init,
        _index: &DynamicIndex,
        sender: AsyncFactorySender<Self>,
    ) -> Self {
        let menu = ParticipantMenu::builder()
            .launch(ParticipantMenuInit {
                participants: participants.clone(),
                participant_id,
                theme,
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantMenuOutput::GrantModeratorRights { target } => {
                    ParticipantEntryOutput::GrantModeratorRights { target }
                }
                ParticipantMenuOutput::RevokeModeratorRights { target } => {
                    ParticipantEntryOutput::RevokeModeratorRights { target }
                }
            });

        let chat_menu = ParticipantChatMenu::builder()
            .launch(ParticipantChatMenuInit {
                participants: participants.clone(),
                participant_id,
                theme,
                chat: ChatInit {
                    enabled: chat_enabled,
                    actions: Vec::new(),
                    theme,
                    emoji_chooser_enabled: false,
                    chat_name: "Private chat".to_string(),
                    scope: Scope::Private(participant_id),
                },
            })
            .forward(sender.output_sender(), |msg| match msg {
                ParticipantChatMenuOutput::SendMessage { scope, content } => {
                    ParticipantEntryOutput::SendMessage { scope, content }
                }
                ParticipantChatMenuOutput::ResetUnreadCount { target } => {
                    ParticipantEntryOutput::ResetUnreadCount { target }
                }
            });

        ParticipantEntry {
            extra_widgets,
            participants,
            participant_id,
            menu,
            chat_menu,
            theme,
            timer_ready: false,
        }
    }

    fn forward_to_parent(output: ParticipantEntryOutput) -> Option<ParticipantListInput> {
        match output {
            ParticipantEntryOutput::GrantModeratorRights { target } => {
                Some(ParticipantListInput::GrantModeratorRights { target })
            }
            ParticipantEntryOutput::RevokeModeratorRights { target } => {
                Some(ParticipantListInput::RevokeModeratorRights { target })
            }
            ParticipantEntryOutput::SendMessage { scope, content } => {
                Some(ParticipantListInput::SendMessage { scope, content })
            }
            ParticipantEntryOutput::ResetUnreadCount { target } => {
                Some(ParticipantListInput::ResetUnreadCount { target })
            }
            ParticipantEntryOutput::ChangeProtocolWritePermissions { target, enabled } => {
                Some(ParticipantListInput::ChangeProtocolWritePermissions { target, enabled })
            }
        }
    }

    async fn update(&mut self, msg: Self::Input, sender: AsyncFactorySender<Self>) {
        match msg {
            ParticipantEntryInput::SwitchTheme(theme) => {
                self.menu.emit(ParticipantMenuInput::SwitchTheme(theme));
                self.chat_menu
                    .emit(ParticipantChatMenuInput::SwitchTheme(theme));
                self.theme = theme;
            }
            ParticipantEntryInput::ParticipantUpdated(participant_id) => {
                self.menu
                    .emit(ParticipantMenuInput::ParticipantUpdated(participant_id));
                self.chat_menu
                    .emit(ParticipantChatMenuInput::ParticipantUpdated(participant_id));
            }
            ParticipantEntryInput::ChatEnableChanged { issued_by, enabled } => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::ChatEnableChanged { issued_by, enabled });
            }
            ParticipantEntryInput::ChatMessageSent { message } => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::MessageSent { message });
            }
            ParticipantEntryInput::UnreadCountWasReset => {
                self.chat_menu
                    .emit(ParticipantChatMenuInput::UnreadCountWasReset);
            }
            ParticipantEntryInput::TimerReadyStatusUpdated { ready } => {
                self.timer_ready = ready;
            }
            ParticipantEntryInput::ChangeProtocolWritePermissions { enabled } => {
                sender.output(ParticipantEntryOutput::ChangeProtocolWritePermissions {
                    target: self.participant_id,
                    enabled,
                });
            }
        }
    }
}

impl ParticipantEntry {
    pub fn is(&self, id: ParticipantId) -> bool {
        self.participant_id == id
    }
}
