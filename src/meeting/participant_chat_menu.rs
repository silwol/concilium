// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::WidgetExt;
use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender,
};

use crate::{meeting::chat::ChatOutput, theme::Theme};

use super::{
    chat::{Chat, ChatAction, ChatInit, ChatInput},
    participants::Participants,
};

pub struct ParticipantChatMenu {
    participants: Participants,
    participant_id: ParticipantId,
    theme: Theme,
    chat: AsyncController<Chat>,
    unread_count: usize,
    message_count: usize,
}

pub struct ParticipantChatMenuInit {
    pub participants: Participants,
    pub participant_id: ParticipantId,
    pub theme: Theme,
    pub chat: ChatInit,
}

#[derive(Debug)]
pub enum ParticipantChatMenuInput {
    SwitchTheme(Theme),
    ParticipantUpdated(ParticipantId),
    SendMessage {
        scope: Scope,
        content: String,
    },
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    MessageSent {
        message: MessageSent,
    },
    ResetUnreadCount,
    UnreadCountWasReset,
}

#[derive(Debug)]
pub enum ParticipantChatMenuOutput {
    SendMessage { scope: Scope, content: String },
    ResetUnreadCount { target: ParticipantId },
}

#[relm4::component(async, pub)]
impl AsyncComponent for ParticipantChatMenu {
    type Init = ParticipantChatMenuInit;
    type Input = ParticipantChatMenuInput;
    type Output = ParticipantChatMenuOutput;
    type CommandOutput = ();

    view! {
        #[root]
        #[name = "menu_button"]
        gtk::MenuButton {
            set_popover: Some(&popover),

            add_css_class: "flat",

            set_child: Some(&button_label),

            #[watch]
            set_visible: model.participants.current_participant_id() != model.participant_id,
        },

        #[name = "popover"]
        gtk::Popover {
            model.chat.widget(),

            connect_show[sender] => move |_| {
                sender.input(ParticipantChatMenuInput::ResetUnreadCount);
            }
        },

        #[name = "button_label"]
        gtk::Overlay {
            add_overlay: &unread_count,
            add_overlay: &message_count,

            gtk::Image {
                #[watch]
                set_icon_name: Some(model.theme.icon_name("chat", "chat-dark")),
            },
        },

        #[name = "unread_count"]
        gtk::Label {
            set_halign: gtk::Align::End,
            set_valign: gtk::Align::Start,
            set_margin_top: 4,

            add_css_class: "unread_indicator",

            #[watch]
            set_visible: model.unread_count > 0,

            #[watch]
            set_text: &format!("{}", model.unread_count),
        },

        #[name = "message_count"]
        gtk::Label {
            set_halign: gtk::Align::End,
            set_valign: gtk::Align::Start,
            set_margin_top: 4,
            add_css_class: "all_read_indicator",

            #[watch]
            set_visible: model.message_count > 0 && model.unread_count == 0,

            #[watch]
            set_text: &format!("{}",model.message_count),
        }
    }

    async fn init(
        ParticipantChatMenuInit {
            participants,
            participant_id,
            theme,
            chat,
        }: ParticipantChatMenuInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let chat = Chat::builder()
            .launch(chat)
            .forward(sender.input_sender(), |msg| match msg {
                ChatOutput::SendMessage { scope, content } => {
                    ParticipantChatMenuInput::SendMessage { scope, content }
                }
            });

        let model = ParticipantChatMenu {
            participants,
            participant_id,
            theme,
            chat,
            unread_count: 0,
            message_count: 0,
        };
        let widgets = view_output!();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            ParticipantChatMenuInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
            ParticipantChatMenuInput::ParticipantUpdated(_participant_id) => {}
            ParticipantChatMenuInput::SendMessage { scope, content } => {
                if !content.is_empty() {
                    sender
                        .output(ParticipantChatMenuOutput::SendMessage { content, scope })
                        .unwrap();
                }
            }
            ParticipantChatMenuInput::ChatEnableChanged { issued_by, enabled } => {
                let display_name = issued_by
                    .as_ref()
                    .map(|p| self.participants.display_name_or_default(*p))
                    .unwrap_or_else(|| "unknown".to_string());
                self.chat
                    .emit(ChatInput::Action(ChatAction::ChatEnableChanged {
                        display_name,
                        issued_by,
                        enabled,
                    }));
            }
            ParticipantChatMenuInput::MessageSent { message } => {
                let display_name = self.participants.display_name_or_default(message.source);
                let is_moderator = self.participants.is_moderator(message.source);

                self.chat.emit(ChatInput::Action(ChatAction::Message {
                    display_name,
                    is_moderator,
                    participant_id: message.source,
                    content: message.content,
                }));

                if message.source != self.participants.current_participant_id() {
                    self.unread_count = self.unread_count.saturating_add(1);
                }
                self.message_count = self.message_count.saturating_add(1);
            }
            ParticipantChatMenuInput::ResetUnreadCount => {
                sender
                    .output(ParticipantChatMenuOutput::ResetUnreadCount {
                        target: self.participant_id,
                    })
                    .unwrap();
            }

            ParticipantChatMenuInput::UnreadCountWasReset => {
                self.unread_count = 0;
            }
        }
    }
}
