// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::collections::HashSet;

use gtk::traits::{OrientableExt as _, WidgetExt as _};
use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    factory::AsyncFactoryVecDeque,
    gtk, AsyncComponentSender,
};

use crate::theme::Theme;

use self::entry::{ParticipantEntry, ParticipantEntryInit, ParticipantEntryInput};

use super::participants::Participants;

mod entry;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum ParticipantExtraWidget {
    ParticipantMenu,
    ChatMenu,
    TimerReadyState,
    ProtocolPermissions,
}

impl ParticipantExtraWidget {
    pub fn default_extra_widgets() -> HashSet<ParticipantExtraWidget> {
        HashSet::from_iter([Self::ParticipantMenu, Self::ChatMenu])
    }
}

#[derive(Debug)]
pub struct ParticipantListInit {
    pub extra_widgets: HashSet<ParticipantExtraWidget>,
    pub participants: Participants,
    pub chat_enabled: bool,
    pub theme: Theme,
}

#[derive(Debug)]
pub enum ParticipantListInput {
    ParticipantJoined(ParticipantId),
    ParticipantUpdated(ParticipantId),
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    SwitchTheme(Theme),
    SendMessage {
        scope: Scope,
        content: String,
    },
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessageSent {
        message: MessageSent,
    },
    ResetUnreadCount {
        target: ParticipantId,
    },
    UnreadCountWasReset {
        target: ParticipantId,
    },
    TimerReadyStatusUpdated {
        participant_id: ParticipantId,
        ready: bool,
    },
    ChangeProtocolWritePermissions {
        target: ParticipantId,
        enabled: bool,
    },
}

pub struct ParticipantList {
    extra_widgets: HashSet<ParticipantExtraWidget>,
    participants: Participants,
    participant_widgets: AsyncFactoryVecDeque<ParticipantEntry>,
    chat_enabled: bool,
    theme: Theme,
}

#[derive(Debug)]
pub enum ParticipantListOutput {
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    SendMessage {
        scope: Scope,
        content: String,
    },
    ResetUnreadCount {
        target: ParticipantId,
    },
    ChangeProtocolWritePermissions {
        target: ParticipantId,
        enabled: bool,
    },
}

#[relm4::component(async, pub)]
impl AsyncComponent for ParticipantList {
    type Init = ParticipantListInit;
    type Input = ParticipantListInput;
    type Output = ParticipantListOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            #[name = "scroll"]
            gtk::ScrolledWindow {
                set_vexpand: true,
                set_hscrollbar_policy: gtk::PolicyType::Automatic,
                set_vscrollbar_policy: gtk::PolicyType::Always,
                set_propagate_natural_width: true,
                model.participant_widgets.widget(),
            }
        }
    }

    async fn init(
        ParticipantListInit {
            extra_widgets,
            participants,
            chat_enabled,
            theme,
        }: ParticipantListInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let mut participant_widgets = AsyncFactoryVecDeque::new(
            gtk::Box::builder()
                .orientation(gtk::Orientation::Vertical)
                .spacing(10)
                .margin_start(5)
                .margin_end(5)
                .margin_top(5)
                .margin_bottom(5)
                .build(),
            sender.input_sender(),
        );

        {
            let mut guard = participant_widgets.guard();
            for participant_id in participants.get_all_ids() {
                guard.push_back(ParticipantEntryInit {
                    extra_widgets: extra_widgets.clone(),
                    participants: participants.clone(),
                    participant_id,
                    chat_enabled,
                    theme,
                });
            }
        }

        let model = Self {
            extra_widgets,
            participants,
            participant_widgets,
            chat_enabled,
            theme,
        };

        let widgets = view_output!();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            ParticipantListInput::ParticipantJoined(participant_id) => {
                if self.get_participant_list_index(participant_id).is_none() {
                    let entry = ParticipantEntryInit {
                        extra_widgets: self.extra_widgets.clone(),
                        participants: self.participants.clone(),
                        participant_id,
                        chat_enabled: self.chat_enabled,
                        theme: self.theme,
                    };

                    let mut guard = self.participant_widgets.guard();
                    guard.push_back(entry);
                }
            }
            ParticipantListInput::ParticipantUpdated(participant_id) => {
                self.participant_widgets
                    .broadcast(ParticipantEntryInput::ParticipantUpdated(participant_id));
            }
            ParticipantListInput::GrantModeratorRights { target } => {
                sender
                    .output(ParticipantListOutput::GrantModeratorRights { target })
                    .unwrap();
            }
            ParticipantListInput::RevokeModeratorRights { target } => {
                sender
                    .output(ParticipantListOutput::RevokeModeratorRights { target })
                    .unwrap();
            }
            ParticipantListInput::SendMessage { scope, content } => {
                sender
                    .output(ParticipantListOutput::SendMessage { scope, content })
                    .unwrap();
            }
            ParticipantListInput::SwitchTheme(theme) => {
                self.participant_widgets
                    .broadcast(ParticipantEntryInput::SwitchTheme(theme));
                self.theme = theme;
            }
            ParticipantListInput::ChatEnableChanged { issued_by, enabled } => {
                self.chat_enabled = enabled;
                self.participant_widgets
                    .broadcast(ParticipantEntryInput::ChatEnableChanged { issued_by, enabled });
            }
            ParticipantListInput::ChatMessageSent { message } => {
                if let Scope::Private(receiver) = message.scope {
                    let other_participant_id =
                        if self.participants.current_participant_id() == message.source {
                            receiver
                        } else {
                            message.source
                        };
                    if let Some(index) = self.get_participant_list_index(other_participant_id) {
                        self.participant_widgets
                            .send(index, ParticipantEntryInput::ChatMessageSent { message });
                    }
                }
            }
            ParticipantListInput::ResetUnreadCount { target } => {
                sender
                    .output(ParticipantListOutput::ResetUnreadCount { target })
                    .unwrap();
            }
            ParticipantListInput::UnreadCountWasReset { target } => {
                if let Some(index) = self.get_participant_list_index(target) {
                    self.participant_widgets
                        .send(index, ParticipantEntryInput::UnreadCountWasReset);
                }
            }
            ParticipantListInput::TimerReadyStatusUpdated {
                participant_id,
                ready,
            } => {
                if let Some(index) = self.get_participant_list_index(participant_id) {
                    self.participant_widgets.send(
                        index,
                        ParticipantEntryInput::TimerReadyStatusUpdated { ready },
                    );
                }
            }
            ParticipantListInput::ChangeProtocolWritePermissions { target, enabled } => {
                sender
                    .output(ParticipantListOutput::ChangeProtocolWritePermissions {
                        target,
                        enabled,
                    })
                    .unwrap();
            }
        }
    }
}

impl ParticipantList {
    fn get_participant_list_index(&self, id: ParticipantId) -> Option<usize> {
        self.participant_widgets.iter().position(|p| {
            matches!(
                p,
                Some(p) if p.is(id)
            )
        })
    }
}
