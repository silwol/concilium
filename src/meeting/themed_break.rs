// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::{
    glib,
    traits::{BoxExt, ButtonExt, OrientableExt, WidgetExt},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};

use crate::{
    meeting::TIMER_STATE,
    theme::Theme,
    timer_ext::{ThemedBreakInfo, TimerStartedExt},
};

pub struct ThemedBreak {
    theme: Theme,
    info: Option<ThemedBreakInfo>,
}

impl ThemedBreak {
    fn is_alert(&self) -> bool {
        self.info.as_ref().map(|i| i.is_alert).unwrap_or_default()
    }

    fn alert_css_class(&self) -> &'static str {
        if self.is_alert() {
            "alert"
        } else {
            ""
        }
    }

    fn is_finished(&self) -> bool {
        self.info.is_none()
    }

    fn break_label(&self) -> &'static str {
        match self.info {
            Some(ref info) if info.timer_theme == Some("coffee-break".to_string()) => {
                "Tea and coffee break!"
            }
            Some(_) | None => "Break!",
        }
    }

    fn duration_label<'a>(&'a self) -> &'a str {
        self.info
            .as_ref()
            .map(|i| i.duration_label.as_str())
            .unwrap_or_else(|| "")
    }
}

pub struct ThemedBreakInit {
    pub theme: Theme,
}

#[derive(Debug, Clone)]
pub enum ThemedBreakInput {
    Tick,
    SwitchTheme(Theme),
}

#[derive(Debug, Clone)]
pub enum ThemedBreakOutput {
    ReturnToConference,
    TimerFinished,
}

#[relm4::component(async, pub)]
impl AsyncComponent for ThemedBreak {
    type Init = ThemedBreakInit;
    type Input = ThemedBreakInput;
    type Output = ThemedBreakOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::CenterBox {
            add_css_class: "splash",
            set_orientation: gtk::Orientation::Vertical,
            set_center_widget: Some(&main),
            set_vexpand: true,
            set_hexpand: true,
        },

        #[name = "main"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            gtk::Image {
                #[watch]
                set_icon_name: Some(model.theme.icon_name("beverage", "beverage-dark")),
                    set_pixel_size: 250,
            },

            gtk::Label {
                set_label: model.break_label(),

                add_css_class: "title-3",
                add_css_class: "large",

                #[watch]
                set_visible: model.info.is_some(),

                #[track = "model.is_alert()"]
                add_css_class: &model.alert_css_class(),
            },

            gtk::Label {
                set_label: "Break is over.",

                add_css_class: "title-1",
                add_css_class: "large",

                #[watch]
                set_visible: model.info.is_none(),
            },

            gtk::Label {
                #[watch]
                set_label: if model.is_finished() { "" } else { &model.duration_label() },
                #[watch]
                set_visible: !model.is_finished(),
                add_css_class: "title-1",
                add_css_class: "large",

                #[track = "model.is_alert()"]
                add_css_class: &model.alert_css_class(),
            },

            gtk::CenterBox {
                set_center_widget: Some(&back_button),
            },
        },

        #[name = "back_button"]
        gtk::Button {
            set_margin_top: 40,
            set_label: "Back to the conference",
            add_css_class: "pill",
            add_css_class: "suggested-action",
            connect_clicked[sender] => move|_| {
                sender.output(ThemedBreakOutput::ReturnToConference).unwrap();
            },
        }
    }

    async fn init(
        ThemedBreakInit { theme }: ThemedBreakInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let info = if let Some(started) = &*TIMER_STATE.read() {
            started.themed_break_info()
        } else {
            None
        };
        TIMER_STATE.subscribe(sender.input_sender(), |_| ThemedBreakInput::Tick);

        let model = Self { theme, info };
        let widgets = view_output!();

        glib::timeout_add_seconds(1, move || {
            let success = sender.input_sender().send(ThemedBreakInput::Tick).is_ok();
            glib::Continue(success)
        });

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: ThemedBreakInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            ThemedBreakInput::Tick => {
                let was_active = self.info.take().is_some();
                self.info = if let Some(timer) = &*TIMER_STATE.read() {
                    timer.themed_break_info()
                } else {
                    None
                };
                if was_active && self.info.is_none() {
                    sender.output(ThemedBreakOutput::TimerFinished).unwrap();
                }
            }
            ThemedBreakInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
        }
    }
}
