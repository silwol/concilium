// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt, ButtonExt, OrientableExt, WidgetExt};
use opentalk_types::{core::ParticipantId, signaling::control::Participant};
use relm4::{
    adw, factory::AsyncFactoryComponent, gtk, prelude::DynamicIndex, AsyncFactorySender,
    RelmWidgetExt,
};

use crate::participant_ext::ParticipantExt;

use super::WaitingParticipantsListInput;

pub struct WaitingParticipantEntry {
    participant: Participant,
}

#[derive(Debug)]
pub struct WaitingParticipantEntryInit {
    pub participant: Participant,
}

#[derive(Debug)]
pub enum WaitingParticipantEntryInput {
    Accept,
}

#[derive(Debug)]
pub enum WaitingParticipantEntryOutput {
    Accept { target: ParticipantId },
}

#[relm4::factory(async, pub)]
impl AsyncFactoryComponent for WaitingParticipantEntry {
    type Init = WaitingParticipantEntryInit;
    type Input = WaitingParticipantEntryInput;
    type Output = WaitingParticipantEntryOutput;
    type CommandOutput = ();
    type Widgets = WaitingParticipantWidgets;
    type ParentInput = WaitingParticipantsListInput;
    type ParentWidget = gtk::Box;

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Horizontal,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 5,
            set_margin_end: 5,

            set_start_widget: Some(&start_box),
            set_end_widget: Some(&end_box)
        },

        #[name = "start_box"]
        gtk::Box {
            set_spacing: 10,

            adw::Avatar {
                set_size: 25,
                set_show_initials: true,

                set_text: Some(&self.participant.display_name_or_default()),
            },

            gtk::Label {
                set_text = &self.participant.display_name_or_default(),
            }
        },

        #[name = "end_box"]
        gtk::Box {
            gtk::Button {
                set_icon_name: "object-select-symbolic",
                add_css_class: "suggested-action",
                set_tooltip: "Allow participant to join meeting",

                connect_clicked => WaitingParticipantEntryInput::Accept
            },
        }
    }

    async fn init_model(
        WaitingParticipantEntryInit { participant }: Self::Init,
        _index: &DynamicIndex,
        _sender: AsyncFactorySender<Self>,
    ) -> Self {
        WaitingParticipantEntry { participant }
    }

    fn forward_to_parent(
        output: WaitingParticipantEntryOutput,
    ) -> Option<WaitingParticipantsListInput> {
        match output {
            WaitingParticipantEntryOutput::Accept { target } => {
                Some(WaitingParticipantsListInput::Accept { target })
            }
        }
    }

    async fn update(&mut self, msg: Self::Input, sender: AsyncFactorySender<Self>) {
        match msg {
            WaitingParticipantEntryInput::Accept => {
                sender.output(WaitingParticipantEntryOutput::Accept {
                    target: self.participant.id,
                });
            }
        }
    }
}

impl WaitingParticipantEntry {
    pub fn is(&self, id: ParticipantId) -> bool {
        self.participant.id == id
    }
}
