// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::WidgetExt;
use opentalk_types::{
    core::ParticipantId,
    signaling::chat::{event::MessageSent, Scope},
};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    factory::AsyncFactoryVecDeque,
    gtk::{self, traits::OrientableExt as _},
    AsyncComponentSender,
};

use crate::{meeting::video_grid::entry::VideoGridEntryInit, theme::Theme};

use self::entry::{VideoGridEntry, VideoGridEntryInput};

use super::participants::Participants;

mod entry;

const MAX_PARTICIPANTS_PER_ROW: u32 = 3;

pub struct VideoGrid {
    participant_widgets: AsyncFactoryVecDeque<VideoGridEntry>,
    participants: Participants,
    max_children_per_row: u32,
    chat_enabled: bool,
    theme: Theme,
}

#[derive(Debug)]
pub struct VideoGridInit {
    pub participants: Participants,
    pub chat_enabled: bool,
    pub theme: Theme,
}

#[derive(Debug)]
pub enum VideoGridInput {
    ParticipantJoined(ParticipantId),
    ParticipantLeft(ParticipantId),
    ParticipantUpdated(ParticipantId),
    SwitchTheme(Theme),
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    SendMessage {
        scope: Scope,
        content: String,
    },
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        enabled: bool,
    },
    ChatMessageSent {
        message: MessageSent,
    },
    ResetUnreadCount {
        target: ParticipantId,
    },
    UnreadCountWasReset {
        target: ParticipantId,
    },
}

#[derive(Debug)]
pub enum VideoGridOutput {
    GrantModeratorRights { target: ParticipantId },
    RevokeModeratorRights { target: ParticipantId },
    SendMessage { scope: Scope, content: String },
    ResetUnreadCount { target: ParticipantId },
}

#[relm4::component(async, pub)]
impl AsyncComponent for VideoGrid {
    type Init = VideoGridInit;
    type Input = VideoGridInput;
    type Output = VideoGridOutput;
    type CommandOutput = ();

    view! {
        #[root]
        #[name = "flowbox"]
        gtk::FlowBox {
            set_orientation: gtk::Orientation::Horizontal,
            set_column_spacing: 15,
            set_row_spacing: 15,
            set_margin_start: 10,
            set_margin_end: 10,
            set_margin_top: 10,
            set_margin_bottom: 10,
            set_min_children_per_line: 1,
            set_selection_mode: gtk::SelectionMode::None,
            set_homogeneous: true,
            set_hexpand: true,
            set_vexpand: true,
            set_valign: gtk::Align::Center,

            #[watch]
            set_max_children_per_line: model.max_children_per_row,
        }
    }

    async fn init(
        VideoGridInit {
            participants,
            chat_enabled,
            theme,
        }: VideoGridInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let participants_filtered: Vec<_> = participants.get_all_present().into_values().collect();

        let mut model = Self {
            participant_widgets: AsyncFactoryVecDeque::new(
                gtk::FlowBox::new(),
                sender.input_sender(),
            ),
            participants: participants.clone(),
            max_children_per_row: MAX_PARTICIPANTS_PER_ROW,
            chat_enabled,
            theme,
        };
        model.update_max_participants(participants_filtered.len());

        let widgets = view_output!();

        let mut participant_widgets =
            AsyncFactoryVecDeque::new(widgets.flowbox.clone(), sender.input_sender());
        {
            let mut guard = participant_widgets.guard();
            for participant in participants_filtered {
                if participant
                    .participant_data
                    .control
                    .as_ref()
                    .map(|c| c.left_at.is_none())
                    .unwrap_or_default()
                {
                    guard.push_back(VideoGridEntryInit {
                        participants: participants.clone(),
                        participant_id: participant.id,
                        chat_enabled,
                        theme,
                    });
                }
            }
        }

        model.participant_widgets = participant_widgets;

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: VideoGridInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            VideoGridInput::ParticipantJoined(participant_id) => {
                let entry = VideoGridEntryInit {
                    participants: self.participants.clone(),
                    participant_id,
                    chat_enabled: self.chat_enabled,
                    theme: self.theme,
                };
                let count = {
                    let mut guard = self.participant_widgets.guard();
                    guard.push_back(entry);
                    guard.len()
                };
                self.update_max_participants(count);
            }
            VideoGridInput::ParticipantLeft(participant_id) => {
                if let Some(index) = self.get_video_grid_index(participant_id) {
                    let count = {
                        let mut guard = self.participant_widgets.guard();
                        guard.remove(index);
                        guard.len()
                    };
                    self.update_max_participants(count);
                }
            }
            VideoGridInput::ParticipantUpdated(participant_id) => {
                self.participant_widgets
                    .broadcast(VideoGridEntryInput::ParticipantUpdated(participant_id));
            }
            VideoGridInput::SwitchTheme(theme) => {
                self.theme = theme;
                self.participant_widgets
                    .broadcast(VideoGridEntryInput::SwitchTheme(theme));
            }
            VideoGridInput::GrantModeratorRights { target } => {
                sender
                    .output(VideoGridOutput::GrantModeratorRights { target })
                    .unwrap();
            }
            VideoGridInput::RevokeModeratorRights { target } => {
                sender
                    .output(VideoGridOutput::RevokeModeratorRights { target })
                    .unwrap();
            }
            VideoGridInput::SendMessage { scope, content } => {
                sender
                    .output(VideoGridOutput::SendMessage { scope, content })
                    .unwrap();
            }
            VideoGridInput::ChatEnableChanged { issued_by, enabled } => {
                self.chat_enabled = enabled;
                self.participant_widgets
                    .broadcast(VideoGridEntryInput::ChatEnableChanged { issued_by, enabled });
            }
            VideoGridInput::ChatMessageSent { message } => {
                if let Scope::Private(receiver) = message.scope {
                    let other_participant_id =
                        if self.participants.current_participant_id() == message.source {
                            receiver
                        } else {
                            message.source
                        };
                    if let Some(index) = self.get_video_grid_index(other_participant_id) {
                        self.participant_widgets
                            .send(index, VideoGridEntryInput::ChatMessageSent { message });
                    }
                }
            }
            VideoGridInput::ResetUnreadCount { target } => {
                sender
                    .output(VideoGridOutput::ResetUnreadCount { target })
                    .unwrap();
            }
            VideoGridInput::UnreadCountWasReset { target } => {
                if let Some(index) = self.get_video_grid_index(target) {
                    self.participant_widgets
                        .send(index, VideoGridEntryInput::UnreadCountWasReset);
                }
            }
        }
    }
}

impl VideoGrid {
    fn get_video_grid_index(&self, id: ParticipantId) -> Option<usize> {
        self.participant_widgets
            .iter()
            .position(|p| matches!(p, Some(p) if p.is(id)))
    }

    fn update_max_participants(&mut self, current_count: usize) {
        self.max_children_per_row = std::cmp::min(MAX_PARTICIPANTS_PER_ROW, current_count as u32);
    }
}
