// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt as _, GridExt, OrientableExt as _, WidgetExt as _};
use relm4::{adw, factory::AsyncFactoryComponent, gtk, prelude::DynamicIndex, AsyncFactorySender};

use crate::theme::Theme;

use super::{ChatAction, ChatInput};

impl ChatAction {
    fn is_status_info(&self) -> bool {
        matches!(
            self,
            ChatAction::Join { .. }
                | ChatAction::Leave { .. }
                | ChatAction::ChatEnableChanged { .. }
        )
    }

    fn is_moderator(&self) -> bool {
        matches!(self,
            ChatAction::Message { is_moderator, .. } if *is_moderator)
    }

    fn status_info_markup(&self) -> Option<String> {
        match self {
            ChatAction::Join { display_name, .. } => {
                Some(format!("<b>{}</b> joined the call", display_name))
            }
            ChatAction::Leave { display_name, .. } => {
                Some(format!("<b>{}</b> left the call", display_name))
            }
            ChatAction::ChatEnableChanged {
                display_name,
                enabled,
                ..
            } => {
                let enabled = if *enabled {
                    "enabled chat"
                } else {
                    "disabled chat"
                };
                Some(format!("Moderator <b>{}</b> {}", display_name, enabled))
            }
            ChatAction::Message { .. } => None,
        }
    }

    fn is_message(&self) -> bool {
        matches!(self, ChatAction::Message { .. })
    }

    fn message(&self) -> Option<&str> {
        match self {
            ChatAction::Join { .. }
            | ChatAction::Leave { .. }
            | ChatAction::ChatEnableChanged { .. } => None,
            ChatAction::Message { content, .. } => Some(content),
        }
    }

    fn sender_display_name(&self) -> Option<&str> {
        match self {
            ChatAction::ChatEnableChanged { .. } => None,
            ChatAction::Join { display_name, .. }
            | ChatAction::Leave { display_name, .. }
            | ChatAction::Message { display_name, .. } => Some(display_name),
        }
    }
}

#[derive(Debug)]
pub struct ChatEntry {
    action: ChatAction,
    theme: Theme,
}

pub struct ChatEntryInit {
    pub action: ChatAction,
    pub theme: Theme,
}

#[relm4::factory(async, pub)]
impl AsyncFactoryComponent for ChatEntry {
    type Init = ChatEntryInit;
    type Input = ();
    type Output = ();
    type CommandOutput = ();
    type Widgets = ChatWidgets;
    type ParentInput = ChatInput;
    type ParentWidget = gtk::Box;

    view! {
        #[root]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 20,

            gtk::CenterBox {
                set_visible: self.action.is_status_info(),
                set_orientation: gtk::Orientation::Horizontal,

                set_start_widget: Some(
                    &gtk::Separator::builder()
                        .valign(gtk::Align::Center)
                        .halign(gtk::Align::Fill)
                        .hexpand(true)
                        .orientation(gtk::Orientation::Horizontal)
                        .build()
                ),
                set_center_widget: Some(&status_info_label),
                set_end_widget: Some(
                    &gtk::Separator::builder()
                        .valign(gtk::Align::Center)
                        .halign(gtk::Align::Fill)
                        .hexpand(true)
                        .orientation(gtk::Orientation::Horizontal)
                        .build()
                ),
            },

            gtk::Grid {
                set_visible: self.action.is_message(),
                set_column_spacing: 10,
                set_row_spacing: 2,

                attach: (&avatar, 0, 0, 1, 1),
                attach: (&moderator_details, 1, 0, 1, 1),
                attach: (&message, 1, 1, 2, 1),
            },
        },

        #[name = "status_info_label"]
        gtk::Label {
            set_markup: &self.action.status_info_markup().unwrap_or_default(),
            set_halign: gtk::Align::Center,
            set_margin_start: 10,
            set_margin_end: 10,
        },

        #[name = "avatar"]
        adw::Avatar {
            set_size: 30,
            set_show_initials: true,
            set_halign: gtk::Align::Start,

            #[watch]
            set_text: Some(self.action.sender_display_name().unwrap_or("unknown")),
        },

        #[name = "moderator_details"]
        gtk::Box {
            set_spacing: 10,

            gtk::Label {
                add_css_class: "heading",
                set_halign: gtk::Align::Start,
                set_text: self.action.sender_display_name().unwrap_or("unknown"),
            },

            gtk::Image {
                set_halign: gtk::Align::Start,

                #[watch]
                set_icon_name: Some(self.theme.icon_name("moderator", "moderator-dark")),

                #[watch]
                set_visible: self.action.is_moderator(),
            },
        },

        #[name = "message"]
        gtk::Label {
            set_halign: gtk::Align::Start,
            set_wrap: true,
            set_text: self.action.message().unwrap_or_default(),
        }
    }

    async fn init_model(
        ChatEntryInit { action, theme }: ChatEntryInit,
        _index: &DynamicIndex,
        _sender: AsyncFactorySender<Self>,
    ) -> Self {
        Self { action, theme }
    }

    async fn update(&mut self, _msg: Self::Input, _sender: AsyncFactorySender<Self>) {}
}
