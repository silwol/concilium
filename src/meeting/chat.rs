// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::{
    traits::{
        AdjustmentExt as _, EditableExt as _, EntryExt as _, OrientableExt as _, PopoverExt as _,
        WidgetExt as _,
    },
    EntryIconPosition,
};
use opentalk_types::{core::ParticipantId, signaling::chat::Scope};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentParts},
    factory::AsyncFactoryVecDeque,
    gtk::{self, gdk},
    AsyncComponentSender,
};

use crate::{meeting::chat::entry::ChatEntryInit, theme::Theme};

use self::entry::ChatEntry;

mod entry;

#[derive(Debug)]
pub enum ChatInput {
    Action(ChatAction),
    UpdateSendText(String),
    SendMessage,
    SwitchTheme(Theme),
    EntryIconPressed(EntryIconPosition, gdk::Rectangle),
    EmojiPicked(String),
}

#[derive(Debug, Clone)]
pub enum ChatAction {
    Join {
        participant_id: ParticipantId,
        display_name: String,
    },
    Leave {
        participant_id: ParticipantId,
        display_name: String,
    },
    ChatEnableChanged {
        issued_by: Option<ParticipantId>,
        display_name: String,
        enabled: bool,
    },
    Message {
        participant_id: ParticipantId,
        display_name: String,
        content: String,
        is_moderator: bool,
    },
}

#[derive(Debug)]
pub struct ChatInit {
    pub enabled: bool,
    pub actions: Vec<ChatAction>,
    pub theme: Theme,
    pub emoji_chooser_enabled: bool,
    pub chat_name: String,
    pub scope: Scope,
}

#[derive(Debug)]
pub enum ChatOutput {
    SendMessage { scope: Scope, content: String },
}

#[derive(Debug)]
pub struct Chat {
    enabled: bool,
    entries: AsyncFactoryVecDeque<ChatEntry>,
    send_text: String,
    send_text_changed: bool,
    theme: Theme,
    emoji_chooser_enabled: bool,
    emoji_chooser_pointing_to: gdk::Rectangle,
    open_emoji_chooser: bool,
    picked_emoji: String,
    chat_name: String,
    scope: Scope,
}

#[relm4::component(async, pub)]
impl AsyncComponent for Chat {
    type Init = ChatInit;
    type Input = ChatInput;
    type Output = ChatOutput;
    type CommandOutput = ();

    view! {
        #[root]
        #[name = "main_box"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,

            gtk::CenterBox{
                set_orientation: gtk::Orientation::Vertical,
                set_vexpand: true,

                #[watch]
                set_visible: model.entries.is_empty(),

                set_center_widget: Some(&chat_empty_status_page),
            },

            #[name = "scroll"]
            gtk::ScrolledWindow {
                #[watch]
                set_visible: !model.entries.is_empty(),

                set_vexpand: true,
                set_hscrollbar_policy: gtk::PolicyType::Automatic,
                set_vscrollbar_policy: gtk::PolicyType::Always,
                set_propagate_natural_width: true,
                set_propagate_natural_height: true,
                model.entries.widget(),
            },

            #[name = "entry"]
            gtk::Entry {
                set_margin_top: 5,
                set_margin_bottom: 5,
                set_margin_start: 5,
                set_margin_end: 5,

                #[watch]
                set_placeholder_text: Some(if model.enabled { "type a message…" } else { "chat is disabled" }),
                set_hexpand: true,

                set_primary_icon_name: if model.emoji_chooser_enabled { Some("face-smile-symbolic") } else { None },

                #[watch]
                set_secondary_icon_name: Some(model.theme.icon_name("send", "send-dark")),
                set_activates_default: true,

                connect_icon_press[sender] => move |widget, pos| {
                    let area = widget.icon_area(pos);
                    sender.input(ChatInput::EntryIconPressed(pos, area));
                },

                #[track = "model.send_text_changed"]
                set_text: &model.send_text,
                #[watch]
                set_sensitive: model.enabled,
                connect_changed[sender] => move |entry| {
                    sender.input(Self::Input::UpdateSendText(entry.text().to_string()));
                },

                #[track = "!model.picked_emoji.is_empty()"]
                insert_emoji: &model.picked_emoji,

                #[watch]
                set_secondary_icon_sensitive: model.enabled && !model.send_text.is_empty(),

                connect_activate => Self::Input::SendMessage,
            },
        },

        #[name = "emoji_chooser"]
        gtk::EmojiChooser {
            set_parent: &entry,

            #[watch]
            set_pointing_to: Some(&model.emoji_chooser_pointing_to),

            #[track = "model.open_emoji_chooser"]
            open: model.open_emoji_chooser,

            connect_emoji_picked[sender] => move |_chooser, value| {
                sender.input(ChatInput::EmojiPicked(value.to_string()));
            }
        },

        #[name = "chat_empty_status_page"]
        adw::StatusPage {

            #[watch]
            set_icon_name: Some(model.theme.icon_name("chat", "chat-dark")),

            set_title: &model.chat_name,
            set_description: Some("No messeges here yet…"),
        }
    }

    async fn init(
        ChatInit {
            enabled,
            actions,
            theme,
            emoji_chooser_enabled,
            chat_name,
            scope,
        }: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let mut entries = AsyncFactoryVecDeque::new(
            gtk::Box::builder()
                .orientation(gtk::Orientation::Vertical)
                .spacing(15)
                .margin_start(10)
                .margin_end(10)
                .margin_top(10)
                .margin_bottom(10)
                .build(),
            sender.input_sender(),
        );

        {
            let mut guard = entries.guard();
            for action in actions {
                guard.push_back(ChatEntryInit { action, theme });
            }
        }

        let model = Self {
            enabled,
            entries,
            send_text: String::new(),
            send_text_changed: false,
            theme,
            emoji_chooser_enabled,
            emoji_chooser_pointing_to: gdk::Rectangle::new(0, 0, 1, 1),
            open_emoji_chooser: false,
            picked_emoji: "".to_string(),
            chat_name,
            scope,
        };

        let widgets = view_output!();
        let scroll = widgets.scroll.clone();
        let adj = scroll.vadjustment();
        adj.connect_upper_notify(move |_| {
            scroll.emit_scroll_child(gtk::ScrollType::End, false);
        });

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        self.send_text_changed = false;
        self.open_emoji_chooser = false;
        self.picked_emoji = "".to_string();

        match msg {
            ChatInput::Action(action) => {
                if let ChatAction::ChatEnableChanged { enabled, .. } = action {
                    self.enabled = enabled;
                }
                let mut guard = self.entries.guard();
                guard.push_back(ChatEntryInit {
                    action,
                    theme: self.theme,
                });
            }
            ChatInput::UpdateSendText(text) => {
                self.send_text = text;
            }
            ChatInput::SendMessage => {
                sender
                    .output(Self::Output::SendMessage {
                        scope: self.scope.clone(),
                        content: self.send_text.to_string(),
                    })
                    .unwrap();
                self.send_text.clear();
                self.send_text_changed = true;
            }
            ChatInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
            ChatInput::EntryIconPressed(pos, area) => match pos {
                EntryIconPosition::Primary => {
                    self.emoji_chooser_pointing_to = area;
                    self.open_emoji_chooser = true;
                }
                EntryIconPosition::Secondary => {
                    sender.input(ChatInput::SendMessage);
                }
                _ => {}
            },
            ChatInput::EmojiPicked(value) => {
                self.picked_emoji = value;
            }
        }
    }
}

trait PopoverExt {
    fn open(&self, do_open: bool);
}

impl<T: gtk::traits::PopoverExt> PopoverExt for T {
    fn open(&self, do_open: bool) {
        if do_open {
            self.popup();
        }
    }
}

trait EntryExt {
    fn insert_emoji(&self, emoji: &str);
}

impl EntryExt for gtk::Entry {
    fn insert_emoji(&self, emoji: &str) {
        let mut position = self.position();
        self.insert_text(emoji, &mut position);
    }
}
