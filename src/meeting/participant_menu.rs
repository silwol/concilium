// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt, ButtonExt, OrientableExt, WidgetExt};
use opentalk_types::{core::ParticipantId, signaling::Role};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};

use crate::theme::Theme;

use super::participants::Participants;

pub struct ParticipantMenu {
    participants: Participants,
    participant_id: ParticipantId,
    theme: Theme,
}

pub struct ParticipantMenuInit {
    pub participants: Participants,
    pub participant_id: ParticipantId,
    pub theme: Theme,
}

#[derive(Debug)]
pub enum ParticipantMenuInput {
    SwitchTheme(Theme),
    ParticipantUpdated(ParticipantId),
    GrantModeratorRights,
    RevokeModeratorRights,
}

#[derive(Debug)]
pub enum ParticipantMenuOutput {
    GrantModeratorRights { target: ParticipantId },
    RevokeModeratorRights { target: ParticipantId },
}

#[relm4::component(async, pub)]
impl AsyncComponent for ParticipantMenu {
    type Init = ParticipantMenuInit;
    type Input = ParticipantMenuInput;
    type Output = ParticipantMenuOutput;
    type CommandOutput = ();

    view! {
        #[root]
        #[name = "menu_button"]
        gtk::MenuButton {
            set_popover: Some(&popover),

            add_css_class: "flat",
            set_icon_name: "open-menu-symbolic",

            #[watch]
            set_visible: model.participants.current_participant_id() != model.participant_id,
        },

        #[name = "popover"]
        gtk::Popover {
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 10,

                gtk::Label {
                    add_css_class: "heading",
                    set_xalign: 0f32,

                    #[watch]
                    set_label: match (model.participants.current_participant_is_room_owner(), model.participants.role(model.participant_id)) {
                        (true, _) => "Role: room owner and moderator",
                        (false, Role::Guest) => "Role: guest",
                        (false, Role::User) => "Role: user",
                        (false, Role::Moderator) => "Role: moderator",
                    }
                },

                gtk::Button {
                    #[watch]
                    set_visible:
                        model.participants.current_participant_is_moderator() &&
                        model.participants.current_participant_id() != model.participant_id &&
                        model.participants.role(model.participant_id).is_user(),

                    adw::ButtonContent {
                        set_label: "Grant moderator rights",

                        #[watch]
                        set_icon_name: model.theme.icon_name("moderator", "moderator-dark"),
                    },

                    connect_clicked[sender] => move |_| {
                        sender.input(ParticipantMenuInput::GrantModeratorRights);
                    }
                },

                gtk::Button {
                    #[watch]
                    set_visible:
                        model.participants.current_participant_is_moderator() &&
                        model.participants.current_participant_id() != model.participant_id &&
                        model.participants.is_moderator(model.participant_id) &&
                        !model.participants.is_room_owner(model.participant_id),

                    adw::ButtonContent {
                        set_label: "Revoke moderator rights",

                        #[watch]
                        set_icon_name: model.theme.icon_name("remove-moderator", "remove-moderator-dark"),
                    },

                    connect_clicked[sender] => move |_| {
                        sender.input(ParticipantMenuInput::RevokeModeratorRights);
                    }
                },
            },
        }
    }

    async fn init(
        ParticipantMenuInit {
            participants,
            participant_id,
            theme,
        }: ParticipantMenuInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = ParticipantMenu {
            participants,
            participant_id,
            theme,
        };
        let widgets = view_output!();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        sender: AsyncComponentSender<Self>,
        root: &Self::Root,
    ) {
        match msg {
            ParticipantMenuInput::SwitchTheme(theme) => {
                self.theme = theme;
            }
            ParticipantMenuInput::ParticipantUpdated(_participant_id) => {}
            ParticipantMenuInput::GrantModeratorRights => {
                root.popdown();
                sender
                    .output(ParticipantMenuOutput::GrantModeratorRights {
                        target: self.participant_id,
                    })
                    .unwrap();
            }
            ParticipantMenuInput::RevokeModeratorRights => {
                root.popdown();
                sender
                    .output(ParticipantMenuOutput::RevokeModeratorRights {
                        target: self.participant_id,
                    })
                    .unwrap();
            }
        }
    }
}
