// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Theme {
    Bright,
    Dark,
}

impl Theme {
    pub fn from_is_dark(dark: bool) -> Self {
        if dark {
            Self::Dark
        } else {
            Self::Bright
        }
    }

    pub fn icon_name<'a>(&self, bright: &'a str, dark: &'a str) -> &'a str {
        match self {
            Theme::Bright => bright,
            Theme::Dark => dark,
        }
    }
}
