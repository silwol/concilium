// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use chrono::{DateTime, Local, Utc};
use opentalk_types::{
    core::Timestamp,
    signaling::{
        control::{state::ControlState, Participant},
        Role,
    },
};

pub(crate) trait ParticipantExt {
    fn display_name(&self) -> Option<String>;

    fn display_name_or_default(&self) -> String {
        self.display_name().unwrap_or_else(|| "unknown".to_string())
    }

    fn joined_at(&self) -> Option<Timestamp>;

    fn left_at(&self) -> Option<Timestamp>;

    fn has_left(&self) -> bool;

    fn is_present(&self) -> bool;

    fn is_visible(&self) -> bool {
        self.left_at().is_none()
    }

    fn joined_at_local(&self) -> Option<DateTime<Local>> {
        self.joined_at()
            .map(|j| DateTime::<Local>::from(DateTime::<Utc>::from(j)))
    }

    fn update_left_at(&mut self, left_at: Timestamp);

    fn hand_is_up(&self) -> bool;

    fn is_moderator(&self) -> bool;

    fn is_room_owner(&self) -> bool;

    fn role(&self) -> Role;

    fn modify_control<F>(&mut self, f: F) -> bool
    where
        F: FnOnce(&mut ControlState);

    fn protocol_write_access(&self) -> bool;
}

impl ParticipantExt for Participant {
    fn display_name(&self) -> Option<String> {
        self.participant_data
            .control
            .as_ref()
            .map(|c| c.display_name.to_string())
    }

    fn joined_at(&self) -> Option<Timestamp> {
        self.participant_data.control.as_ref().map(|c| c.joined_at)
    }

    fn left_at(&self) -> Option<Timestamp> {
        self.participant_data
            .control
            .as_ref()
            .and_then(|c| c.left_at)
    }

    fn has_left(&self) -> bool {
        self.left_at().is_some()
    }

    fn is_present(&self) -> bool {
        !self.has_left()
    }

    fn update_left_at(&mut self, left_at: Timestamp) {
        if let Some(ref mut control) = self.participant_data.control {
            control.left_at = Some(left_at);
        }
    }

    fn hand_is_up(&self) -> bool {
        self.participant_data
            .control
            .as_ref()
            .and_then(|c| Some(c.hand_is_up))
            .unwrap_or_default()
    }

    fn is_moderator(&self) -> bool {
        self.role().is_moderator()
    }

    fn is_room_owner(&self) -> bool {
        self.participant_data
            .control
            .as_ref()
            .and_then(|c| Some(c.is_room_owner))
            .unwrap_or_default()
    }

    fn role(&self) -> Role {
        self.participant_data
            .control
            .as_ref()
            .and_then(|c| Some(c.role))
            .unwrap_or(Role::Guest)
    }

    fn modify_control<F>(&mut self, f: F) -> bool
    where
        F: FnOnce(&mut ControlState),
    {
        self.participant_data
            .control
            .as_mut()
            .map(|c| {
                f(c);
                true
            })
            .unwrap_or_default()
    }

    fn protocol_write_access(&self) -> bool {
        self.participant_data
            .protocol
            .as_ref()
            .map(|p| !p.readonly)
            .unwrap_or_default()
    }
}
