// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use anyhow::Result;
use gtk::traits::{
    BoxExt as _, ButtonExt as _, EditableExt as _, EntryExt as _, OrientableExt as _,
    WidgetExt as _,
};
use opentalk_client::{Client, RoomOrInviteId};
use opentalk_types::core::InviteCodeId;
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};
use url::Url;

#[derive(Debug)]
pub struct ConnectForm {
    message: String,
    meeting: Option<(Url, RoomOrInviteId)>,
}

#[derive(Debug)]
pub enum ConnectFormInput {
    SetUrlValid((Url, RoomOrInviteId)),
    SetInviteInvalid(String),
    JoinAsGuest,
    Authenticate,
}

#[derive(Debug)]
pub enum ConnectFormOutput {
    UnauthenticatedJoinRoomByInvite {
        url: Url,
        invite_code: InviteCodeId,
    },
    AuthenticateAndJoinRoom {
        url: Url,
        meeting_id: RoomOrInviteId,
    },
}

#[derive(Debug)]
pub struct ConnectFormInit {
    pub meeting_url: Option<Url>,
}

fn get_meeting_url(url: &str) -> Result<(Url, RoomOrInviteId)> {
    let url = Url::parse(url)?;
    let data = Client::parse_meeting_url(&url)?;
    Ok(data)
}

#[relm4::component(async, pub)]
impl AsyncComponent for ConnectForm {
    type Init = ConnectFormInit;
    type Input = ConnectFormInput;
    type Output = ConnectFormOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 10,
            set_margin_end: 10,
            set_spacing: 10,

            gtk::Label {
                set_label: "Concilium",
                add_css_class: "title-1",
                set_halign: gtk::Align::Start,
            },
            gtk::Box {
                set_vexpand: true,
            },
            gtk::Label {
                set_label: "Connect to meeting by invite link",
                add_css_class: "title-2",
                set_halign: gtk::Align::Start,
            },
            gtk::Entry {
                set_activates_default: true,
                set_placeholder_text: Some("Invite link"),
                set_hexpand: true,
                set_text: invite_url,
                connect_changed[sender] => move |entry| {
                    let text = entry.text();
                    sender.input(
                        if text.is_empty() {
                            ConnectFormInput::SetInviteInvalid("".to_string())
                        } else {
                            match get_meeting_url(&text) {
                                Ok(invite) => ConnectFormInput::SetUrlValid(invite),
                                Err(e) => ConnectFormInput::SetInviteInvalid(format!("{e}")),
                            }
                        }
                    );
                }
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_spacing: 10,
                gtk::Button {
                    set_label: "Join as guest",
                    set_receives_default: true,
                    add_css_class: "suggested-action",
                    #[watch]
                    set_sensitive: matches!(model.meeting, Some((_, RoomOrInviteId::Invite(_)))),
                    connect_clicked => ConnectFormInput::JoinAsGuest,
                },
                gtk::Button {
                    set_label: "Authenticate",
                    set_receives_default: true,
                    add_css_class: "suggested-action",
                    #[watch]
                    set_sensitive: model.meeting.is_some(),
                    connect_clicked => ConnectFormInput::Authenticate,
                },
            },
            gtk::Box {
                gtk::Label {
                    #[watch]
                    set_label: &model.message
                },
            },
            gtk::Box {
                set_vexpand: true,
            },
        }
    }

    async fn init(
        init: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let invite_url: &str = init
            .meeting_url
            .as_ref()
            .map(Url::as_str)
            .unwrap_or_default();
        let meeting = get_meeting_url(invite_url).ok();
        let model = Self {
            message: "".to_string(),
            meeting,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: ConnectFormInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        println!("input: {msg:?}");
        match msg {
            ConnectFormInput::SetUrlValid(meeting) => {
                self.message = match meeting.1 {
                    RoomOrInviteId::Room(_) => "Valid room url",
                    RoomOrInviteId::Invite(_) => "Valid invite url",
                }
                .to_string();
                self.meeting = Some(meeting);
            }
            ConnectFormInput::SetInviteInvalid(msg) => {
                self.meeting = None;
                self.message = msg;
            }
            ConnectFormInput::JoinAsGuest => {
                if let (url, RoomOrInviteId::Invite(invite_code)) = self.meeting.clone().unwrap() {
                    sender
                        .output(ConnectFormOutput::UnauthenticatedJoinRoomByInvite {
                            url,
                            invite_code,
                        })
                        .unwrap();
                }
            }
            ConnectFormInput::Authenticate => {
                let (url, meeting_id) = self.meeting.clone().unwrap();
                sender
                    .output(ConnectFormOutput::AuthenticateAndJoinRoom { url, meeting_id })
                    .unwrap();
            }
        }
    }
}
