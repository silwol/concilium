// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::{cell::RefCell, iter::once};

use gtk::traits::{OrientableExt as _, WidgetExt as _};
use log::warn;
use opentalk_client::{
    SignalingCommand, SignalingEvent, SignalingSink, SignalingStream, SignalingStreamEvent,
};
use opentalk_types::{
    core::{ParticipantId, ParticipationKind, Timestamp},
    signaling::{
        breakout::event::BreakoutEvent,
        chat::{
            command::{ChatCommand, SendMessage},
            event::{ChatDisabled, ChatEnabled, ChatEvent},
            state::StoredMessage,
            Scope,
        },
        common::TargetParticipant,
        control::{
            command::ControlCommand,
            event::{ControlEvent, JoinSuccess},
            state::ControlState,
            Participant,
        },
        media::event::MediaEvent,
        moderation::{command::ModerationCommand, event::ModerationEvent, KickScope},
        polls::event::PollsEvent,
        protocol::{
            command::ProtocolCommand, event::ProtocolEvent,
            participant_state::ProtocolParticipantState,
        },
        timer::{
            self,
            command::{self as timer_command, TimerCommand},
            event::{self as timer_event, TimerEvent},
            TimerId,
        },
        whiteboard::event::WhiteboardEvent,
        ModuleData, ParticipantData,
    },
};
use relm4::{
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender, SharedState,
};

use crate::{
    meeting::{
        chat::{ChatAction, ChatInit},
        header::MeetingHeaderInit,
        multi_overlay_controller::Overlay,
        sidebar::{SidebarInit, SidebarInput, SidebarOutput},
        themed_break::{ThemedBreakInit, ThemedBreakOutput},
        video_grid::{VideoGridInit, VideoGridOutput},
    },
    participant_ext::ParticipantExt,
    theme::Theme,
    timer_ext::TimerStartedExt,
};

use self::{
    header::{MeetingHeader, MeetingHeaderInput, Protocol},
    multi_overlay_controller::{MultiOverlayController, OverlayExt as _},
    participants::Participants,
    sidebar::Sidebar,
    themed_break::ThemedBreak,
    video_grid::{VideoGrid, VideoGridInput},
};

mod chat;
mod controls;
mod group_chat_list;
mod header;
mod multi_overlay_controller;
mod participant_chat_menu;
mod participant_list;
mod participant_menu;
mod participants;
mod sidebar;
mod themed_break;
mod user_view;
mod video_grid;
mod waiting_participants_list;

static TIMER_STATE: SharedState<Option<timer_event::Started>> = SharedState::new();

pub struct Meeting {
    theme: Theme,
    sidebar: AsyncController<Sidebar>,
    header: AsyncController<MeetingHeader>,
    video_grid: AsyncController<VideoGrid>,
    overlay_controller: RefCell<MultiOverlayController>,
    signaling_sink: SignalingSink,
    participants: Participants,
    is_folded: bool,
}

#[derive(Debug)]
pub enum MeetingInput {
    GetNextSignalingEvent(SignalingStream),
    LeaveRoom,
    RaiseHand,
    LowerHand,
    ResetRaisedHands,
    Shutdown(KickScope),
    SendChatMessage {
        content: String,
        scope: Scope,
    },
    SwitchTheme(Theme),
    GrantModeratorRights {
        target: ParticipantId,
    },
    RevokeModeratorRights {
        target: ParticipantId,
    },
    JoinedWithTimer(timer_event::Started),
    ReturnFromThemedBreak,
    HideModeratorThemedBreak,
    ShowThemedBreak,
    Folded(bool),
    StartTimer {
        kind: timer_command::Kind,
        title: Option<String>,
        style: Option<String>,
        enable_ready_check: bool,
    },
    StopTimer,
    AcceptParticipantFromWaitingRoom {
        target: ParticipantId,
    },
    ResetUnreadCount {
        target: ParticipantId,
    },
    UpdateTimerReadyStatus {
        timer_id: TimerId,
        ready: bool,
    },
    ChangeProtocolWritePermissions {
        target: ParticipantId,
        enabled: bool,
    },
}

#[derive(Debug)]
pub enum MeetingCommandOutput {
    EventReceived(SignalingStream, SignalingEvent),
    MeetingClosed(SignalingStream),
    NothingReceived(SignalingStream),
    PingReceived(SignalingStream, Vec<u8>),
}

#[derive(Debug)]
pub enum MeetingOutput {
    LeaveRoom,
    ShowMessage { content: String },
}

#[relm4::component(async, pub)]
impl AsyncComponent for Meeting {
    type Init = (JoinSuccess, SignalingSink, SignalingStream);
    type Input = MeetingInput;
    type Output = MeetingOutput;
    type CommandOutput = MeetingCommandOutput;

    view! {
        #[root]
        gtk::Overlay {
            #[watch]
            apply_from_multi_controller_b_refcell: &model.overlay_controller,

            #[name = "leaflet"]
            relm4::adw::Leaflet {
                set_orientation: gtk::Orientation::Horizontal,
                set_can_navigate_back: true,
                set_can_navigate_forward: true,
                set_homogeneous: false,

                connect_folded_notify[sender] => move |leaflet| {
                    sender.input(MeetingInput::Folded(leaflet.is_folded()))
                },

                model.sidebar.widget(),

                #[name = "meeting_area"]
                gtk::Overlay {
                    #[watch]
                    apply_from_multi_controller_a_refcell: &model.overlay_controller,

                    gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,

                        model.header.widget(),

                        gtk::ScrolledWindow {
                            set_valign: gtk::Align::Fill,
                            set_hexpand: true,
                            set_vexpand: true,

                            model.video_grid.widget(),
                        },
                    },
                },

                set_visible_child: &meeting_area,
            },
        }
    }

    async fn init(
        (
            JoinSuccess {
                id,
                display_name,
                avatar_url,
                role,
                // TODO: handle `closes_at`
                closes_at: _,
                // TODO: handle `tariff`
                tariff: _,
                module_data,
                participants,
                // TODO: handle event_info
                event_info: _,
                is_room_owner,
            },
            signaling_sink,
            signaling_stream,
        ): Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let ModuleData {
            control: _,
            chat,
            // TODO: handle `media`
            media: _,
            moderation,
            // TODO: handle `poll`
            poll: _,
            // TODO: handle `breakout`
            breakout: _,
            timer,
            // TODO: handle `whiteboard`
            whiteboard: _,
            shared_folder,
            // TODO: handle `others`
            others: _,
        } = *module_data;
        let theme = Theme::from_is_dark(relm4::adw::StyleManager::default().is_dark());
        let participants = Participants::new(
            id,
            participants.into_iter().chain(once(Participant {
                id,
                participant_data: ParticipantData {
                    control: Some(ControlState {
                        display_name: display_name.to_string(),
                        role,
                        avatar_url,
                        // TODO: get this from somewhere else…
                        participation_kind: ParticipationKind::Guest,
                        hand_is_up: false,
                        // TODO: get the timestamp from the message
                        joined_at: Timestamp::now(),
                        left_at: None,
                        hand_updated_at: Timestamp::now(),
                        is_room_owner,
                    }),
                    ..Default::default()
                },
            })),
        )
        .unwrap();
        let meeting_start_time = participants.earliest_join();
        let mut room_history = chat
            .as_ref()
            .map(|c| c.room_history.clone())
            .unwrap_or_default();
        room_history.sort_by(
            |StoredMessage {
                 timestamp: timestamp1,
                 ..
             },
             StoredMessage {
                 timestamp: timestamp2,
                 ..
             }| timestamp1.cmp(timestamp2),
        );
        let chat_enabled = chat.as_ref().map(|c| c.enabled).unwrap_or_default();
        let group_chat = chat
            .as_ref()
            .map(|c| c.groups_history.clone())
            .unwrap_or_default();

        let sidebar = Sidebar::builder()
            .launch(SidebarInit {
                participants: participants.clone(),
                room_chat: ChatInit {
                    enabled: chat.as_ref().map(|c| c.enabled).unwrap_or_default(),
                    actions: room_history
                        .into_iter()
                        .map(
                            |StoredMessage {
                                 source, content, ..
                             }| {
                                let display_name = participants.display_name_or_default(source);
                                let is_moderator = participants.is_moderator(source);
                                ChatAction::Message {
                                    participant_id: source,
                                    display_name,
                                    content,
                                    is_moderator,
                                }
                            },
                        )
                        .collect(),
                    theme,
                    emoji_chooser_enabled: true,
                    chat_name: "Room chat".to_string(),
                    scope: Scope::Global,
                },
                group_chat,
                raise_hands_enabled: moderation
                    .as_ref()
                    .map(|m| m.raise_hands_enabled)
                    .unwrap_or_default(),
                theme,
            })
            .forward(sender.input_sender(), |msg| match msg {
                SidebarOutput::LeaveRoom => MeetingInput::LeaveRoom,
                SidebarOutput::RaiseHand => MeetingInput::RaiseHand,
                SidebarOutput::LowerHand => MeetingInput::LowerHand,
                SidebarOutput::ResetRaisedHands => MeetingInput::ResetRaisedHands,
                SidebarOutput::Shutdown(kick_scope) => MeetingInput::Shutdown(kick_scope),
                SidebarOutput::SendChatMessage { content, scope } => {
                    MeetingInput::SendChatMessage { content, scope }
                }
                SidebarOutput::GrantModeratorRights { target } => {
                    MeetingInput::GrantModeratorRights { target }
                }
                SidebarOutput::RevokeModeratorRights { target } => {
                    MeetingInput::RevokeModeratorRights { target }
                }
                SidebarOutput::StartTimer {
                    kind,
                    title,
                    style,
                    enable_ready_check,
                } => MeetingInput::StartTimer {
                    kind,
                    title,
                    style,
                    enable_ready_check,
                },
                SidebarOutput::StopTimer => MeetingInput::StopTimer,
                SidebarOutput::ResetUnreadCount { target } => {
                    MeetingInput::ResetUnreadCount { target }
                }
                SidebarOutput::ChangeProtocolWritePermissions { target, enabled } => {
                    MeetingInput::ChangeProtocolWritePermissions { target, enabled }
                }
            });
        let moderator_data = moderation
            .as_ref()
            .map(|m| m.moderator_data.clone())
            .unwrap_or_default();
        let waiting_participants = moderator_data
            .as_ref()
            .map(|m| m.waiting_room_participants.clone())
            .unwrap_or_default();
        let header = MeetingHeader::builder()
            .launch(MeetingHeaderInit {
                my_id: id,
                meeting_start_time,
                theme,
                waiting_participants,
                shared_folder,
                protocol: None,
            })
            .forward(sender.input_sender(), |msg| match msg {
                header::MeetingHeaderOutput::ShowThemedBreak => MeetingInput::ShowThemedBreak,
                header::MeetingHeaderOutput::AcceptParticipantFromWaitingRoom { target } => {
                    MeetingInput::AcceptParticipantFromWaitingRoom { target }
                }
                header::MeetingHeaderOutput::UpdateTimerReadyStatus { timer_id, ready } => {
                    MeetingInput::UpdateTimerReadyStatus { timer_id, ready }
                }
            });
        let video_grid = VideoGrid::builder()
            .launch(VideoGridInit {
                participants: participants.clone(),
                chat_enabled,
                theme,
            })
            .forward(sender.input_sender(), |msg| match msg {
                VideoGridOutput::GrantModeratorRights { target } => {
                    MeetingInput::GrantModeratorRights { target }
                }
                VideoGridOutput::RevokeModeratorRights { target } => {
                    MeetingInput::RevokeModeratorRights { target }
                }
                VideoGridOutput::SendMessage { scope, content } => {
                    MeetingInput::SendChatMessage { scope, content }
                }
                VideoGridOutput::ResetUnreadCount { target } => {
                    MeetingInput::ResetUnreadCount { target }
                }
            });

        let model = Self {
            theme,
            sidebar,
            header,
            video_grid,
            overlay_controller: RefCell::new(Default::default()),
            signaling_sink,
            participants,
            is_folded: false,
        };
        let widgets = view_output!();

        if let Some(timer) = timer {
            sender.input(MeetingInput::JoinedWithTimer(timer));
        }
        sender.input(MeetingInput::GetNextSignalingEvent(signaling_stream));

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: MeetingInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            MeetingInput::GetNextSignalingEvent(mut room_stream) => {
                sender.oneshot_command(async move {
                    match room_stream.get_next_event().await.unwrap() {
                        None => MeetingCommandOutput::NothingReceived(room_stream),
                        Some(SignalingStreamEvent::Event(event)) => {
                            MeetingCommandOutput::EventReceived(room_stream, event)
                        }
                        Some(SignalingStreamEvent::Ping(data)) => {
                            MeetingCommandOutput::PingReceived(room_stream, data)
                        }
                        Some(SignalingStreamEvent::Close) => {
                            MeetingCommandOutput::MeetingClosed(room_stream)
                        }
                    }
                });
            }
            MeetingInput::LeaveRoom => {
                sender
                    .output_sender()
                    .send(Self::Output::LeaveRoom)
                    .unwrap();
            }
            MeetingInput::RaiseHand => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(ControlCommand::RaiseHand)
                    .await
                    .unwrap();
            }
            MeetingInput::LowerHand => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(ControlCommand::LowerHand)
                    .await
                    .unwrap();
            }
            MeetingInput::SendChatMessage { content, scope } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Chat(ChatCommand::SendMessage(
                        SendMessage { content, scope },
                    )))
                    .await
                    .unwrap();
            }
            MeetingInput::ResetRaisedHands => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Moderation(
                        ModerationCommand::ResetRaisedHands,
                    ))
                    .await
                    .unwrap();
            }
            MeetingInput::Shutdown(kick_scope) => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Moderation(ModerationCommand::Debrief(
                        kick_scope,
                    )))
                    .await
                    .unwrap();
            }
            MeetingInput::SwitchTheme(theme) => {
                self.theme = theme;
                self.header.emit(MeetingHeaderInput::SwitchTheme(theme));
                self.sidebar.emit(SidebarInput::SwitchTheme(theme));
                self.video_grid.emit(VideoGridInput::SwitchTheme(theme));
                self.overlay_controller.borrow_mut().update_theme(theme);
            }
            MeetingInput::GrantModeratorRights { target } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Control(
                        ControlCommand::GrantModeratorRole(TargetParticipant { target }),
                    ))
                    .await
                    .unwrap();
            }
            MeetingInput::RevokeModeratorRights { target } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Control(
                        ControlCommand::RevokeModeratorRole(TargetParticipant { target }),
                    ))
                    .await
                    .unwrap();
            }
            MeetingInput::JoinedWithTimer(started) => {
                println!("Joined with timer event present {started:#?}");

                self.handle_timer_event(TimerEvent::Started(started), &sender)
                    .await;
            }
            MeetingInput::ReturnFromThemedBreak => {
                let mut controller = self.overlay_controller.borrow_mut();
                if self.participants.current_participant_is_moderator() {
                    controller.set_next_a(None);
                } else {
                    controller.set_next_b(None);
                }
            }
            MeetingInput::HideModeratorThemedBreak => {
                let mut controller = self.overlay_controller.borrow_mut();
                if self.participants.current_participant_is_moderator() {
                    controller.set_next_a(None);
                }
            }
            MeetingInput::ShowThemedBreak => {
                self.show_themed_break(&sender).await;
            }
            MeetingInput::Folded(is_folded) => {
                self.is_folded = is_folded;
            }
            MeetingInput::StartTimer {
                kind,
                style,
                title,
                enable_ready_check,
            } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Timer(TimerCommand::Start(
                        timer_command::Start {
                            kind,
                            style,
                            title,
                            enable_ready_check,
                        },
                    )))
                    .await
                    .unwrap();
            }
            MeetingInput::StopTimer => {
                if let Some(timer) = &*TIMER_STATE.read() {
                    // TODO: this should probably be handled in a relm command
                    self.signaling_sink
                        .send_command(SignalingCommand::Timer(TimerCommand::Stop(
                            timer_command::Stop {
                                timer_id: timer.timer_id,
                                reason: None,
                            },
                        )))
                        .await
                        .unwrap();
                }
            }
            MeetingInput::AcceptParticipantFromWaitingRoom { target } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Moderation(ModerationCommand::Accept {
                        target,
                    }))
                    .await
                    .unwrap();
            }
            MeetingInput::ResetUnreadCount { target } => {
                self.sidebar
                    .emit(SidebarInput::UnreadCountWasReset { target });
                self.video_grid
                    .emit(VideoGridInput::UnreadCountWasReset { target });
            }
            MeetingInput::UpdateTimerReadyStatus { timer_id, ready } => {
                // TODO: this should probably be handled in a relm command
                self.signaling_sink
                    .send_command(SignalingCommand::Timer(TimerCommand::UpdateReadyStatus(
                        timer_command::UpdateReadyStatus {
                            timer_id,
                            status: ready,
                        },
                    )))
                    .await
                    .unwrap();
            }
            MeetingInput::ChangeProtocolWritePermissions { target, enabled } => {
                // TODO: this should probably be handled in a relm command
                let participant_selection =
                    opentalk_types::signaling::protocol::command::ParticipantSelection {
                        participant_ids: vec![target],
                    };
                let command = if enabled {
                    ProtocolCommand::SelectWriter(participant_selection)
                } else {
                    ProtocolCommand::DeselectWriter(participant_selection)
                };
                self.signaling_sink
                    .send_command(SignalingCommand::Protocol(command))
                    .await
                    .unwrap();
            }
        }
    }

    async fn update_cmd(
        &mut self,
        message: MeetingCommandOutput,
        sender: AsyncComponentSender<Self>,
        _: &Self::Root,
    ) {
        match message {
            MeetingCommandOutput::EventReceived(room_stream, event) => {
                println!("Received event {event:#?}");

                self.handle_signaling_event(event, &sender).await;

                sender.input(MeetingInput::GetNextSignalingEvent(room_stream));
            }
            MeetingCommandOutput::MeetingClosed(_room_stream) => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "Meeting was closed".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            MeetingCommandOutput::NothingReceived(room_stream) => {
                sender.input(MeetingInput::GetNextSignalingEvent(room_stream));
            }
            MeetingCommandOutput::PingReceived(room_stream, data) => {
                self.signaling_sink.send_pong(data).await.unwrap();
                sender.input(MeetingInput::GetNextSignalingEvent(room_stream));
            }
        }
    }
}

impl Meeting {
    async fn handle_signaling_event(
        &mut self,
        event: SignalingEvent,
        sender: &AsyncComponentSender<Self>,
    ) {
        match event {
            SignalingEvent::Control(event) => self.handle_control_event(event, sender).await,
            SignalingEvent::Timer(event) => self.handle_timer_event(event, sender).await,
            SignalingEvent::Whiteboard(event) => self.handle_whiteboard_event(event, sender).await,
            SignalingEvent::Protocol(event) => self.handle_protocol_event(event, sender).await,
            SignalingEvent::Polls(event) => self.handle_polls_event(event, sender).await,
            SignalingEvent::Media(event) => self.handle_media_event(event, sender).await,
            SignalingEvent::Moderation(event) => self.handle_moderation_event(event, sender).await,
            SignalingEvent::Breakout(event) => self.handle_breakout_event(event, sender).await,
            SignalingEvent::Chat(event) => self.handle_chat_event(event, sender).await,
        }
    }

    async fn handle_control_event(
        &mut self,
        event: ControlEvent,
        sender: &AsyncComponentSender<Self>,
    ) {
        match event {
            ControlEvent::JoinSuccess(_join_success) => {
                // TODO
                println!("Not yet handling control join_success events");
            }
            ControlEvent::JoinBlocked(_join_blocked) => {
                // TODO
                println!("Not yet handling control join_blocked events");
            }
            ControlEvent::Update(participant) => {
                self.participants.set(participant.clone());
                self.sidebar
                    .emit(SidebarInput::ParticipantUpdated(participant.id));
                self.video_grid
                    .emit(VideoGridInput::ParticipantUpdated(participant.id));
            }
            ControlEvent::Joined(participant) => {
                self.participants.set(participant.clone());
                self.sidebar
                    .emit(SidebarInput::ParticipantJoined(participant.id));
                self.video_grid
                    .emit(VideoGridInput::ParticipantJoined(participant.id));
            }
            ControlEvent::Left(associated_participant) => {
                self.participants
                    .update_with(associated_participant.id, |p| {
                        // TODO: get the timestamp from the message envelope
                        p.update_left_at(Timestamp::now());
                    });
                self.sidebar
                    .emit(SidebarInput::ParticipantLeft(associated_participant.id));
                self.video_grid
                    .emit(VideoGridInput::ParticipantLeft(associated_participant.id));
            }
            ControlEvent::TimeLimitQuotaElapsed => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "Time limit quota elapsed".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            ControlEvent::RoleUpdated { new_role } => {
                let current_participant_was_moderator =
                    self.participants.current_participant_is_moderator();
                let current_participant_is_moderator = new_role.is_moderator();

                if current_participant_is_moderator != current_participant_was_moderator {
                    let mut controller = self.overlay_controller.borrow_mut();
                    controller.start_swap();
                }
                let current_participant_id = self.participants.current_participant_id();
                self.participants.update_with(current_participant_id, |p| {
                    p.modify_control(|c| {
                        c.role = new_role;
                    });
                });
                self.sidebar
                    .emit(SidebarInput::ParticipantUpdated(current_participant_id));
                self.video_grid
                    .emit(VideoGridInput::ParticipantUpdated(current_participant_id));
            }
            ControlEvent::RoomDeleted => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "The meeting room has been deleted".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            ControlEvent::Error(error) => {
                use opentalk_types::signaling::control::event::Error;
                let content = match error {
                    Error::InvalidJson => "Invalid JSON received by server".to_string(),
                    Error::InvalidNamespace => {
                        "Invalid namespace in message received by server".to_string()
                    }
                    Error::InvalidUsername => "Invalid username".to_string(),
                    Error::AlreadyJoined => "Already joined".to_string(),
                    Error::NotYetJoined => "Not yet joined".to_string(),
                    Error::NotAcceptedOrNotInWaitingRoom => {
                        "Not accepted or not in waiting room".to_string()
                    }
                    Error::RaiseHandsDisabled => "Raising hands is disabled".to_string(),
                    Error::InsufficientPermissions => "Insufficient permissions".to_string(),
                    Error::TargetIsRoomOwner => "Target is room owner".to_string(),
                    Error::NothingToDo => "Nothing to do".to_string(),
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
        }
    }

    async fn handle_timer_event(&mut self, event: TimerEvent, sender: &AsyncComponentSender<Self>) {
        use opentalk_types::signaling::timer::event::{Error, Stopped, UpdatedReadyStatus};
        match event {
            TimerEvent::Started(started) => {
                let timer_id = started.timer_id;
                let is_themed_break = started.is_themed_break();

                {
                    let mut timer = TIMER_STATE.write();
                    let removed = timer.replace(started);

                    if let Some(removed) = removed {
                        warn!(
                            "New timer with id {} started while other timer with id {} was running",
                            timer_id, removed.timer_id
                        );
                    }
                }

                if is_themed_break {
                    self.show_themed_break(sender).await;
                }
            }
            TimerEvent::Stopped(Stopped {
                timer_id,
                kind,
                reason: _,
            }) => {
                let mut timer = TIMER_STATE.write();

                let removed = timer.take();
                if let Some(ref removed) = removed {
                    if removed.timer_id != timer_id {
                        warn!(
                        "Received timer stop event for id {} while timer with id {} was running",
                        timer_id, removed.timer_id
                    );
                    }
                }

                match removed {
                    Some(timer) if timer.is_themed_break() => {
                        sender
                            .output(MeetingOutput::ShowMessage {
                                content: "Break is over".to_string(),
                            })
                            .unwrap();
                        if self.participants.current_participant_is_moderator() {
                            let mut controller = self.overlay_controller.borrow_mut();
                            controller.set_next_a(None);
                        }
                    }
                    Some(timer) if timer.is_normal_timer() => {
                        let message = if kind == timer::StopKind::Expired {
                            "Time is up"
                        } else {
                            "Timer stopped by moderator"
                        };
                        sender
                            .output(MeetingOutput::ShowMessage {
                                content: message.to_string(),
                            })
                            .unwrap();
                    }
                    Some(_) | None => {}
                }

                // TODO: handle non-themed-breaks properly
            }
            TimerEvent::UpdatedReadyStatus(UpdatedReadyStatus {
                timer_id,
                participant_id,
                status,
            }) => {
                self.header
                    .emit(MeetingHeaderInput::TimerReadyStatusUpdated {
                        timer_id,
                        participant_id,
                        ready: status,
                    });
                self.sidebar.emit(SidebarInput::TimerReadyStatusUpdated {
                    timer_id,
                    participant_id,
                    ready: status,
                });
            }
            TimerEvent::Error(error) => {
                let content = match error {
                    Error::InvalidDuration => "Invalid timer duration".to_string(),
                    Error::InsufficientPermissions => "Insufficient permissions".to_string(),
                    Error::TimerAlreadyRunning => "Timer already running".to_string(),
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
        }
    }

    async fn show_themed_break(&mut self, sender: &AsyncComponentSender<Meeting>) {
        let themed_break = ThemedBreak::builder()
            .launch(ThemedBreakInit { theme: self.theme })
            .forward(sender.input_sender(), |msg| match msg {
                ThemedBreakOutput::ReturnToConference => MeetingInput::ReturnFromThemedBreak,
                ThemedBreakOutput::TimerFinished => MeetingInput::HideModeratorThemedBreak,
            });

        let timer_id = if let Some(started) = &*TIMER_STATE.read() {
            started.timer_id
        } else {
            return;
        };

        let next_overlay = Some(Overlay::ThemedBreak(timer_id, themed_break));
        let mut controller = self.overlay_controller.borrow_mut();
        if self.participants.current_participant_is_moderator() {
            controller.set_next_a(next_overlay);
        } else {
            controller.set_next_b(next_overlay);
        }
    }

    async fn handle_whiteboard_event(
        &mut self,
        _event: WhiteboardEvent,
        _sender: &AsyncComponentSender<Self>,
    ) {
        // TODO
        println!("Not yet handling whiteboard events");
    }

    async fn handle_protocol_event(
        &mut self,
        event: ProtocolEvent,
        sender: &AsyncComponentSender<Self>,
    ) {
        match event {
            ProtocolEvent::WriteUrl(access_url) => {
                self.participants.update_current_with(|p| {
                    p.participant_data.protocol = Some(ProtocolParticipantState { readonly: false })
                });
                self.sidebar.emit(SidebarInput::ParticipantUpdated(
                    self.participants.current_participant_id(),
                ));
                self.video_grid.emit(VideoGridInput::ParticipantUpdated(
                    self.participants.current_participant_id(),
                ));
                self.header
                    .emit(MeetingHeaderInput::UpdateProtocol(Protocol::Write(
                        access_url.url,
                    )));
                let content =
                    "The protocol was made available to you with write access".to_string();
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
            ProtocolEvent::ReadUrl(access_url) => {
                self.participants.update_current_with(|p| {
                    p.participant_data.protocol = Some(ProtocolParticipantState { readonly: true })
                });
                self.sidebar.emit(SidebarInput::ParticipantUpdated(
                    self.participants.current_participant_id(),
                ));
                self.video_grid.emit(VideoGridInput::ParticipantUpdated(
                    self.participants.current_participant_id(),
                ));
                self.header
                    .emit(MeetingHeaderInput::UpdateProtocol(Protocol::Read(
                        access_url.url,
                    )));
                let content = "The protocol was made available to you with read access".to_string();
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
            ProtocolEvent::PdfAsset(pdf_asset) => {
                println!("Not yet handled: Received protocol pdf asset: {pdf_asset:?}");
            }
            ProtocolEvent::Error(error) => {
                use opentalk_types::signaling::protocol::event::Error;
                let content = match error {
                    Error::InsufficientPermissions => "Insufficient permissions".to_string(),
                    Error::InvalidParticipantSelection => {
                        "Invalid participant selection".to_string()
                    }
                    Error::CurrentlyInitializing => {
                        "Protocol is currently initializing".to_string()
                    }
                    Error::FailedInitialization => "Prototocol initialization failed".to_string(),
                    Error::NotInitialized => "Protocol is not yet initialized".to_string(),
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
        }
    }

    async fn handle_polls_event(
        &mut self,
        _event: PollsEvent,
        _sender: &AsyncComponentSender<Self>,
    ) {
        // TODO
        println!("Not yet handling polls events");
    }

    async fn handle_media_event(
        &mut self,
        _event: MediaEvent,
        _sender: &AsyncComponentSender<Self>,
    ) {
        // TODO
        println!("Not yet handling media events");
    }

    async fn handle_moderation_event(
        &mut self,
        event: ModerationEvent,
        sender: &AsyncComponentSender<Self>,
    ) {
        match event {
            ModerationEvent::Kicked => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "You have been kicked from the meeting".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            ModerationEvent::Banned => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "You have been banned from the meeting".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            ModerationEvent::SessionEnded { issued_by: _ } => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "The meeting session has ended".to_string(),
                    })
                    .unwrap();
                sender.output(MeetingOutput::LeaveRoom).unwrap();
            }
            ModerationEvent::DebriefingStarted { issued_by: _ } => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "Debriefing has started".to_string(),
                    })
                    .unwrap();
            }
            ModerationEvent::InWaitingRoom => {
                println!("Not yet handling moderation in_waiting_room event");
                // TODO
            }
            ModerationEvent::JoinedWaitingRoom(participant) => {
                self.header
                    .emit(MeetingHeaderInput::ParticipantJoinedWaitingRoom(
                        *participant,
                    ));
            }
            ModerationEvent::LeftWaitingRoom(associated_participant) => {
                self.header
                    .emit(MeetingHeaderInput::ParticipantLeftWaitingRoom(
                        associated_participant,
                    ));
            }
            ModerationEvent::WaitingRoomEnabled => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "Waiting room is enabled".to_string(),
                    })
                    .unwrap();
            }
            ModerationEvent::WaitingRoomDisabled => {
                sender
                    .output(MeetingOutput::ShowMessage {
                        content: "Waiting room is disabled".to_string(),
                    })
                    .unwrap();
            }
            ModerationEvent::RaiseHandsEnabled { issued_by } => {
                let issued_by = self
                    .participants
                    .get(issued_by)
                    .and_then(|p| p.display_name());
                let content = if let Some(display_name) = issued_by {
                    format!("Handraises were enabled by moderator {display_name}")
                } else {
                    "Handraieses were enabled by moderator".to_string()
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
                self.sidebar
                    .emit(SidebarInput::RaiseHandsEnableChanged { enabled: true });
            }
            ModerationEvent::RaiseHandsDisabled { issued_by } => {
                let issued_by = self
                    .participants
                    .get(issued_by)
                    .and_then(|p| p.display_name());
                let content = if let Some(display_name) = issued_by {
                    format!("Handraises were disabled by moderator {display_name}")
                } else {
                    "Handraieses were disabled by moderator".to_string()
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
                self.sidebar
                    .emit(SidebarInput::RaiseHandsEnableChanged { enabled: false });
            }
            ModerationEvent::Accepted => {
                println!("Not yet handling moderation accepted event");
                // TODO
            }
            ModerationEvent::Error(error) => {
                use opentalk_types::signaling::moderation::event::Error;
                let content = match error {
                    Error::CannotBanGuest => "Cannot ban guests".to_string(),
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
            ModerationEvent::RaisedHandResetByModerator { issued_by } => {
                let display_name = self.participants.display_name_or_default(issued_by);
                let content = format!("All raised hands were reset by moderator {display_name}");
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
                self.sidebar.emit(SidebarInput::RaisedHandsReset);
            }
        }
    }

    async fn handle_breakout_event(
        &mut self,
        _event: BreakoutEvent,
        _sender: &AsyncComponentSender<Self>,
    ) {
        // TODO
        println!("Not yet handling breakout events");
    }

    async fn handle_chat_event(&mut self, event: ChatEvent, sender: &AsyncComponentSender<Self>) {
        match event {
            ChatEvent::ChatEnabled(ChatEnabled { issued_by }) => {
                let display_name = self.participants.display_name_or_default(issued_by);
                let content = format!("Chat was enabled by moderator {display_name}");
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
                self.sidebar.emit(SidebarInput::ChatEnableChanged {
                    issued_by: Some(issued_by),
                    enabled: true,
                });
                self.video_grid.emit(VideoGridInput::ChatEnableChanged {
                    issued_by: Some(issued_by),
                    enabled: true,
                })
            }
            ChatEvent::ChatDisabled(ChatDisabled { issued_by }) => {
                let display_name = self.participants.display_name_or_default(issued_by);

                let content = format!("Chat was disabled by moderator {display_name}");
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
                self.sidebar.emit(SidebarInput::ChatEnableChanged {
                    issued_by: Some(issued_by),
                    enabled: false,
                });
                self.video_grid.emit(VideoGridInput::ChatEnableChanged {
                    issued_by: Some(issued_by),
                    enabled: false,
                })
            }
            ChatEvent::MessageSent(message) => {
                self.sidebar.emit(SidebarInput::ChatMessage {
                    message: message.clone(),
                });
                self.video_grid
                    .emit(VideoGridInput::ChatMessageSent { message });
            }
            ChatEvent::HistoryCleared(_) => {
                // TODO
                println!("Not yet handling chat history cleared event");
            }
            ChatEvent::Error(error) => {
                use opentalk_types::signaling::chat::event::Error;
                let content = match error {
                    Error::ChatDisabled => "Chat is disabled".to_string(),
                    Error::InsufficientPermissions => "Insufficient permissions".to_string(),
                };
                sender
                    .output(MeetingOutput::ShowMessage { content })
                    .unwrap();
            }
        }
    }
}
