// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::{prelude::Cast as _, traits::GtkWindowExt as _};
use opentalk_client::{
    Client, RoomOrInviteId, SignalingEvent, SignalingSink, SignalingStream, SignalingStreamEvent,
};
use opentalk_types::{
    api::v1::{invites::PostVerifyResponse, rooms::PostStartResponse},
    core::{InviteCodeId, RoomId},
    signaling::{
        control::{
            command::{ControlCommand, Join},
            event::{ControlEvent, JoinBlockedReason, JoinSuccess},
        },
        moderation::event::ModerationEvent,
    },
};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentController, AsyncComponentParts, AsyncController},
    gtk, AsyncComponentSender,
};
use strum::AsRefStr;
use url::Url;

use crate::{
    authentication::{
        Authentication, AuthenticationInit, AuthenticationInput, AuthenticationOutput,
    },
    cli::{Cli, Commands},
    connect_form::{ConnectForm, ConnectFormInit, ConnectFormOutput},
    error_message::{ErrorMessage, ErrorMessageInput, ErrorMessageOutput},
    join_form::{JoinForm, JoinFormInit, JoinFormInput},
    meeting::{Meeting, MeetingInput, MeetingOutput},
    progress_indicator::ProgressIndicator,
    theme::Theme,
    waiting_room::{WaitingRoom, WaitingRoomInit, WaitingRoomInput, WaitingRoomOutput},
};

#[derive(AsRefStr)]
enum Child {
    ConnectForm,
    JoinForm,
    ConnectingProgress,
    Authentication,
    WaitingRoom,
    Meeting,
    ConnectErrorMessage,
}

pub struct App {
    visible_child: Child,
    connect_form: AsyncController<ConnectForm>,
    authentication: AsyncController<Authentication>,
    join_form: AsyncController<JoinForm>,
    connecting_progress_indicator: AsyncController<ProgressIndicator>,
    waiting_room: AsyncController<WaitingRoom>,
    meeting: Option<AsyncController<Meeting>>,
    connect_error_message: AsyncController<ErrorMessage>,
    meeting_updated: bool,
    toast: Option<String>,
}

#[derive(Debug)]
pub enum AppInput {
    UnauthenticatedJoinRoomByInvite {
        url: Url,
        invite_code: InviteCodeId,
    },
    AuthenticateAndJoinRoom {
        url: Url,
        meeting_id: RoomOrInviteId,
    },
    PrepareAuthenticatedConnect {
        meeting_id: RoomOrInviteId,
        user_info: openid::Userinfo,
        client: Client,
    },
    ShowConnectInvite,
    LeaveRoom,
    ShowMessage {
        content: String,
    },
    SwitchTheme(Theme),
    Join {
        client: Client,
        display_name: String,
        password: Option<String>,
        room_id: RoomId,
        invite_code: Option<InviteCodeId>,
    },
    WaitingRoomReturned,
    WaitingRoomDisconnected,
    WaitingRoomAccepted {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
    },
}

#[derive(Debug)]
pub enum AppCommandOutput {
    ReadyToStart {
        client: Client,
        password_required: bool,
        display_name: String,
        room_id: RoomId,
        invite_code: Option<InviteCodeId>,
    },
    ConnectionError(String),
    Joined(JoinSuccess, SignalingSink, SignalingStream),
    InWaitingRoom {
        signaling_sink: SignalingSink,
        signaling_stream: SignalingStream,
    },
}

#[relm4::component(async, pub)]
impl AsyncComponent for App {
    type Init = Cli;
    type Input = AppInput;
    type Output = ();
    type CommandOutput = AppCommandOutput;

    view! {
        #[root]
        gtk::ApplicationWindow {
            set_title: Some("Concilium"),
            set_default_width: 500,
            set_default_height: 400,

            adw::ToastOverlay {
                #[track = "model.toast.is_some()"]
                add_toast_if_some: &model.toast,

                #[name = "stack"]
                gtk::Stack {
                    add_named: (
                        model.connect_form.widget(),
                        Some(Child::ConnectForm.as_ref())
                    ),

                    add_named: (
                        model.authentication.widget(),
                        Some(Child::Authentication.as_ref())
                    ),

                    add_named: (
                        model.join_form.widget(),
                        Some(Child::JoinForm.as_ref()),
                    ),

                    add_named: (
                        model.connecting_progress_indicator.widget(),
                        Some(Child::ConnectingProgress.as_ref())
                    ),

                    add_named: (
                        model.waiting_room.widget(),
                        Some(Child::WaitingRoom.as_ref())
                    ),

                    add_named: (
                        model.connect_error_message.widget(),
                        Some(Child::ConnectErrorMessage.as_ref())
                    ),

                    #[track = "model.meeting_updated"]
                    set_meeting_widget: model.meeting.as_ref().map(|m|m.widget().upcast_ref()),

                    #[watch]
                    set_visible_child_name: model.visible_child.as_ref(),
                    set_transition_type: gtk::StackTransitionType::Crossfade,
                },
            },
        }
    }

    async fn init(
        init: Cli,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        {
            let sender = sender.clone();
            relm4::adw::StyleManager::default().connect_dark_notify(move |style_manager| {
                sender.input(AppInput::SwitchTheme(Theme::from_is_dark(
                    style_manager.is_dark(),
                )));
            });
        }
        let meeting_url = match init.command {
            Some(Commands::Join { url }) => url,
            None => None,
        };
        let connect_form = ConnectForm::builder()
            .launch(ConnectFormInit { meeting_url })
            .forward(sender.input_sender(), |v| match v {
                ConnectFormOutput::UnauthenticatedJoinRoomByInvite { url, invite_code } => {
                    AppInput::UnauthenticatedJoinRoomByInvite { url, invite_code }
                }
                ConnectFormOutput::AuthenticateAndJoinRoom { url, meeting_id } => {
                    AppInput::AuthenticateAndJoinRoom { url, meeting_id }
                }
            });
        let authentication = Authentication::builder()
            .launch(AuthenticationInit {})
            .forward(sender.input_sender(), |v| match v {
                AuthenticationOutput::Aborted => AppInput::ShowConnectInvite,
                AuthenticationOutput::Authenticated {
                    meeting_id,
                    user_info,
                    client,
                } => AppInput::PrepareAuthenticatedConnect {
                    meeting_id: meeting_id.unwrap(),
                    user_info,
                    client,
                },
            });
        let join_form = JoinForm::builder().launch(JoinFormInit {}).forward(
            sender.input_sender(),
            |v| match v {
                crate::join_form::JoinFormOutput::Join {
                    client,
                    display_name,
                    password,
                    room_id,
                    invite_code,
                } => AppInput::Join {
                    client,
                    display_name,
                    password,
                    room_id,
                    invite_code,
                },
            },
        );
        let connecting_progress_indicator = ProgressIndicator::builder().launch(()).detach();
        let waiting_room = WaitingRoom::builder()
            .launch(WaitingRoomInit {
                theme: Theme::from_is_dark(relm4::adw::StyleManager::default().is_dark()),
            })
            .forward(sender.input_sender(), |v| match v {
                WaitingRoomOutput::Accepted {
                    signaling_sink,
                    signaling_stream,
                } => AppInput::WaitingRoomAccepted {
                    signaling_sink,
                    signaling_stream,
                },
                WaitingRoomOutput::Disconnected => AppInput::WaitingRoomDisconnected,
                WaitingRoomOutput::Return => AppInput::WaitingRoomReturned,
            });
        let connect_error_message =
            ErrorMessage::builder()
                .launch(())
                .forward(sender.input_sender(), |v| match v {
                    ErrorMessageOutput::Confirmed => AppInput::ShowConnectInvite,
                });
        let model = Self {
            visible_child: Child::ConnectForm,
            connect_form,
            authentication,
            join_form,
            connecting_progress_indicator,
            waiting_room,
            meeting: None,
            meeting_updated: false,
            toast: None,
            connect_error_message,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: AppInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        self.toast = None;
        match msg {
            AppInput::UnauthenticatedJoinRoomByInvite { url, invite_code } => {
                self.visible_child = Child::ConnectingProgress;
                sender.oneshot_command(async move {
                    let client = match Client::connect_unauthenticated(url.as_str()).await {
                        Ok(client) => client,
                        Err(e) => {
                            return AppCommandOutput::ConnectionError(format!("{e:#?}"));
                        }
                    };

                    let PostVerifyResponse {
                        room_id,
                        password_required,
                    } = match client.verify_invite_code(invite_code).await {
                        Ok(response) => response,
                        Err(e) => {
                            return AppCommandOutput::ConnectionError(format!("{e:#?}"));
                        }
                    };

                    AppCommandOutput::ReadyToStart {
                        client,
                        password_required,
                        display_name: "".to_string(),
                        room_id,
                        invite_code: Some(invite_code),
                    }
                });
            }
            AppInput::AuthenticateAndJoinRoom { url, meeting_id } => {
                self.visible_child = Child::Authentication;
                self.authentication
                    .emit(AuthenticationInput::StartAuthentication {
                        url,
                        meeting_id: Some(meeting_id),
                    });
            }
            AppInput::PrepareAuthenticatedConnect {
                meeting_id,
                user_info,
                client,
            } => {
                self.visible_child = Child::ConnectingProgress;
                sender.oneshot_command(async move {
                    let room_id = match meeting_id {
                        RoomOrInviteId::Room(room_id) => room_id,
                        RoomOrInviteId::Invite(invite_code) => {
                            let PostVerifyResponse {
                                room_id,
                                password_required: _,
                            } = match client.verify_invite_code(invite_code).await {
                                Ok(response) => response,
                                Err(e) => {
                                    return AppCommandOutput::ConnectionError(format!("{e:#?}"));
                                }
                            };
                            room_id
                        }
                    };

                    AppCommandOutput::ReadyToStart {
                        client,
                        // authenticated users can always join without password
                        password_required: false,
                        display_name: user_info.name.unwrap_or_default(),
                        room_id,
                        // not passing the invite code, we want to call the `start` endpoint, not `start_invited`
                        invite_code: None,
                    }
                });
            }
            AppInput::ShowConnectInvite => self.visible_child = Child::ConnectForm,
            AppInput::LeaveRoom => {
                self.visible_child = Child::ConnectForm;
                self.meeting.take();
                self.meeting_updated = true;
            }
            AppInput::ShowMessage { content } => {
                self.toast = Some(content);
            }
            AppInput::SwitchTheme(theme) => {
                if let Some(ref meeting) = self.meeting {
                    meeting.emit(MeetingInput::SwitchTheme(theme));
                }
                self.waiting_room.emit(WaitingRoomInput::SwitchTheme(theme));
            }
            AppInput::Join {
                client,
                display_name,
                password,
                room_id,
                invite_code,
            } => {
                self.visible_child = Child::ConnectingProgress;
                sender.oneshot_command(async move {
                    let response = if let Some(invite_code) = invite_code {
                        client.start_invited(invite_code, room_id, password).await
                    } else {
                        let breakout_room = None;
                        let resumption = None;
                        client.start(room_id, breakout_room, resumption).await
                    };

                    let PostStartResponse {
                        ticket,
                        resumption: _,
                    } = match response {
                        Ok(response) => response,
                        Err(e) => {
                            return AppCommandOutput::ConnectionError(format!("{e:#?}"));
                        }
                    };

                    let (mut signaling_sink, signaling_stream) =
                        match client.connect_to_signaling(ticket).await {
                            Ok(s) => s,
                            Err(e) => {
                                return AppCommandOutput::ConnectionError(format!("{e:#?}"));
                            }
                        };

                    signaling_sink
                        .send_command(ControlCommand::Join(Join { display_name }))
                        .await
                        .unwrap();
                    wait_for_join_answer(signaling_sink, signaling_stream).await
                });
            }
            AppInput::WaitingRoomReturned => {
                self.visible_child = Child::ConnectForm;
            }
            AppInput::WaitingRoomDisconnected => {
                self.visible_child = Child::ConnectForm;
                self.toast = Some("You were disconnected from the waiting room".to_string());
            }
            AppInput::WaitingRoomAccepted {
                mut signaling_sink,
                signaling_stream,
            } => {
                self.visible_child = Child::ConnectingProgress;
                sender.oneshot_command(async move {
                    signaling_sink
                        .send_command(ControlCommand::EnterRoom)
                        .await
                        .unwrap();
                    wait_for_join_answer(signaling_sink, signaling_stream).await
                });
            }
        }
    }

    async fn update_cmd(
        &mut self,
        message: AppCommandOutput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match message {
            AppCommandOutput::ReadyToStart {
                client,
                password_required,
                display_name,
                room_id,
                invite_code,
            } => {
                self.visible_child = Child::JoinForm;
                self.join_form.emit(JoinFormInput::ReadyToStart {
                    client,
                    password_required,
                    display_name,
                    room_id,
                    invite_code,
                });
            }
            AppCommandOutput::ConnectionError(msg) => {
                println!("ERROR CONNECTING: {}", msg);
                self.connect_error_message
                    .emit(ErrorMessageInput::UpdateMessage(msg));
                self.visible_child = Child::ConnectErrorMessage;
            }
            AppCommandOutput::Joined(join_success, signaling_sink, signaling_stream) => {
                let meeting = Meeting::builder()
                    .launch((join_success, signaling_sink, signaling_stream))
                    .forward(sender.input_sender(), |v| match v {
                        MeetingOutput::LeaveRoom => AppInput::LeaveRoom,
                        MeetingOutput::ShowMessage { content } => AppInput::ShowMessage { content },
                    });
                let _ = self.meeting.insert(meeting);
                self.meeting_updated = true;
                self.visible_child = Child::Meeting;
            }
            AppCommandOutput::InWaitingRoom {
                signaling_sink,
                signaling_stream,
            } => {
                self.waiting_room.emit(WaitingRoomInput::Enter {
                    signaling_sink,
                    signaling_stream,
                });
                self.visible_child = Child::WaitingRoom;
            }
        }
    }
}

async fn wait_for_join_answer(
    mut signaling_sink: SignalingSink,
    mut signaling_stream: SignalingStream,
) -> AppCommandOutput {
    loop {
        let answer = signaling_stream.get_next_event().await;
        match answer {
            Ok(Some(SignalingStreamEvent::Event(event))) => match event {
                SignalingEvent::Control(ControlEvent::JoinSuccess(join_success)) => {
                    return AppCommandOutput::Joined(
                        join_success,
                        signaling_sink,
                        signaling_stream,
                    );
                }
                SignalingEvent::Control(ControlEvent::JoinBlocked(reason)) => {
                    let reason = match reason {
                        JoinBlockedReason::ParticipantLimitReached => "Participant limit reached",
                    };
                    return AppCommandOutput::ConnectionError(format!(
                        "Joining the room was blocked: {reason}"
                    ));
                }
                SignalingEvent::Moderation(ModerationEvent::InWaitingRoom) => {
                    return AppCommandOutput::InWaitingRoom {
                        signaling_sink,
                        signaling_stream,
                    };
                }
                other => {
                    return AppCommandOutput::ConnectionError(format!(
                        "Received command which is not an answer to the join:\n{other:#?}"
                    ));
                }
            },
            Ok(Some(SignalingStreamEvent::Ping(ping))) => {
                signaling_sink.send_pong(ping).await.unwrap();
            }
            Ok(Some(SignalingStreamEvent::Close)) => {
                return AppCommandOutput::ConnectionError(
                    "Server unexpetedly sent websocket close command".to_string(),
                );
            }
            Ok(None) => {}
            Err(e) => {
                return AppCommandOutput::ConnectionError(format!("Error:\n{e:#?}"));
            }
        }
    }
}

trait SetMeeting {
    fn set_meeting_widget(&self, widget: Option<&gtk::Widget>);
}

impl SetMeeting for gtk::Stack {
    fn set_meeting_widget(&self, widget: Option<&gtk::Widget>) {
        let child = self.child_by_name(Child::Meeting.as_ref());
        match child {
            Some(child) if Some(&child) == widget => {
                return;
            }
            Some(child) => {
                self.remove(&child);
            }
            None => {}
        }
        if let Some(w) = widget {
            self.add_named(w, Some(Child::Meeting.as_ref()));
        }
    }
}

trait AddToastIfSome {
    fn add_toast_if_some(&self, title: &Option<String>);
}

impl AddToastIfSome for adw::ToastOverlay {
    fn add_toast_if_some(&self, title: &Option<String>) {
        if let Some(title) = title {
            self.add_toast(adw::Toast::builder().title(title).timeout(30).build());
        }
    }
}
