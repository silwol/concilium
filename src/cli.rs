// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use clap::{Parser, Subcommand};
use url::Url;

/// Desktop Client for OpenTalk
#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Open the invite or room url form on start
    Join {
        /// The invite or room url that should be pre-filled
        url: Option<Url>,
    },
}
