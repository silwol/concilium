// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{OrientableExt as _, WidgetExt as _};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};

pub struct ProgressIndicator;

#[relm4::component(async, pub)]
impl AsyncComponent for ProgressIndicator {
    type Init = ();
    type Input = ();
    type Output = ();
    type CommandOutput = ();

    view! {
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,

            gtk::Box {
                set_hexpand: true,
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                gtk::Box {
                    set_vexpand: true,
                },
                gtk::Spinner {
                    set_spinning: true,
                },
                gtk::Label {
                    set_label: "Connecting…"
                },
                gtk::Box {
                    set_vexpand: true,
                },
            },
            gtk::Box {
                set_hexpand: true,
            },
        }
    }

    async fn init(
        _init: Self::Init,
        _root: Self::Root,
        _sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self;
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        _msg: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
    }
}
