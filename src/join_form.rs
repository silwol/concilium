// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{BoxExt, ButtonExt, EditableExt, EntryExt, OrientableExt, WidgetExt};
use opentalk_client::Client;
use opentalk_types::core::{InviteCodeId, RoomId};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};

#[derive(Debug)]
struct UserData {
    client: Client,
    room_id: RoomId,
    invite_code: Option<InviteCodeId>,
}

#[derive(Debug)]
pub struct JoinForm {
    user_data: Option<UserData>,
    display_name: String,
    password_required: bool,
    password: String,
    just_updated: bool,
}

#[derive(Debug)]
pub struct JoinFormInit {}

#[derive(Debug)]
pub enum JoinFormInput {
    SetDisplayName(String),
    SetPassword(String),
    ReadyToStart {
        client: Client,
        password_required: bool,
        display_name: String,
        room_id: RoomId,
        invite_code: Option<InviteCodeId>,
    },
    Join,
}

#[derive(Debug)]
pub enum JoinFormOutput {
    Join {
        client: Client,
        display_name: String,
        password: Option<String>,
        room_id: RoomId,
        invite_code: Option<InviteCodeId>,
    },
}

#[relm4::component(async, pub)]
impl AsyncComponent for JoinForm {
    type Init = JoinFormInit;
    type Input = JoinFormInput;
    type Output = JoinFormOutput;
    type CommandOutput = ();

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Vertical,

            set_center_widget: Some(&form),
        },

        #[name = "form"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_margin_top: 5,
            set_margin_bottom: 5,
            set_margin_start: 10,
            set_margin_end: 10,
            set_spacing: 10,

            gtk::Entry {
                set_placeholder_text: Some("Display name"),
                #[track = "model.just_updated"]
                set_text: &model.display_name,
                set_hexpand: true,
                connect_changed[sender] => move |entry| {
                    sender.input(JoinFormInput::SetDisplayName(entry.text().to_string()));
                }
            },
            gtk::PasswordEntry {
                set_placeholder_text: Some("Password"),
                #[watch]
                set_visible: model.password_required,
                #[track = "model.just_updated"]
                set_text: &model.password,
                set_hexpand: true,
                connect_changed[sender] => move |entry| {
                    sender.input(JoinFormInput::SetPassword(entry.text().to_string()));
                }
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Horizontal,
                set_spacing: 10,
                gtk::Button {
                    set_label: "Join",
                    add_css_class: "suggested-action",
                    set_hexpand: false,
                    connect_clicked[sender] => move |_button| {
                        sender.input(JoinFormInput::Join);
                    },
                },
            }
        }
    }

    async fn init(
        _init: JoinFormInit,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self {
            user_data: None,
            display_name: "".to_string(),
            password_required: false,
            password: "".to_string(),
            just_updated: false,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: JoinFormInput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        self.just_updated = false;

        match msg {
            JoinFormInput::SetDisplayName(display_name) => {
                self.display_name = display_name;
            }
            JoinFormInput::SetPassword(password) => {
                self.password = password;
            }
            JoinFormInput::ReadyToStart {
                client,
                password_required,
                display_name,
                room_id,
                invite_code,
            } => {
                self.user_data = Some(UserData {
                    client,
                    room_id,
                    invite_code,
                });
                self.password_required = password_required;
                self.display_name = display_name;
                self.password = "".to_string();
                self.just_updated = true;
            }
            JoinFormInput::Join => {
                let UserData {
                    client,
                    room_id,
                    invite_code,
                } = self.user_data.take().unwrap();
                sender
                    .output(JoinFormOutput::Join {
                        display_name: self.display_name.clone(),
                        password: if self.password_required {
                            Some(self.password.to_string())
                        } else {
                            None
                        },
                        client,
                        room_id,
                        invite_code,
                    })
                    .unwrap();
            }
        }
    }
}
