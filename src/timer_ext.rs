// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use chrono::Duration;
use opentalk_types::{
    core::Timestamp,
    signaling::timer::event::{self as timer_event, Kind},
};

use crate::utils::{build_duration_label, TimeResolution};

pub(crate) struct NormalTimerInfo {
    pub started: timer_event::Started,
    pub duration_label: String,
    pub duration_name: &'static str,
    pub participant_marked_as_done: bool,
}

impl NormalTimerInfo {
    fn build(started: timer_event::Started) -> Self {
        let now = Timestamp::now();

        let (duration_label, duration_name) = match started.kind {
            Kind::Stopwatch => (
                build_duration_label(started.started_at, now, TimeResolution::Seconds),
                "Elapsed time",
            ),
            Kind::Countdown { ends_at } => (
                build_duration_label(now, ends_at, TimeResolution::Seconds),
                "Remaining time",
            ),
        };

        NormalTimerInfo {
            started,
            duration_label,
            duration_name,
            participant_marked_as_done: false,
        }
    }

    pub(crate) fn update(&mut self, info: NormalTimerInfo) {
        self.started = info.started;
        self.duration_label = info.duration_label;
        self.duration_name = info.duration_name;
    }

    pub(crate) fn title(&self) -> &str {
        self.started
            .title
            .as_ref()
            .map(String::as_str)
            .unwrap_or("Timer")
    }
}

pub(crate) struct ThemedBreakInfo {
    pub timer_theme: Option<String>,
    pub duration_label: String,
    pub is_alert: bool,
}

impl ThemedBreakInfo {
    fn build(ends_at: Timestamp, timer_theme: Option<impl Into<String>>) -> Self {
        let now = Timestamp::now();
        ThemedBreakInfo {
            timer_theme: timer_theme.map(Into::into),
            duration_label: build_duration_label(now, ends_at, TimeResolution::Seconds),
            is_alert: is_alert(now, ends_at),
        }
    }
}

pub(crate) trait TimerStartedExt {
    fn is_normal_timer(&self) -> bool;

    fn is_themed_break(&self) -> bool;

    fn normal_timer_info(&self) -> Option<NormalTimerInfo>;

    fn themed_break_info(&self) -> Option<ThemedBreakInfo>;
}

impl TimerStartedExt for timer_event::Started {
    fn is_normal_timer(&self) -> bool {
        matches!(
            self.style.as_ref().map(String::as_str),
            Some("normal") | None
        )
    }

    fn is_themed_break(&self) -> bool {
        self.themed_break_info().is_some()
    }

    fn normal_timer_info(&self) -> Option<NormalTimerInfo> {
        if matches!(
            self.style.as_ref().map(String::as_str),
            Some("normal") | None
        ) {
            Some(NormalTimerInfo::build(self.clone()))
        } else {
            None
        }
    }

    fn themed_break_info(&self) -> Option<ThemedBreakInfo> {
        match (self.kind, self.style.as_ref()) {
            (Kind::Countdown { ends_at }, Some(style)) if style == "coffee-break" => {
                Some(ThemedBreakInfo::build(ends_at, Some(style)))
            }
            (Kind::Countdown { .. }, Some(_) | None) => None,
            (Kind::Stopwatch, _) => None,
        }
    }
}

fn is_alert(now: Timestamp, ends_at: Timestamp) -> bool {
    (ends_at - now) < Duration::minutes(1)
}
