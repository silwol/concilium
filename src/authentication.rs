// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use std::{
    net::{IpAddr, Ipv6Addr, SocketAddr, TcpListener},
    sync::Arc,
};

use anyhow::Result;
use axum::response::Html;
use gtk::traits::{BoxExt, ButtonExt, OrientableExt, WidgetExt};
use log::error;
use openid::{Token, Userinfo};
use opentalk_client::{Client, RoomOrInviteId};
use relm4::{
    adw,
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender, RelmWidgetExt,
};
use serde::Deserialize;
use url::Url;

pub struct Authentication {
    current_state_message: String,
    auth_url: Option<String>,
    stop_server_tx: Option<tokio::sync::oneshot::Sender<()>>,
    // TODO: we shouldn't loop the meeting id through the authentication component,
    // there must be a better way of getting it to the location where we need it
    meeting_id: Option<RoomOrInviteId>,
}

#[derive(Debug)]
pub struct AuthenticationInit;

#[derive(Debug)]
pub enum AuthenticationInput {
    GoBack,
    StartAuthentication {
        url: Url,
        meeting_id: Option<RoomOrInviteId>,
    },
    BearerReceived {
        base_url: Url,
        bearer: openid::Bearer,
        user_info: openid::Userinfo,
    },
    OpenAuthInBrowser,
    CopyAuthUrl,
}

#[derive(Debug)]
pub enum AuthenticationOutput {
    Aborted,
    Authenticated {
        meeting_id: Option<RoomOrInviteId>,
        user_info: openid::Userinfo,
        client: Client,
    },
}

#[derive(Debug)]
pub enum AuthenticationCommandOutput {
    PerformAuthentication {
        url: String,
        axum_app: axum::Router,
        listener: TcpListener,
    },
    AuthenticationFinished,
    LoggedIn {
        user_info: openid::Userinfo,
        client: Client,
    },
    Failed(String),
}

#[derive(Deserialize, Debug)]
struct LoginQuery {
    pub code: String,
    #[allow(unused)]
    pub state: Option<String>,
}

type OpenIDClient = openid::Client<openid::Discovered, openid::StandardClaims>;

#[relm4::component(async, pub)]
impl AsyncComponent for Authentication {
    type Init = AuthenticationInit;
    type Input = AuthenticationInput;
    type Output = AuthenticationOutput;
    type CommandOutput = AuthenticationCommandOutput;

    view! {
        #[root]
        gtk::CenterBox {
            set_orientation: gtk::Orientation::Vertical,

            set_margin_start: 5,
            set_margin_end: 5,
            set_margin_top: 5,
            set_margin_bottom: 5,

            set_start_widget: Some(&start),
            set_center_widget: Some(&center),
            set_end_widget: Some(&end),
        },

        #[name = "start"]
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,

            set_spacing: 10,

            gtk::Button {
                set_icon_name: "go-previous-symbolic",
                connect_clicked => AuthenticationInput::GoBack,
            },
        },

        #[name = "center"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,
            set_spacing: 10,

            gtk::CenterBox {
                set_orientation: gtk::Orientation::Horizontal,

                set_center_widget: Some(&auth_widgets),
            },
        },

        #[name = "end"]
        gtk::Label {
            #[watch]
            set_text: &model.current_state_message,
        },

        #[name = "auth_widgets"]
        gtk::Box {
            set_orientation: gtk::Orientation::Vertical,

            #[watch]
            set_visible: model.auth_url.is_some(),

            adw::SplitButton {
                set_label: "Authenticate in web browser",
                set_popover: Some(&auth_link_popover),
                add_css_class: "suggested-action",

                connect_clicked => AuthenticationInput::OpenAuthInBrowser,
            },
        },

        #[name = "auth_link_popover"]
        gtk::Popover {
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 10,

                gtk::Label {
                    set_label: "Open this URL in a browser to perform authentication",
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_spacing: 10,

                    gtk::Label {
                        #[watch]
                        set_markup: &format!("<a href=\"{0}\">{0}</a>", model.auth_url.as_ref().map(String::as_str).unwrap_or("")),
                        set_hexpand: true,
                        set_xalign: 0f32,
                    },

                    gtk::Button {
                        set_icon_name: "edit-copy-symbolic",
                        set_tooltip: "Copy URL to clipboard",

                        connect_clicked => AuthenticationInput::CopyAuthUrl,
                    },
                },
            }
        }
    }

    async fn init(
        _init: AuthenticationInit,
        _root: Self::Root,
        _sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self {
            current_state_message: "Idle".to_string(),
            auth_url: None,
            stop_server_tx: None,
            meeting_id: None,
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: AuthenticationInput,
        sender: AsyncComponentSender<Self>,
        root: &Self::Root,
    ) {
        match msg {
            AuthenticationInput::GoBack => {
                if let Some(stop) = self.stop_server_tx.take() {
                    stop.send(()).unwrap();
                }
                self.current_state_message = "Idle".to_string();
                self.auth_url = None;
                sender.output(AuthenticationOutput::Aborted).unwrap();
            }
            AuthenticationInput::StartAuthentication { url, meeting_id } => {
                self.meeting_id = meeting_id;
                let cloned_sender = sender.clone();
                sender.oneshot_command(async move {
                    match prepare_authentication_webservice(url, cloned_sender).await {
                        Ok((url, axum_app, listener)) => {
                            AuthenticationCommandOutput::PerformAuthentication {
                                url,
                                axum_app,
                                listener,
                            }
                        }
                        Err(e) => AuthenticationCommandOutput::Failed(format!("{e:#?}")),
                    }
                });
            }
            AuthenticationInput::BearerReceived {
                base_url,
                bearer,
                user_info,
            } => {
                sender.oneshot_command(async move {
                    match Client::connect_authenticated(
                        base_url.as_str(),
                        bearer.access_token.clone(),
                    )
                    .await
                    {
                        Ok(client) => {
                            match client
                                .post_auth_login(bearer.id_token.clone().unwrap())
                                .await
                            {
                                Ok(_permissions) => {
                                    AuthenticationCommandOutput::LoggedIn { user_info, client }
                                }
                                Err(e) => AuthenticationCommandOutput::Failed(format!("{e}")),
                            }
                        }
                        Err(e) => AuthenticationCommandOutput::Failed(format!("{e}")),
                    }
                });
            }
            AuthenticationInput::OpenAuthInBrowser => {
                if let Some(auth_url) = self.auth_url.as_ref().map(|s| s.as_str()) {
                    webbrowser::open(auth_url).ok();
                }
            }
            AuthenticationInput::CopyAuthUrl => {
                if let Some(auth_url) = self.auth_url.as_ref().map(|s| s.as_str()) {
                    let clipboard = root.clipboard();
                    clipboard.set_text(auth_url);
                }
            }
        }
    }

    async fn update_cmd(
        &mut self,
        msg: AuthenticationCommandOutput,
        sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            AuthenticationCommandOutput::PerformAuthentication {
                url,
                axum_app,
                listener,
            } => {
                self.auth_url = Some(url);
                self.current_state_message =
                    "Waiting for user authenticate on the website…".to_string();
                let (stop_server_tx, stop_server_rx) = tokio::sync::oneshot::channel::<()>();
                if let Some(stop) = self.stop_server_tx.replace(stop_server_tx) {
                    stop.send(()).ok();
                }
                sender.oneshot_command(async move {
                    match perform_authentication(axum_app, listener, stop_server_rx).await {
                        Ok(()) => AuthenticationCommandOutput::AuthenticationFinished,
                        Err(e) => AuthenticationCommandOutput::Failed(format!("{e}")),
                    }
                });
            }
            AuthenticationCommandOutput::AuthenticationFinished => {}
            AuthenticationCommandOutput::LoggedIn { user_info, client } => {
                sender
                    .output(AuthenticationOutput::Authenticated {
                        meeting_id: self.meeting_id,
                        user_info,
                        client,
                    })
                    .unwrap();
            }
            AuthenticationCommandOutput::Failed(message) => {
                println!(
                    "Received authentication command output: {message:?}, but that is not yet handled",
                );
                // TODO
            }
        }
    }
}

#[derive(Clone)]
struct AuthWebserverState {
    oidc_client: Arc<OpenIDClient>,
    sender: AsyncComponentSender<Authentication>,
    auth_url: Url,
    base_url: Url,
}

async fn prepare_authentication_webservice(
    base_url: Url,
    sender: AsyncComponentSender<Authentication>,
) -> Result<(String, axum::Router, TcpListener)> {
    let client = Client::connect_unauthenticated(base_url.as_str()).await?;
    let oidc_config = &client.frontend_config().oidc_config;
    println!("{oidc_config:#?}");

    let socket = SocketAddr::new(IpAddr::V6(Ipv6Addr::LOCALHOST), 0);
    let listener = TcpListener::bind(socket)?;
    let local_address = listener.local_addr()?;
    let local_port = local_address.port();
    println!("Waiting for OIDC answer on port {}", local_port);

    let client_id = oidc_config.client_id.to_string();
    let issuer = Url::parse(&oidc_config.authority)?;
    let url = format!("http://localhost:{local_port}");
    let redirect = format!("{url}/login/oauth2/code/oidc");

    let oidc_client = Arc::new(
        openid::DiscoveredClient::discover(client_id, None, Some(redirect.to_string()), issuer)
            .await?,
    );

    println!(
        "--> OIDC CLIENT CONFIG DISCOVERED: {:#?}",
        oidc_client.config()
    );

    let auth_url = oidc_client.auth_url(&openid::Options {
        scope: Some("openid profile email".into()),
        ..Default::default()
    });
    println!("auth url: {auth_url:#?}");

    let state = AuthWebserverState {
        oidc_client,
        sender,
        auth_url,
        base_url,
    };
    let axum_app: axum::Router = axum::Router::new()
        .route("/", axum::routing::get(get_root))
        .route(
            "/login/oauth2/code/oidc",
            axum::routing::get(get_login_oauth2_code_oidc),
        )
        .with_state(state);
    Ok((url, axum_app, listener))
}

async fn perform_authentication(
    axum_app: axum::Router,
    listener: TcpListener,
    stop_server_rx: tokio::sync::oneshot::Receiver<()>,
) -> Result<()> {
    axum::Server::from_tcp(listener)?
        .serve(axum_app.into_make_service())
        .with_graceful_shutdown(async {
            println!("SERVER STARTED, WAITING FOR SHUTDOWN");
            stop_server_rx.await.ok();
        })
        .await?;
    Ok(())
}

async fn get_root(
    axum::extract::State(AuthWebserverState { auth_url, .. }): axum::extract::State<
        AuthWebserverState,
    >,
) -> Html<String> {
    Html(format!(
        "<html>
          <head>
            <title>Authentication required</title>
          </head>
          <body>
            <h1>Authentication required</h1>
            <b><a href=\"{auth_url}\">Perform authentication</a></b>
            <br />
            <br />
            <br />
            <br />
            <h5>Note</h5>
            <small>
                You may receive an error <i>&quot;Invalid parameter: redirect_uri&quot;</i>.
                For now, <i>Concilium</i> only supports a very basic method of authentication against
                the OAuth2 provider, which requires the identity provider to allow redirecting to
                <i>http://localhost:*/</i>. A proper authentication flow will be implemented once
                a few details are fleshed out.
            </small>
          </body>
        </html>"
    ))
}

async fn get_login_oauth2_code_oidc(
    axum::extract::Query(login_query): axum::extract::Query<LoginQuery>,
    axum::extract::State(AuthWebserverState {
        oidc_client,
        sender,
        base_url,
        ..
    }): axum::extract::State<AuthWebserverState>,
) -> Html<&'static str> {
    let request_token = request_token(oidc_client, &login_query).await;

    match request_token {
        Ok(Some((token, user_info))) => {
            let id_token = &token.id_token;
            let bearer = token.bearer.clone();
            println!(
                "id_token:\n{id_token:#?}\n\nbearer:{bearer:#?}\n\nuser_info:\n{user_info:#?}"
            );

            sender.input(AuthenticationInput::BearerReceived {
                base_url,
                bearer,
                user_info,
            });

            Html(
                "<html>
              <head>
                <title>Authentication successful</title>
              </head>
              <body>
                <h1>Authentication successful</h1>
                You can close this website now.
              </body>
            </html>",
            )
        }
        Ok(None) => {
            error!("No id token found in authentication");
            Html(
                "<html>
              <head>
                <title>Authentication failed, no id token found</title>
              </head>
              <body>
                <h1>Authentication failed, no id token found</h1>
                Please close this website now and retry.
              </body>
            </html>",
            )
        }
        Err(e) => {
            error!("login error in call: {:?}", e);

            Html(
                "<html>
              <head>
                <title>Authentication failed</title>
              </head>
              <body>
                <h1>Authentication failed</h1>
                Please close this website now and retry.
              </body>
            </html>",
            )
        }
    }
}

async fn request_token(
    oidc_client: Arc<OpenIDClient>,
    login_query: &LoginQuery,
) -> Result<Option<(Token, Userinfo)>> {
    let mut token: Token = oidc_client.request_token(&login_query.code).await?.into();

    if let Some(id_token) = token.id_token.as_mut() {
        oidc_client.decode_token(id_token)?;
        oidc_client.validate_token(id_token, None, None)?;
    } else {
        return Ok(None);
    }

    let userinfo = oidc_client.request_userinfo(&token).await?;

    Ok(Some((token, userinfo)))
}
