// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use gtk::traits::{ButtonExt as _, OrientableExt as _, WidgetExt as _};
use relm4::{
    component::{AsyncComponent, AsyncComponentParts},
    gtk, AsyncComponentSender,
};

pub struct ErrorMessage {
    message: String,
}

#[derive(Debug)]
pub enum ErrorMessageInput {
    UpdateMessage(String),
}

#[derive(Debug)]
pub enum ErrorMessageOutput {
    Confirmed,
}

#[relm4::component(async, pub)]
impl AsyncComponent for ErrorMessage {
    type Init = ();
    type Input = ErrorMessageInput;
    type Output = ErrorMessageOutput;
    type CommandOutput = ();

    view! {
        gtk::Box {
            set_orientation: gtk::Orientation::Horizontal,

            gtk::Box {
                set_hexpand: true,
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                gtk::Box {
                    set_vexpand: true,
                },
                gtk::Label {
                    #[watch]
                    set_label: &model.message,
                },
                gtk::Button {
                    set_label: "Ok",
                    connect_clicked[sender] => move |_| {
                        sender.output(Self::Output::Confirmed).unwrap();
                    },
                },
                gtk::Box {
                    set_vexpand: true,
                },
            },
            gtk::Box {
                set_hexpand: true,
            },
        }
    }

    async fn init(
        _init: Self::Init,
        _root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        let model = Self {
            message: "".to_string(),
        };
        let widgets = view_output!();
        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        msg: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        match msg {
            Self::Input::UpdateMessage(message) => {
                self.message = message;
            }
        }
    }
}
