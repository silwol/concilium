// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
// SPDX-License-Identifier: EUPL-1.2

use chrono::Duration;
use opentalk_types::core::Timestamp;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TimeResolution {
    Minutes,
    Seconds,
}

impl TimeResolution {
    fn format_duration(&self, duration: Duration) -> String {
        let days = duration.num_days();
        let hours = duration.num_hours() % 24;
        let minutes = duration.num_minutes() % 60;

        match self {
            TimeResolution::Minutes => {
                if days != 0 {
                    format!("{days}d {hours:02}:{minutes:02}")
                } else {
                    format!("{hours:02}:{minutes:02}")
                }
            }
            TimeResolution::Seconds => {
                let seconds = duration.num_seconds() % 60;
                if days != 0 {
                    format!("{days}d {hours:02}:{minutes:02}:{seconds:02}")
                } else if hours != 0 {
                    format!("{hours:02}:{minutes:02}:{seconds:02}")
                } else {
                    format!("{minutes:02}:{seconds:02}")
                }
            }
        }
    }
}

pub(crate) fn build_duration_label(
    start_time: Timestamp,
    end_time: Timestamp,
    resolution: TimeResolution,
) -> String {
    let duration = end_time - start_time;

    if duration > Duration::zero() {
        resolution.format_duration(duration)
    } else {
        "".to_string()
    }
}
