# Concilium

Experimental OpenTalk desktop client using GTK and Rust.

## Status

This project is in a early stage, and it is not guaranteed to ever
leave that stage, because for now it is purely a spare time project
of a single developer.

Currently only buildable with my personal `opentalk-client` crate,
which is being developed in the
[`experiment/client`](https://salsa.debian.org/silwol/opentalk-controller/-/tree/experiment/client)
of the OpenTalk project. I force-push that branch to the linked repository 
on a regular basis until the changes are published with an
OpenTalk release one day.

### Features

A list of currently known OpenTalk features and its implementation state
in *Concilium*

* [x] Join as guest or authenticated
* [x] Join through room or invitation link
* [x] Room chat
* [x] Group chat
* [x] Private chat
* [x] Notification of unread chat messages
* [x] Waiting room
* [x] Waiting room management for moderators
* [ ] Breakout rooms
* [ ] Media (audio and video streams)
* [ ] Screen sharing
* [x] Countdown timer
* [x] Unlimited timer counting up
* [x] Show timer participant ready status to moderator
* [x] Coffee break timer
* [ ] Protocol (meeting notes)
* [ ] Polls
* [x] Debriefing
* [x] Reset all raised hands
* [x] Kick participants
* [ ] Whiteboard
* [ ] Recording
* [x] Shared folder (integration with NextCloud)
* [ ] Automoderation (enterprise feature)
* [ ] Legal vote (enterprise feature)

## License

Licensed under the [EUPL](https://joinup.ec.europa.eu/collection/eupl/introduction-eupl-licence) version 1.2.

Wherever possible, each file contains a license header compatible with [`reuse`](https://reuse.software/).
All other files are documented either in [`.reuse/dep5`](.reuse/dep5), or have a `*.license` file with
the same name next to them.
